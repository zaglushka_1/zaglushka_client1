<?php
    use yii\helpers\Url;
?>
<header>
    <div class="logo">
        <a href="<?= Url::to(['site/index']); ?>">
            <img src="https://zaglushka.ru/images/logo.svg?v=2" />
        </a>
    </div>
    <div class="phone_local">
        <div class="phone_local_icon">
            <svg>
                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#marker"></use>
            </svg>
        </div>
        <div class="phone_local_text">
            <a href="tel:+78005550499"><?= (Yii::$app->params['local_phone']) ? Yii::$app->params['local_phone'] : ''; ?></a>
            <p><span class="change-town"><?= (Yii::$app->params['city']) ? Yii::$app->params['city'] : ''; ?></span></p>
        </div>
    </div>
    <div class="phone_all">
        <div class="phone_all_icon">
            <svg>
                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#phone"></use>
            </svg>
        </div>
        <div class="phone_all_text">
            <a href="tel:+78005550499">8 (800) 555 04 99</a>
            <p>Бесплатно по России</p>
        </div>
    </div>
    <div class="phone_mob">
        <a class="mob_phone" href="tel:+78005550499">
            <svg>
                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#phone2"></use>
            </svg>
        </a>
    </div>
    <div class="soc_icon">
        <div class="soc_icon_block">
            <a class="mob_phone" href="tel:+78005550499">
                <svg>
                    <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#phone"></use>
                </svg>
            </a>
            <a class="social" href="https://msng.link/o/?andreydinev1991=tg" target="_blank" style="color: white;">
                <img src="/images/icons/telegram.png" width="30" height="30">
            </a>
            <a class="social" href="https://vk.com/im?sel=-187690751" target="_blank" style="margin-left: 18px;color: white;">
                <img src="/images/icons/vk.png" width="30" height="30">
            </a>
            <a class="social" href="https://wa.me/message/AWWEIHNDT6HVD1" target="_blank" style="margin-left: 18px;color: white;">
                <img src="/images/icons/whatsapp.png" width="30" height="30">
            </a>
            <a class="social" href="mailto:sales@zaglushka.ru" target="_blank" style="margin-left: 18px;color: white;">
                <img src="/images/icons/e-mail.png" width="30" height="30">
            </a>
            <p style="flex-basis:100%; text-align:center; margin-top:0px; margin-bottom: 0px;">
                <span>Задать вопрос в соц сети</span>
            </p>
        </div>
    </div>
    <div class="change_language">
        <div class="change_language_flag">
            <svg>
                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#flag_ru"></use>
            </svg>
        </div>
    </div>
</header>
