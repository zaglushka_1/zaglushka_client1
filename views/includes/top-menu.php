<?php
    use yii\helpers\Url;
?>
<nav>
    <div class="nav_left">
        <ul>
            <li class="">
                <a href="/mob_menu">
                    <svg class="icon icon_hamburger-menu">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#hamburger-menu"></use>
                    </svg>
                </a>
            </li>
            <li class="hamburger_menu_item"><a href="<?= Url::to(['site/index']); ?>" class="production_link">Продукция</a>
                <svg class="nav__link-arrow icon icon_arrow-menu">
                    <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
                </svg>
            </li>
            <li><a href="<?= Url::to(['site/about']); ?>">О компании</a></li>
            <li><a href="<?= Url::to(['site/delivery']); ?>">Доставка</a></li>
            <li><a href="<?= Url::to(['site/contacts']); ?>">Контакты</a></li>
            <li><a href="<?= Url::to(['site/booklet']); ?>">Брошюры</a></li>
        </ul>
    </div>
    <div class="nav_phone">
        <a href="tel:+78005550499">8 (800) 555 04 99</a>
    </div>
    <div class="nav_right">
        <ul>
            <li><a href="<?= Url::to(['site/search']); ?>">
                    <svg class="nav__link-icon nav__link-icon_search icon icon_search">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#search"></use>
                    </svg>
                    <span>Поиск</span>
                </a></li>
            <li>
                <a href="<?= Url::to(['site/cart']); ?>">
                    <svg class="nav__link-icon nav__link-icon_basket icon icon_basket">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#basket"></use>
                    </svg>
                    <?php if(Yii::$app->params['cart_sum_element']>0): ?>
                        <span class="nav__tooltip js-nav-cart-count-block">
                            <span class="js-nav-cart-count"><?=number_format(Yii::$app->params['cart_sum_element'],0,'',' '); ?></span> <span>шт.</span>
                        </span>
                    <?php else: ?>
                    <span>Корзина</span>
                    <?php endif; ?>
                </a>
            </li>
        </ul>
    </div>
    <div class="nav_catalog">
        <div class="top_level">
        <?php foreach($catalog_element as $key => $element): ?>
            <div class="">
                <a href="<?= $element['top_element']['slug']; ?>" data-target="<?= $key; ?>"<?= ($key==60) ? 'class="active"' : ''; ?>><?= $element['top_element']['title']; ?></a>
            </div>
        <?php endforeach; ?>
            <div class="sales">
                <img class="header-catalog-menu__icon" src="https://zaglushka.ru/images/sale.svg">
                <a href="">Товары со скидками</a>
            </div>
        </div>
        <div class="sub_level">
        <?php foreach($catalog_element as $key => $element): ?>
            <div id="sub-level-<?= $key; ?>"<?= ($key==60) ? ' class="active"' : ''; ?>>
                <?php if(isset($element['sub_element'])): ?>
                    <?php foreach($element['sub_element'] as $sub_element): ?>
                        <a href="/<?= $sub_element['slug']; ?>" class="">
                           <img src="/images/cat/<?= $sub_element['id']; ?>_list.jpg" style="width: 100px; height: 100px;"/>
                           <span><?= $sub_element['title']; ?></span>
                        </a>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
        </div>
    </div>
</nav>
