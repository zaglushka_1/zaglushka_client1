<div class="p-delivery__body" style="margin: 20px 20px;">
    <p class="p-delivery__paragraph">
        Мы отправляем заказы по России каждый будний день.
        <br>
        Доставка возможна до терминала транспортной компании или по указанному Вами адресу.
    </p>
    <p class="p-delivery__paragraph">
        Оформляем все необходимые документы и бесплатно доставляем товар до склада ТК в г. Санкт-Петербурге. Самостоятельно Вы оплачиваете только услуги транспортной компании.
    </p>
    <div class="p-delivery__title">Мы также предлагаем бесплатную доставку до пункта выдачи ТК, если::</div>
    <ul class="p-delivery__list">
        <li>сумма заказа более 3 600 рублей (для города Санкт-Петербург), без учета товаров со скидкой;</li>
        <li>заказ полностью оплачен;</li>
        <li>заказ не содержит крупногабаритные товары: <a class="link" href="https://zaglushka.ru/gorki-plastikovye">горки</a>,
            <a class="link" href="https://zaglushka.ru/cepi-i-komplektuyushchie">стальные цепи</a>,
            <a class="link" href="https://zaglushka.ru/spinki-i-sidenya">сиденья</a> и
            <a class="link" href="https://zaglushka.ru/spinki-i-sidenya">каркасы для стульев</a>.
        </li>
    </ul>
</div>