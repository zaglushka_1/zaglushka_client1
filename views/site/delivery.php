<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\bootstrap\Tabs;

$this->title = 'Доставка';
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="catalog_content">
        <?php
        echo Tabs::widget([
            'items' => [
                [
                    'label'     =>  'по Санкт-Петербургу',
                    'content'   =>  $this->render('delivery/delivery_city', []),
                    'active'    =>  true
                ],
                [
                    'label'     => 'по России',
                    'content'   =>  $this->render('delivery/delivery_russia', [])
                ],
                [
                    'label'     => 'по СНГ и Европе',
                    'content'   =>  $this->render('delivery/delivery_world', [])
                ]
            ]
        ]);
        ?>
    </div>
    <h2>Способы оплаты</h2>
    <div class="catalog_content">
        <p>Компания Заглушка.Ру работает не только с юридическими лицами и оптовыми покупателями, но также и с физическими лицами, предоставляя удобные способы оплаты банковской картой, электронными деньгами, оплатой через мобильное приложение банков и платежных систем Google Pay, Apple Pay и другие.</p>
        <h3>Для юридических лиц</h3>
        <h3 class="pay pay-wallet">Безналичный расчет</h3>
        <p>Размер комиссии зависит от банка, через который осуществляется платеж. Оплата фиксируется по рабочим дням, только по факту поступления средств на расчетный счет компании. Реквизиты компании</p>
        <h3>Для физических лиц</h3>
        <div class="payments-logo">
            <ul class="payments">
                <li class="visa"></li>
                <li class="maestro"></li>
                <li class="mastercard"></li>
                <li class="mir"></li>
                <li class="jcb"></li>
                <li class="apple"></li>
                <li class="google"></li>
                <li class="yandexmoney"></li>
                <li class="webmoney"></li>
                <li class="qiwi"></li>
                <li class="umoney"></li>
            </ul>
        </div>
        <p>
            <strong>Банковские карты и платежные системы</strong>
        </p>
        <p>Автоматическое зачисление средств в течение 10–15 минут. Совершить оплату можно с карт Visa, Visa Electron, MasterCard, Maestro, Eurocard и МИР, а также используя электронные деньги, банковские переводы, платежные системы.</p>
    </div>
</div>
