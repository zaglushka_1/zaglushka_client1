<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\web\JqueryAsset;
use yii\bootstrap\Tabs;
use yii\jui\Accordion;
use app\models\ItemsModel;
/* @var $this yii\web\View */

$this->title="Модель: ".$title;
/*for($i=sizeof($catalog_element['breadcrumbs'])-1;$i>=0;$i--){
    $this->params['breadcrumbs'][] = array(
        'label'=> $catalog_element['breadcrumbs'][$i]['title'],
        'url'=>$catalog_element['breadcrumbs'][$i]['url']
    );
}*/
$additionalPrices = [
    0 => 0,
    1 => 20,
    2 => 30,
    7 => 35,
    8 => 40,
    9 => 10,
    3 => 50,
    4 => 70,
    5 => 100,
    6 => 200
];
?>

<div class="item_title_mob">
    <div class="item_title_mob--back">
        <a href="/">
            <svg class="p-main-catalog__link-mob-icon icon icon_arrow-bredcrumbs2">
                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-braedcrumbs2"></use>
            </svg>
        </a>
    </div>
    <div class="item_title_mob--value">
        <span>"Модель: <?= $title; ?>"</span>
    </div>
    <div class="item_title_mob--filters">

    </div>
</div>
<div class="item_content">
    <div class="item_title" style="flex-basis: 100%;">Модель: <?= $title; ?></div>
    <div class="item_photo">
        <div class="item_photo_carousel">

        </div>
    </div>
    <div class="item_description">
        <div class="item_articul">
            <table border="1" width="100%;">
                <tr>
                    <td rowspan="2">Артикул</td>
                    <td colspan="1">Размеры</td>
                    <td colspan="7">Цены</td>
                    <td rowspan="2"></td>
                </tr>
                <tr>
                    <td>ØD</td>
                    <td>до 100</td>
                    <td>от 100 до 500</td>
                    <td>от 500 до 1000</td>
                    <td>от 1000 до 5000</td>
                    <td>от 5000 до 10000</td>
                    <td>от 10000</td>
                </tr>
            <?php foreach($items as $item): ?>
            <tr>
                <td><?= $item->title; ?></td>
                <td><?= $item->itemSizes->size_1; ?></td>
                <td><?= $item->price0; ?></td>
                <td><?= $item->price1; ?></td>
                <td><?= $item->price2; ?></td>
                <td><?= $item->price3; ?></td>
                <td><?= $item->price4; ?></td>
                <td><?= $item->price5; ?></td>
                <td>Заказать</td>
            </tr>
            <?php endforeach; ?>
            </table>
        </div>
        <div class="order-container">
            <div class="free-item">
            </div>
            <div class="item-order">
                <a href='#' class="add_to_cart" data-id=''>Заказать</a>
            </div>
        </div>
    </div>
    <div class="item_content_text">

    </div>
</div>


