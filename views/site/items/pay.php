<ul class="item_pay_list">
    <li>По выставленному счёту (для юр. лиц)</li>
    <li>Банковской картой
        <img src="https://volgograd.zaglushka.ru/images/card/visa.svg" alt="VISA" title="VISA">
        <img src="https://volgograd.zaglushka.ru/images/card/mastercard.svg" alt="MasterCard" title="MasterCard">
        <img src="https://volgograd.zaglushka.ru/images/card/mir.svg" alt="Мир" title="Мир">
    </li>
    <li>По номеру кошелька WebMoney, Яндекс.Деньги или QIWI</li>
    <li>Наличными при получении (для физ. лиц)</li>
</ul>