<?php
$colors=[];
foreach($catalog_element['item']['colors'] as $item_color) {
    $colors[]=$item_color['color_name'];
}
$color_string=implode(", ",$colors);
?>

<?php ?>
<table class="pack_table">
    <?php if(isset($catalog_element['prop_fit_pipe']) and strlen($catalog_element['prop_fit_pipe'])>0): ?>
    <tr>
        <td><?= $catalog_element['prop_fit_pipe']; ?>:</td>
        <td><?= $catalog_element['item']['prop_pipes']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($catalog_element['matherial']['title']) and strlen($catalog_element['matherial']['title'])>0): ?>
    <tr>
        <td>Материал:</td>
        <td><?= $catalog_element['matherial']['title']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($color_string) and strlen($color_string)>0): ?>
    <tr>
        <td>Цвета:</td>
        <td><?= $color_string; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($catalog_element['item']['prop_weight']) and strlen($catalog_element['item']['prop_weight'])>0): ?>
    <tr>
        <td>Вес, гр.:</td>
        <td><?= $catalog_element['item']['prop_weight']; ?></td>
    </tr>
    <?php endif; ?>
</table>
