<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */?>
<div class="item_info_container" style="">
    <?php if($catalog_element['item']['colors'] && sizeof($catalog_element['item']['colors'])>0): ?>
    <h3 class="item_info_h3">В наличии</h3>
    <table class="item_info_table">
        <tr>
            <th>Цвет</th>
            <th>Количество</th>
        </tr>
        <?php foreach($catalog_element['item']['colors'] as $itemcolor): ?>
        <?php if((int)$itemcolor['remains']>0): ?>
        <tr>
            <td>
                <div class="" style="width: 12px; height: 12px; border: 1px solid #cacaca; background-color: #<?= $itemcolor['color_hex']; ?>"></div>
                <span style=""><?= $itemcolor['color_name']; ?> (арт. <?= $itemcolor['code']; ?>)</span>
            </td>
            <td><span style=""><?= number_format($itemcolor['remains'],0,'',' ');?> шт.</span></td>
        </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </table>
    
    <h3 class="item_info_h3_production">В производстве</h3>
    <table class="item_info_table item_info_table_production">
        <?php foreach($catalog_element['item']['colors'] as $itemcolor): ?>
        <?php if((int)$itemcolor['remains']==0): ?>
        <tr>
            <td>
                <div class="" style="width: 12px; height: 12px; border: 1px solid #cacaca; background-color: #<?= $itemcolor['color_hex']; ?>"></div>
            </td>
            <td>
                <span style=""><?= $itemcolor['color_name']; ?> (арт. <?= $itemcolor['code']; ?>)</span>
            </td>
        </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    
</div>

