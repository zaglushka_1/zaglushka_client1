<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */?>
<table class="pack_table">
    <?php if(isset($catalog_element['prop_package'])): ?>
    <tr>
        <td>Тип упаковки:</td>
        <td><?= $catalog_element['item']['prop_package']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($catalog_element['item']['pack_size'])): ?>
    <tr>
        <td>Количество в упаковке шт.:</td>
        <td><?= $catalog_element['item']['pack_size']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($catalog_element['item']['prop_pack_width'])): ?>
    <tr>
        <td>Габариты упаковки (ШхГхВ), мм:</td>
        <td><?= $catalog_element['item']['prop_pack_width']; ?>х<?= $catalog_element['item']['prop_pack_length']; ?>х<?= $catalog_element['item']['prop_pack_height']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($catalog_element['item']['prop_pack_weight_net'])): ?>
    <tr>
        <td>Вес нетто / брутто упаковки, кг:</td>
        <td><?= $catalog_element['item']['prop_pack_weight_net']; ?>/<?= $catalog_element['item']['prop_pack_weight_gross']; ?></td>
    </tr>
    <?php endif; ?>
    <?php if(isset($catalog_element['item']['prop_pack_volume'])): ?>
    <tr>
        <td>Объем упаковки, м³:</td>
        <td><?= $catalog_element['item']['prop_pack_volume']; ?></td>
    </tr> 
    <?php endif; ?>
</table>
