<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\bootstrap\Tabs;
/* @var $this yii\web\View */

AppAsset::register($this);
JqueryAsset::register($this);

$this->title = 'Корзина';
$additionalPrices = [
        0 => 0,
        1 => 20,
        2 => 30,
        7 => 35,
        8 => 40,
        9 => 10,
        3 => 50,
        4 => 70,
        5 => 100,
        6 => 200
    ];
?>
<div class="cart-page">
    <div class="heading-row">
        <h1>Корзина</h1>
    </div>
    <?php if($cart): ?>
    <div class="cart-container">
        <?php if(isset($cart) && sizeof($cart)>0): ?>
            <?php $i=1; ?>
            <div class="cart-order-item-table">
                <div class="cart-order-item-table-header">
                    <div class="cart-order-item-table-header-number">
                        <span>№</span>
                    </div>
                    <div class="cart-order-item-table-header-photo">
                        <span>Фото</span>
                    </div>
                    <div class="cart-order-item-table-header-articul">
                        <span>Артикул</span>
                    </div>
                    <div class="cart-order-item-table-header-color">
                        <span>Цвет</span>
                    </div>
                    <div class="cart-order-item-table-header-quantity">
                        <span>Количество, штук</span>
                    </div>
                    <div class="cart-order-item-table-header-price1">
                        <span>Цена за штуку</span>
                    </div>
                    <div class="cart-order-item-table-header-sum">
                        <span>Сумма</span>
                    </div>
                    <div class="cart-order-item-table-header-action">
                        <span>
                            
                        </span>
                    </div>
                </div>
            <?php foreach($cart as $id_element=>$cart_element): ?>
                <div class="cart-order-item-table-row" id="item-<?= $cart_element['item']['items_id']; ?>" data-id="<?= $cart_element['item']['items_id']; ?>">
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-number">
                        <span><?= $i; ?></span>
                    </div>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-photo">

                            <?php $img="/images/item/".substr($cart_element['item']['items_id'], -2)."/".$cart_element['item']['items_id']."/5.jpg";?>
                            <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                                <img src="<?= $img; ?>" width="95" heigh="95"/>
                            <?php else: ?>
                                <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                            <?php endif; ?>

                    </div>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-articul">
                        <span><?= $cart_element['item']['title']; ?></span>
                    </div>
                    <?php if($cart_element['quantity']=="free"): ?>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-color-free">
                        <p>Наш менеджер свяжется с вами, чтобы уточнить количество и цвет бесплатных образцов.</p>
                    </div>
                    <?php else: ?>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-color">
                        <select class="change_color_cart" name="color_name" data-id="<?= $cart_element['item']['items_id']; ?>">
                        <?php foreach($cart_element['item_colors'] as $cart_colors_element): ?>
                            <option data-title="<?= $cart_colors_element['code']; ?>" data-addPrice="<?= $cart_colors_element['add_price']; ?>" data-price0="<?= $cart_element['item']['price0']; ?>" data-price1="<?= $cart_element['item']['price1']; ?>" data-price2="<?= $cart_element['item']['price2']; ?>" data-price3="<?= $cart_element['item']['price3']; ?>" data-price4="<?= $cart_element['item']['price4']; ?>" data-price5="<?= $cart_element['item']['price5']; ?>" value="<?= $cart_colors_element['item_id']; ?>" <?= ($cart_element['item_id']==$cart_colors_element['id']) ? 'selected' : ''; ?>><?= $cart_colors_element['name']; ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-quantity">
                        <input data-id="<?= $cart_element['item']['items_id']; ?>" type="number" class="quantity" value="<?= $cart_element['quantity']; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php if($cart_element['quantity']=="free"): ?>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-price1-free">
                        Бесплатные образцы
                    </div>
                    <?php else: ?>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-price1">
                        <div class="cart_one_price"></div>
                    </div>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-sum">
                        <div class="cart_total_price"></div>
                    </div>
                    <?php endif; ?>
                    <div class="cart-order-item-table-row-element cart-order-item-table-row-action">
                        <svg class="js-order-remove order-table__close icon icon_cross" data-id="<?= ($cart_element['quantity']=="free") ? $cart_element['item']['items_id'] : $cart_element['item_id']; ?>">
                            <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#cross"></use>
                        </svg>
                    </div>
                </div>
                <?php $i++; ?>
            <?php endforeach; ?>
                <div class="cart-order-item-table-footer">
                    <div class="cart-order-item-table-footer-sum-text">
                        <span>Итого:</span>
                    </div>
                    <div class="cart-order-item-table-footer-sum-val">
                        <span></span>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="contacts-container">
        <div class="row-contact-info">
            <div class="row-contact-info-header">
                <div class="ordering-step-item__icon">
                    <svg class="icon icon_cart-personal">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#cart-personal"></use>
                    </svg>                                        
                </div>
                <h2>Контактная информация</h2>
            </div>
            <div class="contact-form-input-group-type-client">
                <div class="contact-form-input">
                    <label>
                        <input type="radio" name="type_client" value="fiz" checked="on"/>
                        Физическое лицо
                    </label>
                </div>
                <div class="contact-form-input">
                    <label>
                        <input type="radio" name="type_client" value="jur" />
                        Юридическое лицо
                    </label>
                </div>
            </div>
            <div class="contact-form-input-group">
                <div class="contact-form-input">
                    <label>ФИО:</label>
                    <div class="contact-form-input-field">
                        <input type="text" name="contact-fio"  />
                    </div>
                    <span class="hint">Представьтесь, пожалуйста.</span>
                </div>
                <div class="contact-form-input">
                    <label>Эл. почта:</label>
                    <div class="contact-form-input-field">
                        <input type="text" name="contact-email"  />
                    </div>
                    <span class="hint">Пришлем письмо с информацией о заказе.</span>
                </div>
                <div class="contact-form-input">
                    <label>Телефон:</label>
                    <div class="contact-form-input-field">
                        <input type="text" name="contact-phone"  />
                    </div>
                    <span class="hint">Перезвоним, чтобы подтвердить заказ.</span>
                </div>
                <div class="contact-form-delivery-company-container">
                    <div class="contact-form-delivery-company-name">
                        <label>Название организации:</label><br />
                        <input type="text" name="organization-name"  id="organization-name" placeholder="Введите название организации или ИНН в свободной форме"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="delivery-container">
        <div class="row-delivery-info">
            <div class="row-contact-info-header">
                <div class="ordering-step-item__icon">
                    <svg class="icon icon_cart-car">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#cart-car"></use>
                    </svg>                                        
                </div>
                <h2>Варианты получения</h2>
            </div>
            <div class="contact-form-delivery">    
                <div class="contact-form-input-carrier_type">
                    <label>
                        <input type="radio" name="carrier_type" value="pickup" checked="on"/> Самовывоз - бесплатно
                    </label>
                </div>
                <div class="contact-form-input-carrier_type">
                    <label>
                        <input type="radio" name="carrier_type" value="delivery" /> Доставка
                    </label>
                </div>
                <div class="contact-form-input-address_info">
                    <div class="contact-form-delivery-address">
                        <label>Адрес доставки:</label><br />
                        <input type="text" name="contact-address"  id="contact-address" placeholder="Введите адрес в свободной форме"/>
                    </div>
                    <div class="contact-form-delivery-comment">
                        <label>Комментарий к заказу:</label><br />
                        <textarea></textarea>
                    </div>
                    <div class="contact-form-delivery-hint">
                        <span class="hint hint_pickup">Со склада в г. Санкт-Петербурге, ул. Железнодорожная 11, пос. Парголово - Схема проезда</span>
                        <span class="hint hint_delivery">Доставим заказ сегодня, если заявка сформирована до 15:00. Заказы, поступившие после 15:00, будут доставлены на следующий день.</span>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="payment-container">
        <div class="row-pay-info">
            <div class="row-contact-info-header">
                <div class="ordering-step-item__icon">
                    <svg class="icon icon_cart-wallet">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#cart-wallet"></use>
                    </svg>                                      
                </div>
                <h2>Способы оплаты</h2>
            </div>
            <div class="contact-form-input">
                <input type="radio" name="pay_method" value="cash" /> 
                Наличными
            </div>
            <div class="contact-form-input">
                <input type="radio" name="pay_method" value="yandex" /> 
                Онлайн банковской картой
            </div>
            <div>
                
            </div>
        </div>
    </div>
    <div class="pay_button">
        <button class="btn btn-success btn-send-order" role="submit">Оплатить</button>
    </div>
    <?php else: ?>
    Корзина пуста
    <?php endif; ?>
</div>