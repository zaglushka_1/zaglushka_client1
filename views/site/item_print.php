<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use app\models\ItemsModel;

AppAsset::register($this);
JqueryAsset::register($this);

$additionalPrices = [
    0 => 0,
    1 => 20,
    2 => 30,
    7 => 35,
    8 => 40,
    9 => 10,
    3 => 50,
    4 => 70,
    5 => 100,
    6 => 200
];
?>
<table class="header">
    <tbody><tr class="header__row">
        <td class="header__logo"><img src="https://zaglushka.ru/images/logo_print.png"></td>
        <td class="header__address">
            <strong>ОТДЕЛ ПРОДАЖ</strong>
            <br>
            <strong>тел. 7 (812) 242-80-99</strong>
            <br>
            <strong>тел. 7 (800) 555-04-99</strong>
            <br>
            <br>
            <p>
                <strong>АДРЕС СКЛАДА:</strong>
                <br>
                г. Санкт-Петербург, п. Парголово ул. Подгорная, дом 6. Въезд с ул. Железнодорожная, дом 11 На территории АНГАР № 12 График работы: Пн-Пт, 9:00 - 18:00
            </p>
        </td>
        <td class="header__contacts">

        </td>
    </tr>
    </tbody>
</table>
<main>
    <?php

    if(substr($catalog_element['item']['id'], -2)=="00") {
        $imagePath="/images/item/0/".$catalog_element['item']['id']."/";
    } else {
        $imagePath="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/";
    }
    ?>
    <table class="images" style="width:100%;">
        <tbody>
        <tr>
            <td rowspan="2" class="images__large">
                <?php $img="{$imagePath}1.jpg"; ?>
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
            <td class="images__small">
                <?php $img="{$imagePath}6.jpg"; ?>
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
            <td rowspan="2" class="images__large">
                <?php $img="{$imagePath}4.jpg"; ?>
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
            <td rowspan="2" class="images__large">
                <?php $img="{$imagePath}9.jpg"; ?>
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td class="images__small">
                <?php $img="{$imagePath}7.jpg"; ?>
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
        </tr>
        </tbody>
    </table>

    <p>
        <?= $catalog_element['item']['short_descr']; ?>
    </p>

    <table class="list" style="width: 100%;">
        <thead>
        <tr>
            <th rowspan="2">Артикул</th>
            <th rowspan="2">Цвет</th>
            <th colspan="6">Цена, руб. за 1 шт.</th>
            <th rowspan="2">Упаковка</th>
            <th rowspan="2">Наличие на складе на <?= date('d.m.Y', mktime()); ?></th>
        </tr>
        <tr>
            <th>более 10 000</th>
            <th>от 5 000 до 10 000</th>
            <th>от 1 000 до 5 000</th>
            <th>от 500 до 1 000</th>
            <th>от 100 до 500</th>
            <th>розница до 100</th>
        </tr>
        </thead>
        <tbody>
            <?php if($catalog_element['item']['colors']): ?>
                <?php foreach($catalog_element['item']['colors'] as $color): ?>
                    <?php if((int)$color['remains']>0): ?>
                        <tr>
                            <td><?= $color['code']; ?></td>
                            <td><?= $color['color_name']; ?></td>
                            <td>
                                <?= number_format($catalog_element['item']['price5']*(1+($additionalPrices[$color['add_price']])/100), 2, ',', ' '); ?>
                            </td>
                            <td>
                                <?= number_format($catalog_element['item']['price4']*(1+($additionalPrices[$color['add_price']])/100), 2, ',', ' '); ?>
                            </td>
                            <td>
                                <?= number_format($catalog_element['item']['price3']*(1+($additionalPrices[$color['add_price']])/100), 2, ',', ' '); ?>
                            </td>
                            <td>
                                <?= number_format($catalog_element['item']['price2']*(1+($additionalPrices[$color['add_price']])/100), 2, ',', ' '); ?>
                            </td>
                            <td>
                                <?= number_format($catalog_element['item']['price1']*(1+($additionalPrices[$color['add_price']])/100), 2, ',', ' '); ?>
                            </td>
                            <td>
                                <?= number_format($catalog_element['item']['price0']*(1+($additionalPrices[$color['add_price']])/100), 2, ',', ' '); ?>
                            </td>
                            <td>
                                <?= $catalog_element['item']['pack_size']; ?>
                            </td>
                            <td>
                                <strong><?= $color['remains']; ?> шт.</strong></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach;?>
            <?php endif; ?>
        </tbody>
    </table>




    <p>Все цены указаны в рублях и включают в себя НДС.</p>
    <p>Все цены указаны при условии 100% предоплаты и действительны в течение 5 дней.</p>
    <p>Все цены указаны при условии самовывоза продукции со склада в г. Санкт-Петербург</p>
    <p>(для иногородних клиентов доставка до склада ТК в г. Санкт-Петербург бесплатна).</p>
    <p>Все цены указаны за стандартный цвет для каждого вида изделия</p>
    <p>(изменение цвета на любой из предлагаемой палитры +30% к стоимости, изменение цвета на любой по системе RAL +50% к стоимости).</p>
    <p>О возможных сроках поставки заказного товара, необходимо уточнять заранее.</p>
</main>