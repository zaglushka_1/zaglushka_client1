<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О Компании';
?>
<div class="site-about">
<style>
        
        a.tag {display: inline-block; padding:10px 15px; background: white; border-radius:5px; color:black; text-decoration:none; margin: 0 10px; font-size:18px;line-height: 18px;transition-duration: .2s;transition-timing-function:ease-in;transition-property:background-color, color;}
        a.tag span.num {color: #969696; display: inline-block; margin-left: 10px; font-size: 16px;font-weight: bold;}
        a.tag.selected, a.tag:hover {color: white; background: #c54004;}
        a.tag.selected {cursor: default;}
        .page-header h1 {margin: 50px; margin-bottom:30px;}
        .intro_text p {text-align: center;font-size: 1.15em;line-height: 1.75em;}
        .intro_text a.tag {}
        .cloud_tag { margin: 60px 0; display: flex; align-content: center; flex-wrap: wrap; justify-content: center; margin-top:40px;}
        .cloud_tag a {margin-top:10px; margin-bottom:10px;}
        .filter_events {display: flex; justify-content: space-between;}
        .filter_events_left {flex-basis: 45%; width: 45%;margin-right: 25px;margin-top:30px; transition: .3s;}
        .filter_events_left.expand {flex-basis: 75%; width:75%;}
        .filter_events_center {flex-basis: 10%; width: 10%; background: url('https://zaglushka.ru/images/managers/bg-vertical-line.png') top center repeat-y;text-align: center;}
        .filter_events_center .year_div {margin: 0 auto; display: inline-block;background-color:#01997c;color:white;border-radius:5px;padding: 7px 25px;}
        .filter_events_center .date_div {width: calc(100% + 35px); position:relative;z-index:2;display:flex; justify-content: left;margin-top:50px;}
        .filter_events_center .date_div .pointer{display: block; width: 20px; height:20px; border-radius:10px; border:3px solid #ebebeb;background: #01997c; margin-top:35px;}
        .filter_events_center .date_div.left {left:-35px;}
        .filter_events_center .date_div.left {margin-top: 150px;}
        .filter_events_center .date_div.left.first {margin-top:50px;}
        .filter_events_center .date_div.right {margin-left:-25px;right:-35px;}
        .filter_events_center .date_div.left .pointer {}
        .filter_events_center .date_div .date_outer {border:5px solid #01997c; width:90px; height:90px; background-color:white; border-radius:100px;}
        .filter_events_center .date_div.left .date_outer {margin-left: 25px;}
        .filter_events_center .date_div.right .date_outer {margin-right: 25px;}
        .filter_events_center .date_div .date_outer a {color: #969696;font-size:24px;font-weight:bold; text-decoration:none;display:block;width: 90px; height: 90px;margin-left: -5px;padding-top: 32px;line-height:20px;}
        .filter_events_center .date_div .date_outer a span {font-size: 14px;color:#01997c;}
        
        .filter_events_right {flex-basis: 45%;  width: 45%;margin-left: 25px;}
        
        .filter_events section {background: #fff;-webkit-box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75);-moz-box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75);box-shadow: 0px 0px 10px 0px rgba(50, 50, 50, 0.75); margin: 50px 0;}
        
        .filter_events section .text {padding:20px;}
        .filter_events section .text .btn {transition-duration: .2s;transition-timing-function:ease-in;transition-property:background-color, color;}
        .filter_events section .text .btn:hover {background: #c54004;border-color:#c54004;}
        
        .filter_events .video-frame{/*max-height:360px;*/}
        .video_hover.video_hover_button_play {cursor: pointer;}
        div.hide_text {display:none;transition:3s;transform-origin: bottom;}
        .filter_event {overflow-x:hidden;}
        @media (max-width: 720px) {
            .d-sm-none {display:none;}
            .filter_events {flex-basis: 100%; flex-flow: wrap;}
            .filter_events_left, .filter_events_right {width:100%; flex-basis: 100%; margin-right: 0; margin-top: 0px !important;margin-left: 0px; margin-bottom: 20px;} 
            .filter_events section {margin-bottom: 0px;}
            .video_hover.video_hover_button_play {margin-top: 40px !important; margin-bottom: -90px}
        }
    </style>
    <script>
    </script>
    <div class="container">
        <div class="page-header">
                <h1 class="page__title" style="text-align:center; margin-bottom:0px;">О компании</h1>
        </div>
        <div class="page">
            <div class="page__content page__content_text">
                <div class="intro_text">
                    <p>Наша компания успешно работает на рынке уже почти 30 лет! Мы постоянно развиваемся и стремимся сделать сотрудничество с нами максимально удобным на всех этапах работы. На данный момент мы являемся крупнейшим производителем и поставщиком пластиковой фурнитуры, комплектующих для детских и спортивных площадок в России и СНГ. За годы успешной работы наша компания завоевала отличную репутацию, зарекомендовала себя как надёжный деловой партнёр и ответственный производитель.</p>
                    <p style="line-height:2.75em; font-size:1.1em;"><!--Наша компания успешно работает на российском рынке уже более 25 лет и на данный момент является крупнейшим в России -->
                        
                        <!--<a href="#" class="tag selected">производителей <span class="num">124</span></a> и <a href="#" class="tag">поставщиков <span class="num">24</a></a> <a href="#" class="tag">пластиковой фурнитуры <span class="num">86</a></a></p> -->
                </div>
                <!--<div class="cloud_tag">
                    <a href="#" class="tag">показать всё <span class="num">1</span></a> 
                    <a href="#" class="tag selected">Производство <span class="num">1</span></a>
                </div> -->
                <div class="filter_events" style="margin-top:50px;">
                    <div class="filter_event filter_events_left">
                        
                        <section data-link="date-2019-april">
                            <div class="video-frame" style="background: url('/images/play.gif') center center no-repeat;">
                                <a class="js-modal-btn" data-video-id="FYOejvfnRxQ">
                                    <img class="video_hover video_hover_img img-fluid" src="/images/five.jpg" style="width:100%;">
                                    <img class="video_hover video_hover_button_play" src="/images/play.gif" style="position: relative; width:auto; background:transparent;top: -150px; left: calc(50% - 25px);"/>
                                </a>
                            </div>
                            <div class="text">
                                <h3>СКЛАД КОМПАНИИ "ЗАГЛУШКА.РУ"</h3>
                                <p>Быстрая доставка продукции клиенту всегда была одним из приоритетов работы компании «Заглушка.Ру». Правильная организация складской логистики – гарант нашей слаженной и результативной работы. Четкость и оперативность учета продукции, эффективное использование складских помещений, доступ к ним и рациональное планирование складских операций связано с точностью работы всей компании. </p>
                                <div class="hide_text">
                                    <h3>Вместительность и удобное расположение</h3>
                                    <p>Площадь складских помещений составляет около 8000 м², что позволяет грамотно разместить весь ассортимент нашей продукции и существенно сократить период доставки товара до адресата. </p>
                                    <h3>Техническое оснащение</h3>
                                    <p>Склад оснащен современным грузоподъемным и транспортным оборудованием – электоштабелерами и погрузчиками различного вида.</p>
                                    <h3>Адресное хранение продукции – всему свое место</h3>
                                    <p>Для автоматизации и оптимизации работы мы используем адресную систему хранения товара. Она обеспечивает четкую, логичную нумерацию мест хранения и существенно упрощает работу.  </p>
                                    <h3>Регулярное пополнение</h3>
                                    <p>Собственное производство постоянно обеспечивает склад продукцией, поэтому 90% представленных в каталоге изделий всегда в наличии. </p>
                                </div>
                                <a href="" class="btn btn-more">Подробнее&nbsp;&nbsp;&nbsp;></a>
                            </div>
                        </section>
                        
                        <section data-link="date-2019-april">
                            <div class="video-frame" style="background: url('/images/play.gif') center center no-repeat;">
                                <a class="js-modal-btn" data-video-id="8BbPL9SE63U">
                                    <img class="video_hover video_hover_img img-fluid" src="/images/third_post.png" style="width:100%;">
                                    <img class="video_hover video_hover_button_play" src="/images/play.gif" style="position: relative; width:auto; background:transparent;top: -150px; left: calc(50% - 25px);"/>
                                </a>
                                <!--<iframe style="display: none;" width="100%" height="315" src="https://www.youtube.com/embed/hlYxg1RBOpw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                            </div>
                            <div class="text">
                                <h3>ПРОИЗВОДСТВО ПЛАСТИКОВЫХ ИЗДЕЛИЙ</h3>
                                <p>Производство и продажа изделий из пластика – то, с чего началась наша компания. Это та сфера деятельности, которой мы занимаемся с удовольствием и знаем все ее особенности. За 30 лет работы из небольшой фирмы мы превратились крупную компанию с большими производственными мощностями, для которой не существует нерешаемых задач.</p>                             
                                <div class="hide_text">
                                    <h3>Технологии производства и оборудование</h3>
                                    <p>Наш цех оснащен современным оборудованием, которое отвечает европейским стандартам качества – термопластавтоматами для единичного и массового производства. Мы применяем разные технологии производства изделий (литье под давлением, ротоформование, вакуумную формовку), используем все виды полимерных материалов. Мы работаем на собственных пресс-формах, производим новые пресс-формы под ваш заказ, а также можем изготовить изделия на ваших пресс-формах.</p>
                                    <h3>Ассортимент пластиковых изделий</h3>
                                    <p>Мы постоянно работаем над расширением ассортимента: разрабатываем новые изделия, вносим изменения в старые модели для улучшения их визуальных и технических характеристик, изготавливаем продукцию под заказ по индивидуальным запросам, в том числе с рельефным логотипом. Мы гарантируем полное соответствие заявленным параметрам, так как на каждом этапе производства реализуем тщательный контроль всех процессов.</p>
                                    <p>Производство пластиковых <a href="/zaglushki-dlya-trub" target="_blank">заглушек</a>, <a href="/plastikovye-opory" target="_blank">опор</a>, <a href="/plastikovye-fiksatory" target="_blank">фиксаторов</a>, <a href="/plastikovye-kolpachki" target="_blank">колпачков</a> – это не единственное направление деятельности нашей компании. Мы предлагаем различные комплектующие для обустройства <a href="/komplektuyushchie-dlya-maf" target="_blank">детских и спортивных площадок</a>, а также широкий ряд пластиковой фурнитуры, использующейся в <a href="/mebelnye-komplektuyushchie" target="_blank">мебельной индустрии</a>.</p>
                                    <p>Собственное производство постоянно обеспечивает склад продукцией, поэтому 90% представленных в каталоге изделий всегда в наличии. Сотрудничая с нами, вы получаете продукцию в кратчайшие сроки по конкурентоспособной цене.</p>
                                </div>
                                <a href="" class="btn btn-more">Подробнее&nbsp;&nbsp;&nbsp;></a>
                            </div>
                        </section>
                        
                        <section data-link="date-2019-april">
                            <div class="video-frame" style="background: url('/images/play.gif') center center no-repeat;">
                                <a class="js-modal-btn" data-video-id="vvw5km932W0">
                                    <img class="video_hover video_hover_img img-fluid" src="/images/second_post.jpg" style="width:100%;">
                                    <img class="video_hover video_hover_button_play" src="/images/play.gif" style="position: relative; width:auto; background:transparent;top: -150px; left: calc(50% - 25px);"/>
                                </a>
                                <!--<iframe style="display: none;" width="100%" height="315" src="https://www.youtube.com/embed/hlYxg1RBOpw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                            </div>
                            <div class="text">
                                <h3>АЛЮМИНИЕВЫЕ ВТУЛКИ</h3>
                                <p>Обжимные втулки: ассортимент и особенности производства</p>
                                <p>Обжимные втулки, которые используются в детских игровых комплексах, мы разрабатываем и изготавливаем самостоятельно. Наличие собственного конструкторского отдела и производственного цеха металлообработки позволяет нам создавать изделия, в которых учтены все нюансы и особенности дальнейшего применения.
                                <p>С ассортиментом наших обжимных втулок можно ознакомиться <a href="https://zaglushka.ru/obzhimnye-gilzyvtulki">здесь</a></p>
                                
                                <div class="hide_text">
                                    <div style="display: flex;">
                                    <p>Втулки используются для обжима отрезков каната или троса. Это позволяет исключить возможное расплетание прядей каната и увеличить максимальную нагрузку на соединение. Мы применяем эти изделия в детских канатных конструкциях. Обжимаем втулки на собственных гидравлических и механических пресс-машинах разного вида. Для каждого изделия используем индивидуальные матрицы, в зависимости от материала и размера втулки, – конические, цилиндрические, специальные и др.</p>
                                    <div>
                                        <img class="video_hover video_hover_img img-fluid" src="/images/image_post_2.jpg" style="width:100%;">
                                    </div>
                                    </div>
                                    <h3>Особенности производства</h3>
                                    <p>Современный станочный парк (фрезерное, токарное и виброгалтовочное оборудование) позволяет выполнять полный цикл производства – мы подходим к изготовлению деталей комплексно. Готовое изделие проходит проверку на собственном испытательном стенде – мы гарантируем качество и безопасность каждой втулки.</p>
                                    <p>Также мы работаем под заказ – изготавливаем изделие любой сложности, учитывая ваши пожелания.</p>
                                </div>
                                <a href="" class="btn btn-more">Подробнее&nbsp;&nbsp;&nbsp;></a>
                            </div>
                        </section>

                    </div>
                    
                    <div class="filter_events_center d-sm-none">
                        <div class="year_div">2019</div>
                        <div class="date_div left first active">
                            <div class="pointer"></div>
                            <div class="date_outer">
                                <a href="">ДЕК<br /><!-- <span>АВГ</span>--></a>
                            </div>
                        </div>
                        <div class="date_div right">
                            <div class="date_outer">
                                <a href="">НОЯ<br /><!-- <span>АВГ</span>--></a>
                            </div>
                            <div class="pointer"></div>
                        </div>
                        <div class="date_div left" style="margin-top: 490px;">
                            <div class="pointer"></div>
                            <div class="date_outer">
                                <a href="">ОКТ<br /><!--<span>АПР</span>--></a>
                            </div>
                        </div>
                        <div class="date_div right" style="">
                            <div class="date_outer">
                                <a href="">СЕНТ<br /><!--<span>АПР</span>--></a>
                            </div>
                            <div class="pointer"></div>
                        </div>
                        <div class="date_div left" style="margin-top: 490px;">
                            <div class="pointer"></div>
                            <div class="date_outer">
                                <a href="">АВГ<br /><!--<span>АПР</span>--></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="filter_event filter_events_right" style="margin-top:150px;">
                        
                        <section data-link="date-2019-april">
                            <div class="video-frame" style="background: url('/images/play.gif') center center no-repeat;">
                                <a class="js-modal-btn" data-video-id="wELVRrA7tZE">
                                    <img class="video_hover video_hover_img img-fluid" src="/images/four_post.jpg" style="width:100%;">
                                    <img class="video_hover video_hover_button_play" src="/images/play.gif" style="position: relative; width:auto; background:transparent;top: -150px; left: calc(50% - 25px);"/>
                                </a>
                            </div>
                            <div class="text">
                                <h3>ОБЖИМ АЛЮМИНИЕВЫХ ВТУЛОК</h3>
                                <p>Наша компания уже несколько десятилетий занимается производством канатных конструкций для детских игровых площадок. Изготовление такого игрового оборудования предполагает несколько этапов, одним из которых является опрессовка отрезков каната. Она позволяет исключить расплетание прядей каната и увеличить максимальную нагрузку на соединение. </p>
                                <div class="hide_text">
                                    <p>Обжим втулок осуществляется на собственном опрессовочном оборудовании – гидравлических и механических пресс-машинах разного вида и диапазона. Все оборудование отвечает мировым стандартам и разработано для эффективной, качественной и безопасной опрессовки. </p>
                                    <p>Диапазон прессового усилия составляет от 40 до 550 тонн, что позволяет опрессовывать алюминиевые втулки для канатов разного диаметра. После обжатия втулки выдерживают нагрузку на соединение до 20 kN в зависимости от модели изделия. Тип пресса влияет на способ обжатия – чаще всего это или прямое, или радиальное.</p>

                                    <p>Для каждой <a href="https://zaglushka.ru/obzhimnye-gilzyvtulki" target="_blank">обжимной втулки</a> (в зависимости от ее размера, материала) и вида опрессовки используются индивидуальные матрицы – конические, цилиндрические, специальные и др. Возможно изготовление и обжим втулок с логотипом компании-заказчика – в этом случае также подбирается определенная матрица.</p>
                                </div>
                                <a href="" class="btn btn-more">Подробнее&nbsp;&nbsp;&nbsp;></a>
                            </div>
                        </section>
                        
                        <section data-link="date-2019-april">
                            <div class="video-frame" style="background: url('/images/play.gif') center center no-repeat;">
                                <a class="js-modal-btn" data-video-id="cVf1_7Xd-8I">
                                    <img class="video_hover video_hover_img img-fluid" src="/images/first_post.png" style="width:100%;">
                                    <img class="video_hover video_hover_button_play" src="/images/play.gif" style="position: relative; width:auto; background:transparent;top: -150px; left: calc(50% - 25px);"/>
                                </a>
                            </div>
                            <div class="text">
                                <h3>МЕТАЛЛООБРАБОТКА</h3>
                                <p>Станочный парк</p>
                                <p>Цех по работе с металлом – хорошо налаженное, современное производство, оснащенное передовым оборудованием. В нашем распоряжении большой станочный парк: токарное, фрезерное, ленточнопильное, сверлильное, виброгалтовочное, шлифовальное, электроэрозионное оборудование, инверторные сварочные аппараты (под аргон и полуавтоматы), лазерные и плазменные труборезы, трубогибы и др. </p>
                                <div class="hide_text">
                                    <p>Автоматизация производства (наличие сварочных роботов, станков с ЧПУ) обеспечивает постоянное увеличение объема, точность обработки деталей и безопасность. </p>
                                    <h3>Антикоррозионное покрытие металла</h3>
                                    <p>Для того, чтобы металлические изделия имели антикоррозионную стойкость, на них наносится защитное покрытие. В нашем цехе металлообработки осуществляют следующие виды антикоррозийной обработки металла: цинкование (горячее, гальваническое, термодиффузионное), порошковая покраска, прорезинивание (пластизоль на основе ПВХ, полиуретан). В некоторых случаях мы используем оба вида покрытия одновременно – например, сначала цинкование, затем покраску.</p>
                                    <h3>Ассортимент изделий из металла</h3>
                                    <p>Наш цех металлообработки создавался для изготовления комплектующих, использующихся в детских игровых комплексах и спортивных площадках. Изделия, которые здесь производят, – собственные разработки компании. Здесь изготавливаются обжимные втулки, хомуты, элементы кручения, крепления для подвесных качелей, цепи, крепежная фурнитура, собственные пресс-формы. Все изделия проходят сертификацию на соответствие стандартам. Наша компания имеет все регламентирующие документы, подтверждающие безопасность производимого оборудования.</p>
                                    <p>Подробнее с ассортиментом нашей продукции из металла можно ознакомиться <a href="https://zaglushka.ru/komplektuyushchie-dlya-maf" target="_blank">здесь</a></p>
                                </div>
                                <a href="" class="btn btn-more">Подробнее&nbsp;&nbsp;&nbsp;></a>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
