<div class="search-filter-container">
    <div class="search-filter-container-item">
        <span class="filter_name">Круглая</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-1', ['catalog_element'=>$filter[1]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name">Квадратная</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-2', ['catalog_element'=>$filter[2]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name"> ДУ (DN)</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-10', ['catalog_element'=>$filter[10]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name">Прямоугольная</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-3', ['catalog_element'=>$filter[3]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name">Овальная</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-4', ['catalog_element'=>$filter[4]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name">Эллипсоидная</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-9', ['catalog_element'=>$filter[9]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name">Полуовальная</span>
        <div class="search_filter_input">
            <?= $this->render('filters/filter-type-5', ['catalog_element'=>$filter[5]]); ?>
        </div>
    </div>
    <div class="search-filter-container-item">
        <span class="filter_name">Отверстие</span>
            <div class="search_filter_input">
        <?= $this->render('filters/filter-type-11', ['catalog_element'=>$filter[11]]); ?>
        </div>
    </div>
</div>