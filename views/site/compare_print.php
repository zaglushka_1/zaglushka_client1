<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use app\models\ItemsModel;

AppAsset::register($this);
JqueryAsset::register($this);

$additionalPrices = [
    0 => 0,
    1 => 20,
    2 => 30,
    7 => 35,
    8 => 40,
    9 => 10,
    3 => 50,
    4 => 70,
    5 => 100,
    6 => 200
];
?>
<table border="0">
    <tr>
        <td rowspan="9"><img src="https://zaglushka.ru/images/logo_print.png" /></td>
        <td>
            <p><strong>ОТДЕЛ ПРОДАЖ</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>тел. 7 (812) 242-80-99</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>тел. 7 (800) 555-04-99</strong></p></td>
    </tr>
    <tr>
        <td>
            <p></td>
    </tr>
    <tr>
        <td>
            <p><strong>АДРЕС СКЛАДА</strong></p></td>
    </tr>
    <tr>
        <td><p>г. Санкт-Петербург, п. Парголово ул. Подгорная</p></td>
    </tr>
    <tr>
        <td><p>дом 6. Въезд с ул. Железнодорожная, дом 11</p></td>
    </tr>
    <tr>
        <td><p>На территории АНГАР № 12</p></td>
    </tr>
    <tr>
        <td><p>График работы: Пн-Пт, 9:00 - 18:00</p></td>
    </tr>
</table>
<div class="container">
    <?php foreach ($catalog_element['catalog_items'] as $key => $Item): ?>
        <?php $item=ItemsModel::find()->where(['id'=>$Item['id']])->one(); ?>
        <table class="compare-images" style="width: 100%;">
            <tbody>
            <?php

                if(substr($Item['id'], -2)=="00") {
                    $imagePath="/images/item/0/".$Item['id']."/";
                } else {
                    $imagePath="/images/item/".substr($Item['id'], -2)."/".$Item['id']."/";
                }
            ?>
            <tr>
                <td>
                    <?php $img="{$imagePath}1.jpg"; ?>
                    <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                        <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                    <?php else: ?>
                        <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                    <?php endif; ?>
                </td>
                <td>
                    <?php $img="{$imagePath}2.jpg"; ?>
                    <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                        <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                    <?php else: ?>
                        <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                    <?php endif; ?>
                </td>
                <td>
                    <?php $img="{$imagePath}3.jpg"; ?>
                    <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                        <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                    <?php else: ?>
                        <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                    <?php endif; ?>
                </td>
                <td>
                    <?php $img="{$imagePath}4.jpg"; ?>
                    <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                        <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                    <?php else: ?>
                        <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                    <?php endif; ?>
                </td>
                <td>
                    <?php $img="{$imagePath}9.jpg"; ?>
                    <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                        <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                    <?php else: ?>
                        <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                    <?php endif; ?>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <?= $item->short_descr; ?>
                </td>
            </tr>
            </tbody>
        </table>
        <?php if($item->itemColors): ?>
            <table class="list">
                <thead>
                <tr>
                    <th rowspan="2">№</th>
                    <th rowspan="2">Артикул</th>
                    <th rowspan="2">Цвет</th>
                    <th colspan="6">Цена, руб. за 1 шт.</th>
                    <th rowspan="2">Упаковка</th>
                    <th rowspan="2">Наличие на складе на 28.01.2021</th>
                </tr>
                <tr>
                    <th>более 10 000</th>
                    <th>от 5 000 до 10 000</th>
                    <th>от 1 000 до 5 000</th>
                    <th>от 500 до 1 000</th>
                    <th>от 100 до 500</th>
                    <th>розница до 100</th>
                </tr>
                </thead>
                <tbody>
                <?php $i=1; ?>
                <?php foreach($item->itemColors as $color): ?>
                    <tr>
                        <?php $addPrice=1+((int)$color->add_price/100); ?>
                        <td><?= $i; ?></td>
                        <td><?= $color->code; ?>></td>
                        <td><?= $color->colors->name; ?></td>
                        <td>
                            <?php if($item->price5): ?>
                                <?= $item->price5*$addPrice; ?> р.
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($item->price4): ?>
                                <?= $item->price4*$addPrice; ?> р.
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($item->price3): ?>
                                <?= $item->price3*$addPrice; ?> р.
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($item->price2): ?>
                                <?= $item->price2*$addPrice; ?> р.
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($item->price1): ?>
                                <?= $item->price1*$addPrice; ?> р.
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php if($item->price0): ?>
                                <?= $item->price0*$addPrice; ?> р.
                            <?php endif; ?>
                        </td>
                        <td><?= ($item->pack_size) ? $item->pack_size." шт." : ''; ?></td>
                        <td><?= ($color->remains) ? $color->remains." шт." : '' ?></td>
                    </tr>
                <?php $i++; ?>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    <?php endforeach; ?>
</div>