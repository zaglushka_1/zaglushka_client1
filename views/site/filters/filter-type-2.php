<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
<?php $filter['size_1']=[]; ?>
<?php $filter['size_2']=[]; ?>
<?php $filter['size_3']=[]; ?>
<?php $filter['size_4']=[]; ?>
<?php $filter['size_5']=[]; ?>
<?php $filter['size_6']=[]; ?>
<?php foreach($catalog_element['filter'] as $filter_values): ?>
    <?php for($i=1;$i<=5;$i++): ?>
        <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
            <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endforeach; ?>
<?php $fitler_title=""; ?>
<?php $fitler_img=""; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>
<?php $filter_size_1=implode(",",$filter['size_1']); ?>
<?php $fitler_title="Укажите размер трубы"; ?>
<?php $fitler_img='<svg width="106" height="76" viewBox="0 0 106 76" xmlns="http://www.w3.org/2000/svg"><g class="element"  fill="none" fill-rule="evenodd"><path d="M31.389 23.525h6.687M20.204 29.97h7.985m13.088 0h7.984M9.019 36.431h7.985m35.457 0h7.985M2.98 42.891h7.986m47.534 0h5.75m-50.084 6.461h7.985m25.163 0h2.732m-24.696 6.46h7.986m-2.294-32.091h7.366m-22.234 8.574h7.984m21.149 0h7.984M1.29 40.884h7.985m50.9 0h7.984m-53.766 8.588h7.984m24.71 0h.951m2.129 0h4.905m-25.811 8.589h1.026m4.846 0h5.071m-9.947-33.902h8.936M22.77 28.491h7.985m7.955 0h7.985M19.69 32.823h3.578m22.929 0h7.985m-46.4 4.317h7.984m37.934 0h7.984M.52 41.472h3.684m56.756 0h7.984m-56.996 4.333h4.045m37.464 0h7.985M15.51 50.121h7.984m22.477 0h7.984m-30.959 4.333h7.985m7.489 0h7.983M30.498 58.77h8.47M.4 41.412l34.325-19.819" stroke="#FFF" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path stroke="#FFF" stroke-width=".8" stroke-linejoin="round" mask="url(#mask-2)" d="M.4 35V.638l34.325 19.804" transform="translate(0 40.774)"></path><path stroke="#FFF" stroke-width=".8" stroke-linejoin="round" mask="url(#mask-4)" d="M.725 35V20.442L35.05.638V35" transform="translate(34 40.774)"></path><path d="M34.725 21.593L69.05 41.412m-7.984 0L34.725 56.613 8.385 41.412l26.34-15.215v30.416m26.341-15.201L34.725 26.197" stroke="#FFF" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><g class="input_1"><path d="M34.725 21.593L53.08 10.996m15.97 30.416l18.355-10.596" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M39.465 4.928l-2.129 3.669 12.076 4.513"></path><path d="M38.393 6.755L27.389.4" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M95.798 37.457l-2.113 3.668-9.948-8.196"></path><path d="M94.742 39.283l11.004 6.355M83.738 32.929L49.413 13.133" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path></g></g></svg>'; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1="20 мм"; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>
<script>
    var filter_size_2_1=[<?= $filter_size_1; ?>];
</script>
<div class="cat-filter">
    <div class="cat-filter-heading">
        <span><?= $fitler_title; ?>:</span>
    </div>
    <div class="cat-filter-icon">
        <?= $fitler_img; ?>
    </div>
    <div class="cat-filter-params">
        <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
            <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
            <input class="filter" type="text" name="size_1" value="<?= (isset($_GET['size_1'])) ? $_GET['size_1'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_1; ?>" />
        </div>
        <div class="cat-filter-params-button">
            <button class="zag_button" type="submit">Подобрать</button>
        </div>
    </div>
</div>