<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
<?php $filter['size_1']=[]; ?>
<?php $filter['size_2']=[]; ?>
<?php $filter['size_3']=[]; ?>
<?php $filter['size_4']=[]; ?>
<?php $filter['size_5']=[]; ?>
<?php $filter['size_6']=[]; ?>
<?php foreach($catalog_element['filter'] as $filter_values): ?>
    <?php for($i=1;$i<=5;$i++): ?>
        <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
            <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endforeach; ?>
<?php $fitler_title=""; ?>
<?php $fitler_img=""; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>

<?php $filter_size_1=implode(",",$filter['size_1']); ?>
<?php $filter_size_2=implode(",",$filter['size_2']); ?>

<?php $fitler_title="Укажите размер<br />трубы"; ?>
<?php $fitler_img='<svg width="137" height="77" viewBox="0 0 137 77" xmlns="http://www.w3.org/2000/svg"><g class="element" fill="none" fill-rule="evenodd"><g stroke="#FFF" stroke-width=".8" stroke-linejoin="round"><path d="M72.678 21.278h5.737m-15.348 5.53h6.86m11.239 0h6.831m-34.513 5.561h6.832m30.432 0h6.861M43.872 37.9h6.861m51.904 0h.148M34.29 43.43h6.832m49.33 0h2.721m-54.417 5.559h6.861m32.651 0h5.323M48.367 54.52h6.625m12.156 0h6.86M57.95 60.05h6.447m7.985-38.625h6.33M59.606 28.82h6.86m18.16 0h1.301m4.141 0h1.39M46.86 36.183h6.83m43.682 0h6.861m-70.15 7.364h6.861m45.19 0h6.861m-50.868 7.365h4.555m26.706 0h6.832m-23.838 7.364h11.091m4.229-36.436h7.66m-14.078 3.697h6.832m-13.279 3.726h6.861m19.786 0h6.743M56.649 32.96h2.632m32.532 0h6.86m-52.671 3.726h6.831m45.22 0h4.052m-62.55 3.697h3.814m48.237 0h6.86M33.137 44.11h6.831m45.22 0h6.86m-55.304 3.696h6.861m35.135 0h6.862m-41.138 3.727h5.56m22.299 0h6.861M49.61 55.23h6.86m9.436 0h6.831m-16.68 3.726h2.81m-27.15-14.048l43.83-25.286 29.456 17.006" stroke-linecap="round"></path><path d="M61.173 61.914L31.717 44.908V76.79m73.286 0V36.629l-43.83 25.286m0-.001V76.79"></path><path stroke-linecap="round" d="M75.547 49.67V23.556L38.578 44.908 61.173 57.95l36.97-21.323-22.596-13.072"></path></g><g class="input_2"><path d="M31.717 44.908l-15.733-9.08m59.563-16.206l-15.734-9.108M28.582 32.191l24.932-14.403" stroke="currentColor"></path><path fill="currentColor" d="M8.767 41.508L10.6 44.67l8.547-7.038z"></path><path d="M9.684 43.104L.25 48.545m18.898-10.912l9.435-5.442" stroke="currentColor"></path><path fill="currentColor" d="M71.495 5.307L73.3 8.472l-10.352 3.874z"></path><path d="M72.382 6.876l9.464-5.442M62.947 12.346l-9.434 5.441" stroke="currentColor"></path></g><g class="input_1"><path d="M105.003 36.628l15.733-9.11m-23.156-9.73l10.558 6.093" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M79.599 5.307l-1.805 3.165 10.352 3.874z"></path><path d="M78.682 6.876l-9.435-5.442m18.899 10.912l9.434 5.441" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M127.953 33.226l-1.834 3.135-8.518-7.009z"></path><path d="M127.036 34.794l9.434 5.472m-18.868-10.914l-9.464-5.472" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path d="M91.28 10.513l-15.732 9.108" stroke="currentColor"></path></g></g></svg>'; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1="20 мм"; ?>
<?php $fitler_placeholder_size_2="40 мм"; ?>
<?php $fitler_placeholder_size_3=""; ?>
<script>
    var filter_size_3_1=[<?= $filter_size_1; ?>];
    var filter_size_3_2=[<?= $filter_size_2; ?>];
</script>
<div class="cat-filter">
    <div class="cat-filter-heading">
        <span><?= $fitler_title; ?>:</span>
    </div>
    <div class="cat-filter-icon">
        <?= $fitler_img; ?>
    </div>
    <div class="cat-filter-params">
        <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
            <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
            <input class="filter" type="text" name="size_1" value="<?= (isset($_GET['size_1'])) ? $_GET['size_1'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_1; ?>" />
            <input class="filter" type="text" name="size_2" value="<?= (isset($_GET['size_2'])) ? $_GET['size_2'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_2; ?>" />
        </div>
        <div class="cat-filter-params-button">
            <button class="zag_button" type="submit">Подобрать</button>
        </div>
    </div>
</div>