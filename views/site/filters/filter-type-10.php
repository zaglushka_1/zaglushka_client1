<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
        <?php $filter['size_1']=[]; ?>
        <?php $filter['size_2']=[]; ?>
        <?php $filter['size_3']=[]; ?>
        <?php $filter['size_4']=[]; ?>
        <?php $filter['size_5']=[]; ?>
        <?php $filter['size_6']=[]; ?>
    <?php foreach($catalog_element['filter'] as $filter_values): ?>
        <?php for($i=1;$i<=5;$i++): ?>
            <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
                <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
            <?php endif; ?>
        <?php endfor; ?>
    <?php endforeach; ?>
    <?php $fitler_title=""; ?>
    <?php $fitler_img=""; ?>
    <?php $filter_size_1_prepend=""; ?>
    <?php $fitler_placeholder_size_1=""; ?>
    <?php $fitler_placeholder_size_2=""; ?>
    <?php $fitler_placeholder_size_3=""; ?>

    <?php $filter_size_1=implode(",",$filter['size_1']); ?>

<?php $fitler_title="Выберите размеры ДУ"; ?>
<?php $fitler_img='<svg width="113" height="77" viewBox="0 0 113 77" xmlns="http://www.w3.org/2000/svg"><g class="element" fill="none" fill-rule="evenodd"><g stroke="#FFF" stroke-width=".8" stroke-linejoin="round"><path d="M6.25 37.535h14.241m22.002 0h14.24M1.517 45.528h10.399m39.114 0h10.398M3.65 53.522h11.72m32.206 0h6.558m-39.036 8.032h21.419m-9.118-31.78h8.188M3.65 40.406h11.72m32.206 0h11.758M4.31 51.077h8.77m36.785 0h10.826M15.525 61.71h20.76m2.638 0h8.498M22.898 30.356h17.15M8.617 35.71h3.104m8.033 0h9.157m5.161 0h20.256M3.224 41.066H14.67m38.65 0h6.44M11.722 46.42h.077m39.93 0h9.816m-51.414 5.356h3.492m35.738 0h11.02M7.104 57.13h15.52m21.11 0H55.88m-37.872 5.355H34.15m7.994 0h2.833M1.4 46.965l.078 1.24.233 1.243.388 1.203.543 1.203.659 1.203.816 1.125.97 1.126 1.086 1.047 1.242 1.008 1.358.97 1.436.932 1.59.815 1.669.815 1.746.699 1.863.62 1.9.544 1.98.504 2.057.389 2.095.309 2.096.194 2.134.156 2.134.039 2.173-.04 2.134-.155 2.096-.194 2.095-.31 2.018-.388 1.979-.504 1.94-.544 1.824-.62 1.785-.7 1.668-.814 1.553-.815 1.475-.931 1.357-.971 1.203-1.008 1.087-1.047.97-1.126.814-1.125.699-1.203.504-1.203.389-1.203.232-1.242.078-1.241-.078-1.242-.232-1.203-.39-1.242-.503-1.203-.7-1.164-.813-1.125-.97-1.126-1.087-1.048-1.203-1.046-1.357-.971-1.475-.893-1.553-.853-1.668-.777-1.785-.698-1.824-.62-1.94-.583-1.98-.465-2.017-.388-2.095-.311-2.096-.193-2.134-.156-2.173-.04-2.134.04-2.134.156-2.096.193-2.095.31-2.056.389-1.98.465-1.901.583-1.863.62-1.746.698-1.67.777-1.59.853-1.435.893-1.358.97-1.242 1.047-1.086 1.048-.97 1.126L3.3 40.91l-.66 1.164-.542 1.203-.388 1.242-.233 1.203-.078 1.242" stroke-linecap="round"></path><path stroke-linecap="round" d="M11.76 46.965l.078 1.009.234.97.387.969.504.97.66.894.777.892.93.854 1.048.775 1.164.737 1.28.66 1.359.622 1.435.503 1.553.466 1.59.388 1.63.31 1.669.195 1.708.116 1.707.078 1.707-.078 1.708-.116 1.707-.194 1.63-.311 1.59-.388 1.514-.466 1.474-.503 1.358-.622 1.243-.66 1.164-.737 1.047-.775.93-.854.778-.892.659-.893.544-.971.348-.97.234-.97.077-1.008-.077-.971-.234-1.008-.348-.97-.544-.932-.66-.931-.776-.854-.931-.854-1.047-.776-1.164-.737-1.243-.66-1.358-.62-1.474-.543-1.513-.427-1.591-.39-1.63-.308-1.707-.195-1.708-.155-1.707-.04-1.707.04-1.708.155-1.668.195-1.63.309-1.591.389-1.553.427-1.435.543-1.358.62-1.281.66-1.164.737-1.048.776-.93.854-.777.854-.66.93-.504.933-.387.97-.234 1.008-.077.97"></path><path d="M1.4 76.033V46.965m60.145 29.068V46.965"></path></g><g class="input_1"><path d="M17.542 38.931l44.003-25.377M45.404 55.035l44.002-25.416m9.081 10.515l13.659 7.877M43.347 8.277L29.727.4m55.139 31.858l-27.86-16.104" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M99.806 37.846l-2.64 4.54-12.3-10.129zm-55.14-31.82l-2.639 4.54 14.978 5.588z"></path></g></g></svg>'; ?>
<?php $filter_size_1_prepend="ДУ "; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>

<div class="cat-filter">
    <div class="cat-filter-heading">
        <span><?= $fitler_title; ?>:</span>
    </div>
    <div class="cat-filter-icon">
        <?= $fitler_img; ?>
    </div>
    <div class="cat-filter-params">
        <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
            <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
            <select class="filter" name="size_1" placeholder="<?= $fitler_placeholder_size_1; ?>">
                <option <?= (!isset($_GET['size_1']) || strlen($_GET['size_1'])==0) ? 'selected' : ''; ?>><?= $fitler_placeholder_size_1; ?></option>
                <?php if(isset($filter['size_1']) and sizeof($filter['size_1'])>0): ?>
                    <?php foreach($filter['size_1'] as $filter_element): ?>
                        <?php if(isset($_GET['size_1'])): ?>
                            <?php $size_1=str_replace("%20", " ", $_GET['size_1']);?>
                        <?php endif; ?>
                        <option <?= (isset($_GET['size_1']) && str_replace("'","",$filter_element)==$size_1) ? 'selected' : ''; ?> value="<?= htmlspecialchars(str_replace("'","",$filter_element),ENT_COMPAT|ENT_QUOTES); ?>"><?= $filter_size_1_prepend; ?><?= str_replace("'", "", $filter_element); ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
        </div>
        <div class="cat-filter-params-button">
            <button class="zag_button" type="submit">Подобрать</button>
        </div>
    </div>
</div>
