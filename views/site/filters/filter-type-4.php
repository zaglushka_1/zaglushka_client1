<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
<?php $filter['size_1']=[]; ?>
<?php $filter['size_2']=[]; ?>
<?php $filter['size_3']=[]; ?>
<?php $filter['size_4']=[]; ?>
<?php $filter['size_5']=[]; ?>
<?php $filter['size_6']=[]; ?>
<?php foreach($catalog_element['filter'] as $filter_values): ?>
    <?php for($i=1;$i<=5;$i++): ?>
        <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
            <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endforeach; ?>
<?php $fitler_title=""; ?>
<?php $fitler_img=""; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>

<?php $filter_size_1=implode(",",$filter['size_1']); ?>
<?php $filter_size_2=implode(",",$filter['size_2']); ?>

<?php $fitler_title="Укажите размер<br />трубы"; ?>
<?php $fitler_img='<svg width="155" height="77" viewBox="0 0 155 77" xmlns="http://www.w3.org/2000/svg"><g class="element"  fill="none" fill-rule="evenodd"><g class="input_2"><path d="M52.551 59.465L18 39.512m84.551-9.062L68 10.512M22 41.177l49.692-28.665" stroke="currentColor"></path><path fill="currentColor" d="M10 45.908l2.053 3.577 9.688-7.973"></path><path d="M10.699 47.512L0 53.703" stroke="currentColor"></path><path fill="currentColor" d="M80.673 4.512l2.053 3.577L71 12.485"></path><path d="M82 6.703L92.715.512" stroke="currentColor"></path></g><g stroke="#FFF" stroke-width=".8" stroke-linejoin="round"><path d="M74 28.512h23.933m-35.933 7h7.763m-18.763 6h7.603m45.397 0h5.502m-63.502 6h5.598m47.402 0h6.85m-58.85 6h5.822m35.178 0h7.763m-41.763 7h11.902m7.098 0h11.147M73 28.512h9.656m4.344 0h10.635m-39.635 9h7.764m37.236 0h5.694m-61.694 8h5.887m48.113 0h6.096m-60.096 8h5.854m35.146 0h7.779m-35.779 9h8.469m1.531 0h8.758m-1.758-33h22.022m-35.022 4h5.502m29.498 0h6.753m-48.753 4h7.779m38.221 0h5.631m-58.631 4h7.298m46.702 0h5.534m-62.534 5h5.774m48.226 0h6.256m-61.256 4h5.502m43.498 0h6.993m-54.993 4h5.983m34.017 0h.514m6.486 0h.963m-43.963 4h3.16m24.84 0h8.469m-24.469 5h12.655M53 39.927l16.297-9.415m-16.377 10l-1.187.723-1.075.785c-6.52 5.069-6.059 11.924.53 16.666l1.123.754 1.218.689c8.623 4.597 21.24 4.792 30.172.963l1.331-.626 1.268-.674" stroke-linecap="round"></path><path d="M86 77V59.927l16.312-9.415"></path><path d="M102.396 76.273V49.785l1.171-.722 1.075-.786c6.522-5.078 6.067-11.912-.529-16.666l-1.123-.754-1.219-.69c-8.594-4.59-21.256-4.796-30.156-.962l-1.347.626-1.268.673"></path><path d="M82 56.928l16.297-9.416m-41.99-5l-1.026.625-.915.69c-4.988 4.062-4.225 9.346.915 12.832l1.026.642 1.091.594c6.582 3.144 15.507 3.258 22.247.529l1.172-.529 1.106-.594" stroke-linecap="round"></path><path stroke-linecap="round" d="M57 57.717V42.928l16.297-9.416v27.076"></path><path d="M98.616 48.369l1.026-.625.914-.69c4.996-4.066 4.233-9.345-.914-12.832l-1.026-.642-1.091-.593c-6.543-3.147-15.546-3.26-22.248-.53l-1.171.53L73 33.58" stroke-linecap="round"></path><path d="M46 76.225V49.512m63 27.129V40.512"></path></g><g class="input_1"><path d="M69 30.45l34.534-19.938M102 50.45l34.534-19.938" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M90.069 4.512L88 8.089l11.741 4.396"></path><path d="M88.699 6.703L78 .512m22 13l37.006 21.416" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M144.741 36.907l-2.054 3.577L133 32.512"></path><path d="M144 38.512l10.522 6.079" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path></g></g></svg>'; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1="20 мм"; ?>
<?php $fitler_placeholder_size_2="40 мм"; ?>
<?php $fitler_placeholder_size_3=""; ?>
<script>
    var filter_size_4_1=[<?= $filter_size_1; ?>];
    var filter_size_4_2=[<?= $filter_size_2; ?>];
</script>
<div class="cat-filter">
    <div class="cat-filter-heading">
        <span><?= $fitler_title; ?>:</span>
    </div>
    <div class="cat-filter-icon">
        <?= $fitler_img; ?>
    </div>
    <div class="cat-filter-params">
        <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
            <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
            <input class="filter" type="text" name="size_1" value="<?= (isset($_GET['size_1'])) ? $_GET['size_1'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_1; ?>" />
            <input class="filter" type="text" name="size_2" value="<?= (isset($_GET['size_2'])) ? $_GET['size_2'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_2; ?>" />
        </div>
        <div class="cat-filter-params-button">
            <button class="zag_button" type="submit">Подобрать</button>
        </div>
    </div>
</div>