<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
<?php $filter['size_1']=[]; ?>
<?php $filter['size_2']=[]; ?>
<?php $filter['size_3']=[]; ?>
<?php $filter['size_4']=[]; ?>
<?php $filter['size_5']=[]; ?>
<?php $filter['size_6']=[]; ?>
<?php foreach($catalog_element['filter'] as $filter_values): ?>
    <?php for($i=1;$i<=5;$i++): ?>
        <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
            <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endforeach; ?>
<?php $fitler_title=""; ?>
<?php $fitler_img=""; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>

<?php $filter_size_1=implode(",",$filter['size_1']); ?>
<?php $filter_size_2=implode(",",$filter['size_2']); ?>

<?php $fitler_title="Укажите размеры<br />трубы"; ?>
<?php $fitler_img='<svg width="144" height="78" viewBox="0 0 144 78" xmlns="http://www.w3.org/2000/svg"><g class="element" fill="none" fill-rule="evenodd"><g class="input_2"><path d="M33.397 47.232L16.808 37.68m78.294-8.154L62.978 11.015M30.08 33.836l26.294-15.163" stroke="currentColor"></path><path fill="currentColor" d="M9.243 43.665l1.89 3.318 8.993-7.41z"></path><path d="M10.172 45.34l-9.92 5.736m19.874-11.504l9.954-5.736" stroke="currentColor"></path><path fill="currentColor" d="M75.289 5.527l1.922 3.317-10.914 4.093z"></path><path d="M76.25 7.17l9.954-5.737M66.296 12.937l-9.923 5.736" stroke="currentColor"></path></g><g stroke="#FFF" stroke-width=".8" stroke-linejoin="round"><path d="M68.466 27.572H90.7m-33.334 5.83h7.194m29.364 0h1.271m-47.937 5.86h7.193m41.923 0h5.085m-64.309 5.83h7.193m47.597 0h6.356m-58.511 5.829h7.225m34.852 0h7.225m-33.55 5.829h1.58m14.637 0h7.225m-18.977 5.862h8.868m-.9-34.852h23.226m-37.488 7.751h7.225m34.665 0h5.303M40.28 43.293h7.195m46.759 0h5.644m-59.907 7.752h7.225m35.286 0h6.388m-35.442 7.752h7.225m8.248 0h6.543m-8.559-30.636h.9m5.829 0H92.25m-32.558 3.908h7.193m31.256 0h.341m-45.581 3.907h6.697m37.365 0h4.124m-54.946 3.907h7.194m42.915 0h5.116M39.351 43.79h7.225m47.1 0h5.83m-65.302 3.938h7.225m48.526 0h4.683m-53.644 3.906h7.226m32.433 0h7.225m-40.124 3.907h7.225m18.883 0h2.078m-21.395 3.908h7.225m5.332 0h7.226m-13.024 3.907h6.232M33.397 47.232l30.667-17.706" stroke-linecap="round"></path><path d="M64.436 77.179V65.154L33.397 47.232v29.783"></path><path d="M95.102 47.448L64.436 65.153" stroke-linecap="round"></path><path d="M64.064 29.526l.62-.341.651-.31.651-.341.682-.28.713-.278.745-.28.744-.247.744-.248.775-.217.807-.187.774-.185.838-.155.806-.156.838-.124.837-.092.868-.093.837-.063.868-.061.838-.031h1.736l.868.03.837.062.868.063.837.093.838.092.837.124.837.156.807.155.805.185.776.187.775.217.775.248.745.248.712.279.713.279.683.279.682.342.62.309.651.34.59.373.558.372.558.372.527.404.496.403.434.434.465.434.403.435.372.433.341.465.311.466.28.465.247.465.217.496.185.496.156.466.124.496.093.496.031.496.031.496-.03.496-.032.496-.093.496-.124.496-.156.496-.185.496-.217.465-.248.496-.279.465-.31.466-.342.434-.372.465-.403.434-.465.435-.434.402-.496.403-.527.404-.558.403-.558.372-.59.34v29.73"></path><path stroke-linecap="round" d="M67.66 59.108V31.635L40.623 47.232l23.814 13.767 27.038-15.627.466-.28.434-.28.434-.308.373-.28.402-.342.342-.31.34-.34.31-.311.279-.34.28-.372.248-.341.216-.372.187-.372.155-.372.155-.373.093-.372.093-.372.062-.373.062-.372v-.774l-.062-.372-.062-.404-.093-.372-.093-.372-.155-.372-.155-.372-.187-.372-.216-.341-.248-.373-.28-.341-.28-.34-.31-.341-.34-.311-.341-.341-.402-.31-.373-.31-.434-.28-.434-.278-.466-.28-.464-.278-.496-.248-.528-.248-.527-.218-.527-.217-.558-.216-.558-.187-.59-.185-.588-.156-.621-.155-.62-.154-.62-.124-.62-.094-.651-.124-.652-.062-.65-.062-.651-.062-.652-.031-.651-.031h-1.334l-.652.03-.681.032-.652.062-.651.062-.62.062-.651.124-.652.094-.62.124-.62.154-.59.155-.588.156-.59.185-.557.187-.56.216-.556.217-.528.218-.496.248-.496.248-.496.279"></path><path d="M101.521 77.179V38.488"></path></g><g class="input_1"><path d="M64.064 29.526l32.093-18.511m-1.055 36.434l32.093-18.512m-24.402-10.264l11.132 6.42" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M83.846 5.527l-1.922 3.317 10.914 4.093z"></path><path d="M82.885 7.17l-9.953-5.737m19.906 11.504l9.954 5.736" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path><path fill="currentColor" d="M134.761 34.921l-1.892 3.318-8.992-7.41z"></path><path d="M133.8 36.596l9.953 5.736m-19.875-11.504l-9.953-5.736" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path></g></g></svg>'; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1="20 мм"; ?>
<?php $fitler_placeholder_size_2="40 мм"; ?>
<?php $fitler_placeholder_size_3=""; ?>
<script>
    var filter_size_5_1=[<?= $filter_size_1; ?>];
    var filter_size_5_2=[<?= $filter_size_2; ?>];
</script>
<div class="cat-filter">
    <div class="cat-filter-heading">
        <span><?= $fitler_title; ?>:</span>
    </div>
    <div class="cat-filter-icon">
        <?= $fitler_img; ?>
    </div>
    <div class="cat-filter-params">
        <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
            <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
                <input class="filter" type="text" name="size_1" value="<?= (isset($_GET['size_1'])) ? $_GET['size_1'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_1; ?>" />
                <input class="filter" type="text" name="size_2" value="<?= (isset($_GET['size_2'])) ? $_GET['size_2'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_2; ?>" />

        </div>
        <div class="cat-filter-params-button">
            <button class="zag_button" type="submit">Подобрать</button>
        </div>
    </div>
</div>