<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
<?php $filter['size_1']=[]; ?>
<?php $filter['size_2']=[]; ?>
<?php $filter['size_3']=[]; ?>
<?php $filter['size_4']=[]; ?>
<?php $filter['size_5']=[]; ?>
<?php $filter['size_6']=[]; ?>
<?php foreach($catalog_element['filter'] as $filter_values): ?>
    <?php for($i=1;$i<=5;$i++): ?>
        <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
            <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endforeach; ?>
<?php $fitler_title=""; ?>
<?php $fitler_img=""; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>

<?php $filter_size_1=implode(",",$filter['size_1']); ?>

    <?php $fitler_title="Укажите наружный диаметр трубы:"; ?>
    <?php $fitler_img='<svg width="85" height="67" viewBox="0 0 85 67" xmlns="http://www.w3.org/2000/svg">
        <g fill="none" fill-rule="evenodd">
            <g class="element" stroke-width=".8" stroke-linejoin="round" stroke="#000">
                <path d="M29.328 11.744h26.497m-37.186 6.951h6.852m34.172 0h6.852m-50.023 6.951h6.098m39.973 0h6.098m-48.402 6.951h7.576m29.482 0h7.577m-27.379 6.952h10.107M28.731 11.957h20.015m2.288 0h5.388m-39.248 9.24h6.312m38.167 0h6.312m-49.582 9.254h6.737m34.913 0h2.615m2.288 0h1.834" stroke-linecap="round"></path>
                <path d="M38.952 39.69h4.577"></path>
                <path d="M27.466 12.44h16.9m6.954 0h6.367M20.06 17.102h7.462m30.094 0h7.478M16.96 21.75h6.24m38.737 0h6.24m-51.572 4.664h6.127m39.688 0h6.128m-49.697 4.662h6.923m33.605 0h3.057m-37.785 4.648h1.635m9.737 0h.185m12.737 0h11.557m8.202-10.974l-.057-.98-.171-.981-.27-.98C65.065 13.282 52.402 9.886 44.282 9.697l-1.706-.03-1.705.03-1.706.099c-4.315.326-8.522 1.248-12.495 2.985l-1.322.626-1.251.682c-3.004 1.713-6.003 4.36-7.151 7.72l-.27.98-.17.98-.057.982.057.995.17.98c2.174 8.722 14.649 12.514 22.489 12.979l1.706.1 1.705.028c8.56.133 22.248-3.107 25.63-12.14l.27-.966.17-.981.058-.995z" stroke-linecap="round"></path>
                <path d="M22.518 24.75l.058.868.17.853.284.852c2.586 6.273 11.933 8.782 18.053 8.97l1.493.043 1.493-.043c6.273-.168 16.361-2.938 18.337-9.822l.171-.853.043-.867-.043-.867-.17-.854c-1.83-6.446-10.93-9.298-16.846-9.722l-1.492-.1-1.493-.028c-6.484-.105-16.767 2.324-19.546 8.998l-.284.852-.17.854-.058.867z" stroke-linecap="round"></path>
                <path d="M16.449 66.47V24.75m52.255 41.72V24.75"></path>
            </g>
            <g class="input_1 active">
                <path fill="currentColor" d="M74.049 40.288l-2.29 3.95-10.702-8.811"></path>
                <path d="M72.897 42.264L84.752 49.1M61.056 35.427l-36.92-21.338" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path>
                <path fill="currentColor" d="M13.392 5.261l-2.288 3.952 12.992 4.876"></path>
                <path d="M12.256 7.237L.4.4" stroke="currentColor" stroke-width=".8" stroke-linecap="round" stroke-linejoin="round"></path>
            </g>
        </g>
    </svg>'; ?>
    <?php $filter_size_1_prepend=""; ?>
    <?php $fitler_placeholder_size_1="20 мм"; ?>
    <?php $fitler_placeholder_size_2=""; ?>
    <?php $fitler_placeholder_size_3=""; ?>
<script>
    var filter_size_1_1=[<?= $filter_size_1; ?>];
</script>
<div class="cat-filter">
        <div class="cat-filter-heading">
            <span><?= $fitler_title; ?>:</span>
        </div>
        <div class="cat-filter-icon">
            <?= $fitler_img; ?>
        </div>
        <div class="cat-filter-params">
            <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
                <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
                <input class="filter" type="text" name="size_1" value="<?= (isset($_GET['size_1'])) ? $_GET['size_1'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_1; ?>" />
            </div>
            <div class="cat-filter-params-button">
                <button class="zag_button" type="submit">Подобрать</button>
            </div>
        </div>
    </div>


