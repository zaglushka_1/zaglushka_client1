<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;
AppAsset::register($this);
JqueryAsset::register($this);
?>
<?php $filter['size_1']=[]; ?>
<?php $filter['size_2']=[]; ?>
<?php $filter['size_3']=[]; ?>
<?php $filter['size_4']=[]; ?>
<?php $filter['size_5']=[]; ?>
<?php $filter['size_6']=[]; ?>
<?php foreach($catalog_element['filter'] as $filter_values): ?>
    <?php for($i=1;$i<=5;$i++): ?>
        <?php if(is_array($filter['size_'.$i]) and !in_array("'".html_entity_decode($filter_values['size_'.$i])."'",$filter['size_'.$i])): ?>
            <?php $filter['size_'.$i][]="'".html_entity_decode($filter_values['size_'.$i])."'"; ?>
        <?php endif; ?>
    <?php endfor; ?>
<?php endforeach; ?>
<?php $fitler_title=""; ?>
<?php $fitler_img=""; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1=""; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>

<?php $filter_size_1=implode(",",$filter['size_1']); ?>


<?php $fitler_title="Укажите диаметр отверстия"; ?>
<?php $fitler_img='<svg xmlns="http://www.w3.org/2000/svg" width="108.89" height="63.02" viewBox="0 0 108.89 63.02"><defs><style>.cls-1,.cls-2{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:.8px}.cls-2{stroke:currentColor}</style></defs><path class="cls-1 element"  d="M.4 47.21l.06.91.13.91.24.91.33.89.41.89.5.85.59.86.69.81.76.8.86.78.92.74.99.71 1.07.69 1.14.63 1.2.59 1.25.56 1.31.5 1.38.46 1.39.43 1.47.37 1.46.32 1.52.26 1.54.22 1.56.15 1.58.11 1.58.06h1.58l1.57-.06 1.58-.11 1.56-.15 1.54-.22 1.5-.26 1.49-.32 1.45-.37 1.41-.43 1.37-.46 1.3-.5 1.26-.56 1.21-.59 1.13-.63 1.06-.69 1-.71.93-.74.83-.78.78-.8.67-.81.6-.86.51-.85.41-.89.34-.89.22-.91.15-.91.04-.91-.04-.91-.15-.91-.22-.91-.34-.89-.41-.89-.51-.85-.6-.86-.67-.81-.78-.82-.83-.76-.93-.74-1-.71-1.06-.69-1.13-.63-1.21-.59-1.26-.56-1.3-.52-1.37-.46-1.41-.41-1.45-.37-1.49-.32-1.5-.26-1.54-.22-1.56-.17-1.58-.11-1.57-.03h-1.58l-1.58.03-1.58.11-1.56.17-1.54.22-1.52.26-1.46.32-1.47.37-1.39.41-1.38.46-1.31.52-1.25.56-1.2.59-1.14.63-1.07.69-.99.71-.92.74-.86.76-.76.82-.69.81-.59.86-.5.85-.41.89-.33.89-.24.91-.13.91-.06.91"></path><g class="input_1"><path class="cls-2" d="M53.24 10.33L8.23 36.32m82.78-4.2L46 58.11"></path><path fill="currentColor" d="M38.27 5.14l-2.22 3.82 12.58 4.71-10.36-8.53z"></path><path class="cls-2" d="M37.14 7.05L25.68.4"></path><path fill="currentColor" d="M98.13 39.75l-2.22 3.82-10.4-8.56 12.62 4.74z"></path><path class="cls-2" d="M97 41.66l11.49 6.62M85.76 35.17L48.12 13.45"></path></g></svg>'; ?>
<?php $filter_size_1_prepend=""; ?>
<?php $fitler_placeholder_size_1="20 мм"; ?>
<?php $fitler_placeholder_size_2=""; ?>
<?php $fitler_placeholder_size_3=""; ?>
<script>
    var filter_size_11_1=[<?= $filter_size_1; ?>];
</script>
<div class="cat-filter">
    <div class="cat-filter-heading">
        <span><?= $fitler_title; ?>:</span>
    </div>
    <div class="cat-filter-icon">
        <?= $fitler_img; ?>
    </div>
    <div class="cat-filter-params">
        <div class="cat-filter-params-group filter-type-<?= $catalog_element['filter'][0]['type']; ?>">
            <input type="hidden" name="filter_type" value="<?= $catalog_element['filter'][0]['type']; ?>" />
            <input class="filter" type="text" name="size_1" value="<?= (isset($_GET['size_1'])) ? $_GET['size_1'] : ''; ?>" placeholder="<?= $fitler_placeholder_size_1; ?>" />

        </div>
        <div class="cat-filter-params-button">
            <button class="zag_button" type="submit">Подобрать</button>
        </div>
    </div>
</div>


