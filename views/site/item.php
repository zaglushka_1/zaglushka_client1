<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\web\JqueryAsset;
use yii\bootstrap\Tabs;
use yii\jui\Accordion;
use app\models\ItemsModel;
/* @var $this yii\web\View */

$this->title=$catalog_element['item']['yandex_name'];
for($i=sizeof($catalog_element['breadcrumbs'])-1;$i>=0;$i--){
    $this->params['breadcrumbs'][] = array(
        'label'=> $catalog_element['breadcrumbs'][$i]['title'], 
        'url'=>$catalog_element['breadcrumbs'][$i]['url']
    ); 
}
$additionalPrices = [
        0 => 0,
        1 => 20,
        2 => 30,
        7 => 35,
        8 => 40,
        9 => 10,
        3 => 50,
        4 => 70,
        5 => 100,
        6 => 200
    ];
?>

<div class="item_title_mob">
    <div class="item_title_mob--back">
        <a href="/<?= $catalog_element['breadcrumbs'][1]['url']; ?>">
            <svg class="p-main-catalog__link-mob-icon icon icon_arrow-braedcrumbs2">
                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-braedcrumbs2"></use>
            </svg>
        </a>
    </div>
    <div class="item_title_mob--value">
        <span><?= $catalog_element['item']['title']; ?></span>
    </div>
    <div class="item_title_mob--filters">

    </div>
</div>
<div class="item_content">
    <div class="item_title" style="flex-basis: 100%;"><?= $catalog_element['item']['title']; ?></div>
    <div class="item_photo">
        <div class="item_photo_carousel">
                <div class="slider slider-for item_photo_carousel-full">
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/1.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/2.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/3.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/4.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/9.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/11.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/12.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/13.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/19.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/20.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/21.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/22.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/23.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="slider slider-nav item_photo_carousel-list">
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/5.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/6.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/7.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/8.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/10.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/11.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/14.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/15.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/16.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/24.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/25.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/27.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                    <?php $img="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/28.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <div>
                        <img src="<?= $img; ?>"/>
                    </div>
                    <?php endif; ?>
                </div>
        </div>
    </div>
    <div class="item_description">
        <div class="item_articul">
            Артикул: <span class="title"><?= $catalog_element['item']['title']; ?></span>
        </div>
        <div class="item_tabs">
            <?php
            echo Tabs::widget([
                'items' => [
                    [
                        'label'     =>  'Наличие',
                        'content'   =>  $this->render('items/info', ['catalog_element' => $catalog_element]),
                        'active'    =>  true
                    ],
                    [
                        'label'     => 'Характеристики',
                        'content'   =>  $this->render('items/characters', ['catalog_element' => $catalog_element])
                    ],
                    [
                        'label'     => 'Упаковка',
                        'content'   =>  $this->render('items/pack', ['catalog_element' => $catalog_element])
                    ],
                    [
                        'label'     => 'Оплата',
                        'content'   =>  $this->render('items/pay', ['catalog_element' => $catalog_element])
                    ]
                ]
            ]);
            ?>
        </div>
            <?php 
                $color_type=[];
            ?>
            <?php foreach($catalog_element['item']['colors'] as $color): ?>
                <?php $color_types[$color['add_price']][]=$color; ?>
            <?php endforeach;?>
            <div class="item-price-table">
                <div class="item-price-table-header">
                    <div class="item-price-table-header-color">
                        <span>Цвет</span>
                    </div>
                    <div class="item-price-table-header-price0">
                        <span>от 10 000</span>
                    </div>
                    <div class="item-price-table-header-price1">
                        <span>от 5 000</span>
                    </div>
                    <div class="item-price-table-header-price2">
                        <span>от 1 000</span>
                    </div>
                    <div class="item-price-table-header-price3">
                        <span>от 500</span>
                    </div>
                    <div class="item-price-table-header-price4">
                        <span>от 100</span>
                    </div>
                    <div class="item-price-table-header-price5">
                        <span>розница</span>
                    </div>
                </div>
                <?php
                    $color_count=0;
                    $all_colors=sizeof($catalog_element['item']['colors'])-1;
                    $all_types=sizeof($color_types)-1;
                    $type_count=0;
                ?>
                <?php foreach($color_types as $color_type): ?>
                    <?php if(isset($color_type)): ?>
                    <?php $color_in_type=count($color_type); ?>
                        <?php if($type_count==$all_types): ?>
                            <div class="item-price-table-row">
                                <div class="item-price-table-row-color">другие</div>
                                <?php $addPrice=1+($additionalPrices[$color_type[0]['add_price']]/100); ?>
                                <?php for($i=5;$i>=0;$i--): ?>
                                <div class="item-price-table-row-price".<?= 5-$i; ?>><?= number_format(($catalog_element['item']['price'.$i]*$addPrice),2,',',' '); ?></div>
                                <?php endfor; ?>
                            </div>
                        <?php else: ?>
                        <?php foreach($color_type as $color): ?>
                            <?php if((int)$color['color']==1): ?>
                            <div class="item-price-table-row">
                                <div class="item-price-table-row-color"><?= $color['color_name']; ?></div>
                                <?php $addPrice=1+($additionalPrices[$color['add_price']]/100); ?>
                                <?php for($i=5;$i>=0;$i--): ?>
                                    <div class="item-price-table-row-price".<?= 5-$i; ?>><?= number_format(($catalog_element['item']['price'.$i]*$addPrice),2,',',' '); ?></div>
                                <?php endfor; ?>
                            </div>
                            <?php endif; ?>
                            <?php $color_count++; ?>
                        <?php endforeach; ?>
                        <?php $type_count++; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        <div class="order-container">
            <div class="free-item">
                <?php if($catalog_element['item']['promo']=="1"): ?>
                    <p>Чтобы убедиться в качестве нашей продукции закажите бесплатные образцы.<br />
                    <a href="#" class="add_to_cart_free" data-id="<?= $catalog_element['item']['id']; ?>">Получить бесплатные образцы</a>
                    </p>
                <?php endif; ?>
            </div>
            <div class="item-order">
                <a href='#' class="add_to_cart" data-id='<?= $catalog_element['item']['id']; ?>'>Заказать</a>
            </div>
        </div>
    </div>
    <div class="item_content_text">
        <p><?= $catalog_element['item']['descr']; ?></p>
    </div>
</div>
<?php if(isset($catalog_element['itemObject']->complectItems) && count($catalog_element['itemObject']->complectItems)>0): ?>
<h3>Товары из комплекта</h3>
<div class="catalog-table">
    <div class="catalog-table-head">
        <div class="catalog-table-head-position">
            <span>№</span>
        </div>
        <div class="catalog-table-head-articul">
            <span>Артикул</span>
        </div>
        <div class="catalog-table-head-images">
            <span></span>
        </div>
        <div class="catalog-table-head-prices">
            <div class="catalog-table-head-prices-head">
                <span>Цена за 1 шт.</span>
            </div>
            <div class="catalog-table-head-prices-10000">
                <span>от 10 000</span>
            </div>
            <div class="catalog-table-head-prices-5000">
                <span>от 5 000</span>
            </div>
            <div class="catalog-table-head-prices-1000">
                <span>от 1 000</span>
            </div>
            <div class="catalog-table-head-prices-500">
                <span>от 500</span>
            </div>
            <div class="catalog-table-head-prices-100">
                <span>от 100</span>
            </div>
            <div class="catalog-table-head-prices-1">
                <span>розница</span>
            </div>
        </div>
        <div class="catalog-table-head-quantity">
            <span>Наличие</span>
        </div>
        <div class="catalog-table-head-cart">
            <span></span>
        </div>
    </div>
    <?php $i=1; ?>
    <?php foreach($catalog_element['itemObject']->complectItems as $key => $item): ?>
            <div class="catalog-table-body-row">
                <div class="catalog-table-body-row-position">
                    <div><span><?= $i ;?></span></div>
                </div>
                <div class="catalog-table-body-row-articul">
                    <span>Артикул:</span>
                    <a href='/<?= $item->item->slug->slug; ?>'>
                        <span><?= $item->item->title; ?></span>
                    </a>
                </div>
                <div class="catalog-table-body-row-images">
                    <a href='/<?= $item->item->slug->slug; ?>'>
                        <?php
                        if(substr($item->complect_item_id, -2)=="00") {
                            $imagePath="/images/item/0/".$item->complect_item_id."/";
                        } else {
                            $imagePath="/images/item/".substr($item->complect_item_id, -2)."/".$item->complect_item_id."/";
                        }

                        ?>
                        <?php $img="{$imagePath}18.jpg";?>
                        <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                            <img src="<?= $img; ?>" width="95" heigh="95"/>
                        <?php else: ?>
                            <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                        <?php endif; ?>
                    </a>
                    <?php $img="{$imagePath}5.jpg";?>
                    <a href='/<?= $item->item->slug->slug; ?>'>
                        <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                            <img src="<?= $img; ?>" width="95" heigh="95"/>
                        <?php else: ?>
                            <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                        <?php endif; ?>
                    </a>
                </div>
                <div class="catalog-table-body-row-prices">
                    <?php $price_head=['розница', 'от 100', 'от 500', 'от 1 000', 'от 5 000', 'от 10 000']; ?>
                    <div class="catalog-table-body-row-price catalog-table-body-row-price-head">
                        <span>Кол-во, шт.</span>
                        <span>Цена за шт.</span>
                    </div>
                    <?php for($j=5;$j>=0;$j--): ?>
                        <div class="catalog-table-body-row-price">
                            <span><?= $price_head[$j]; ?></span>
                            <?php $price_prop="price".$j; ?>
                            <?php if($item->item->$price_prop>0): ?>
                                <span><?= $item->item->$price_prop; ?> р.</span>
                            <?php endif; ?>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="catalog-table-body-row-quantity">
                    <div class="catalog-table-body-row-quantity-colors">
                        <?php $all_remains=0; ?>
                        <?php if(isset($item->item->itemColors) && sizeof($item->item->itemColors)>0): ?>
                            <?php $out_item=0; ?>
                            <?php $remains_item=0; ?>
                            <?php $all_remains=0; ?>
                            <?php foreach($item->item->itemColors as $color): ?>
                                <?php $all_remains+=(int)$color->remains; ?>
                                <?php if((isset($color->transit) && (int)$color->remains==0 && (int)$color->transit>0) or (int)$color->remains>0): ?>
                                    <div class="catalog-table-body-row-quantity-color-row<?= ($remains_item>2) ? "" : "";?>" data-item_id="<?= $key; ?>">
                                        <div class="catalog-table-body-row-quantity-color-row-remains">
                                            <div class="catalog-table-body-row-quantity-color-row-remains-colorhex">
                                                <div class="" style="width: 12px; height: 12px; border: 1px solid #cacaca; background-color: #<?= $color->colors->hex; ?>; margin-right:0px; margin-top:5px;">

                                                </div>
                                            </div>
                                            <div class="catalog-table-body-row-quantity-color-row-remains-colorname">
                                        <span>
                                            <?= $color->colors->name; ?>
                                        </span>
                                            </div>
                                            <div class="catalog-table-body-row-quantity-color-row-remains-quamtity">
                                        <span>
                                            <?= number_format($color->remains,0,'',' '); ?> шт.
                                        </span>
                                            </div>
                                        </div>
                                        <?php if(isset($color->transit) && $color->transit>0): ?>
                                            <div class="catalog-table-body-row-quantity-color-row-transit">
                                                <div class="catalog-table-body-row-quantity-color-row-transit-transit">
                                                    <span>+едет</span>
                                                </div>
                                                <div class="catalog-table-body-row-quantity-color-row-transit-quantity">
                                        <span>
                                            <?= number_format($color->transit,0,'',' '); ?> шт.
                                        </span>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php $remains_item++; ?>
                                <?php else: ?>
                                    <?php $out_item++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>

                        <?php endif; ?>
                        <?php if(isset($remains_item) and $remains_item>3): ?>
                            <a href="#" class="other-color-show-hide" data-id="<?= $key; ?>">
                                <span>другие цвета</span>
                                <svg class="product-table-colors__icon icon icon_arrow-thin">
                                    <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-thin"></use>
                                </svg></a>
                        <?php endif; ?>
                        <?php if($all_remains==0): ?>
                            <div class="catalog-not-available">
                                <div class="catalog-not-available--active">
                                    <span>В производстве</span>
                                </div>
                                <div class="catalog-not-available--disable">
                                    <span>Срок поставки <br /> спрашивайте у менеджера</span>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="catalog-table-body-row-info">
                    <div class="catalog-table-body-row-info-quantity">
                        <span>на складе <?= (isset($all_remains)) ? number_format($all_remains,0,'',' ') : '0'; ?> шт.</span>
                    </div>
                    <div class="catalog-table-body-row-info-more">
                        <a href='/<?= $item->item->slug->slug; ?>'>
                            <span>Подробнее</span>
                            <svg class="mob-product__link-icon icon icon_arrow-menu">
                                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="catalog-table-body-row-cart <?=  (Yii::$app->user->isGuest) ? '' : 'manager_login'; ?>">
                    <a href='#' class="add_to_cart" data-id='<?= $key; ?>' style="">
                        <span>В КОРЗИНУ ></span>
                        <img src="https://zaglushka.ru/images/cart.svg">
                    </a>
                </div>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <div class="compare-catalog" data-itemId="<?= $key; ?>">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star" style="display:none;"></i>
                    </div>
                <?php endif; ?>
            </div>
            <?php $i++; ?>
    <?php endforeach; ?>
</div>
<?php endif; ?>
<?php if(isset($catalog_element['itemObject']->sameItems) && count($catalog_element['itemObject']->sameItems)>0): ?>
    <h3>Рекомендуем посмотреть</h3>
    <div class="catalog-table">
        <div class="catalog-table-head">
            <div class="catalog-table-head-position">
                <span>№</span>
            </div>
            <div class="catalog-table-head-articul">
                <span>Артикул</span>
            </div>
            <div class="catalog-table-head-images">
                <span></span>
            </div>
            <div class="catalog-table-head-prices">
                <div class="catalog-table-head-prices-head">
                    <span>Цена за 1 шт.</span>
                </div>
                <div class="catalog-table-head-prices-10000">
                    <span>от 10 000</span>
                </div>
                <div class="catalog-table-head-prices-5000">
                    <span>от 5 000</span>
                </div>
                <div class="catalog-table-head-prices-1000">
                    <span>от 1 000</span>
                </div>
                <div class="catalog-table-head-prices-500">
                    <span>от 500</span>
                </div>
                <div class="catalog-table-head-prices-100">
                    <span>от 100</span>
                </div>
                <div class="catalog-table-head-prices-1">
                    <span>розница</span>
                </div>
            </div>
            <div class="catalog-table-head-quantity">
                <span>Наличие</span>
            </div>
            <div class="catalog-table-head-cart">
                <span></span>
            </div>
        </div>
        <?php $i=1; ?>
        <?php foreach($catalog_element['itemObject']->sameItems as $key => $item): ?>
            <div class="catalog-table-body-row">
                <div class="catalog-table-body-row-position">
                    <div><span><?= $i ;?></span></div>
                </div>
                <div class="catalog-table-body-row-articul">
                    <span>Артикул:</span>
                    <a href='/<?= $item->item->slug->slug; ?>'>
                        <span><?= $item->item->title; ?></span>
                    </a>
                </div>
                <div class="catalog-table-body-row-images">
                    <a href='/<?= $item->item->slug->slug; ?>'>
                        <?php
                        if(substr($item->item->id, -2)=="00") {
                            $imagePath="/images/item/0/".$item->item->id."/";
                        } else {
                            $imagePath="/images/item/".substr($item->item->id, -2)."/".$item->item->id."/";
                        }

                        ?>
                        <?php $img="{$imagePath}18.jpg";?>
                        <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                            <img src="<?= $img; ?>" width="95" heigh="95"/>
                        <?php else: ?>
                            <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                        <?php endif; ?>
                    </a>
                    <?php $img="{$imagePath}5.jpg";?>
                    <a href='/<?= $item->item->slug->slug; ?>'>
                        <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                            <img src="<?= $img; ?>" width="95" heigh="95"/>
                        <?php else: ?>
                            <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                        <?php endif; ?>
                    </a>
                </div>
                <div class="catalog-table-body-row-prices">
                    <?php $price_head=['розница', 'от 100', 'от 500', 'от 1 000', 'от 5 000', 'от 10 000']; ?>
                    <div class="catalog-table-body-row-price catalog-table-body-row-price-head">
                        <span>Кол-во, шт.</span>
                        <span>Цена за шт.</span>
                    </div>
                    <?php for($j=5;$j>=0;$j--): ?>
                        <div class="catalog-table-body-row-price">
                            <span><?= $price_head[$j]; ?></span>
                            <?php $price_prop="price".$j; ?>
                            <?php if($item->item->$price_prop>0): ?>
                                <span><?= $item->item->$price_prop; ?> р.</span>
                            <?php endif; ?>
                        </div>
                    <?php endfor; ?>
                </div>
                <div class="catalog-table-body-row-quantity">
                    <div class="catalog-table-body-row-quantity-colors">
                        <?php $all_remains=0; ?>
                        <?php if(isset($item->item->itemColors) && sizeof($item->item->itemColors)>0): ?>
                            <?php $out_item=0; ?>
                            <?php $remains_item=0; ?>
                            <?php $all_remains=0; ?>
                            <?php foreach($item->item->itemColors as $color): ?>
                                <?php $all_remains+=(int)$color->remains; ?>
                                <?php if((isset($color->transit) && (int)$color->remains==0 && (int)$color->transit>0) or (int)$color->remains>0): ?>
                                    <div class="catalog-table-body-row-quantity-color-row<?= ($remains_item>2) ? "" : "";?>" data-item_id="<?= $key; ?>">
                                        <div class="catalog-table-body-row-quantity-color-row-remains">
                                            <div class="catalog-table-body-row-quantity-color-row-remains-colorhex">
                                                <div class="" style="width: 12px; height: 12px; border: 1px solid #cacaca; background-color: #<?= $color->colors->hex; ?>; margin-right:0px; margin-top:5px;">

                                                </div>
                                            </div>
                                            <div class="catalog-table-body-row-quantity-color-row-remains-colorname">
                                        <span>
                                            <?= $color->colors->name; ?>
                                        </span>
                                            </div>
                                            <div class="catalog-table-body-row-quantity-color-row-remains-quamtity">
                                        <span>
                                            <?= number_format($color->remains,0,'',' '); ?> шт.
                                        </span>
                                            </div>
                                        </div>
                                        <?php if(isset($color->transit) && $color->transit>0): ?>
                                            <div class="catalog-table-body-row-quantity-color-row-transit">
                                                <div class="catalog-table-body-row-quantity-color-row-transit-transit">
                                                    <span>+едет</span>
                                                </div>
                                                <div class="catalog-table-body-row-quantity-color-row-transit-quantity">
                                        <span>
                                            <?= number_format($color->transit,0,'',' '); ?> шт.
                                        </span>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php $remains_item++; ?>
                                <?php else: ?>
                                    <?php $out_item++; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php else: ?>

                        <?php endif; ?>
                        <?php if(isset($remains_item) and $remains_item>3): ?>
                            <a href="#" class="other-color-show-hide" data-id="<?= $key; ?>">
                                <span>другие цвета</span>
                                <svg class="product-table-colors__icon icon icon_arrow-thin">
                                    <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-thin"></use>
                                </svg></a>
                        <?php endif; ?>
                        <?php if($all_remains==0): ?>
                            <div class="catalog-not-available">
                                <div class="catalog-not-available--active">
                                    <span>В производстве</span>
                                </div>
                                <div class="catalog-not-available--disable">
                                    <span>Срок поставки <br /> спрашивайте у менеджера</span>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="catalog-table-body-row-info">
                    <div class="catalog-table-body-row-info-quantity">
                        <span>на складе <?= (isset($all_remains)) ? number_format($all_remains,0,'',' ') : '0'; ?> шт.</span>
                    </div>
                    <div class="catalog-table-body-row-info-more">
                        <a href='/<?= $item->item->slug->slug; ?>'>
                            <span>Подробнее</span>
                            <svg class="mob-product__link-icon icon icon_arrow-menu">
                                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="catalog-table-body-row-cart <?=  (Yii::$app->user->isGuest) ? '' : 'manager_login'; ?>">
                    <a href='#' class="add_to_cart" data-id='<?= $key; ?>' style="">
                        <span>В КОРЗИНУ ></span>
                        <img src="https://zaglushka.ru/images/cart.svg">
                    </a>
                </div>
                <?php if(!Yii::$app->user->isGuest): ?>
                    <div class="compare-catalog" data-itemId="<?= $key; ?>">
                        <i class="fa fa-star-o"></i>
                        <i class="fa fa-star" style="display:none;"></i>
                    </div>
                <?php endif; ?>
            </div>
            <?php $i++; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<div class="item_accordion">
    <?php
    echo Accordion::widget([
        'items' => [
            [
                'header'     =>  '<svg class="mob-product-item-list__icon icon icon_in-stock">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#in-stock"></use>
</svg><span>Наличие</span><svg class="mob-product-item-list__arrow icon icon_arrow-menu">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
</svg>',
                'content'   =>  $this->render('items/info', ['catalog_element' => $catalog_element])
            ],
            [
                'header'     =>  '<svg class="mob-product-item-list__icon icon icon_info">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#info"></use>
</svg><span>Описание</span><svg class="mob-product-item-list__arrow icon icon_arrow-menu">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
</svg>',
                'content'   =>  "<section class=\"item_description\">".$catalog_element['item']['descr']."</section>",
            ],
            [
                'header'     => '<svg class="mob-product-item-list__icon icon icon_characteristics">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#characteristics"></use>
</svg><span>Характеристики</span><svg class="mob-product-item-list__arrow icon icon_arrow-menu">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
</svg>',
                'content'   =>  $this->render('items/characters', ['catalog_element' => $catalog_element])
            ],
            [
                'header'     => '<svg class="mob-product-item-list__icon icon icon_package">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#package"></use>
</svg><span>Упаковка</span><svg class="mob-product-item-list__arrow icon icon_arrow-menu">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
</svg>',
                'content'   =>  $this->render('items/pack', ['catalog_element' => $catalog_element])
            ],
            [
                'header'     => '<svg class="mob-product-item-list__icon icon icon_payment">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#payment"></use>
</svg><span>Оплата</span><svg class="mob-product-item-list__arrow icon icon_arrow-menu">
<use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-menu"></use>
</svg>',
                'content'   =>  $this->render('items/pay', ['catalog_element' => $catalog_element])
            ],
        ],
        'options' => ['tag' => 'div'],
        'itemOptions' => ['tag' => 'div'],
        'headerOptions' => ['tag' => 'h3'],
        'clientOptions' => ['heightStyle' => 'content','collapsible' => true,],
    ]);
    ?>
</div>