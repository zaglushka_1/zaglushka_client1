<?php

use yii\helpers\Url;
use app\assets\AppAsset;
AppAsset::register($this);
/* @var $this yii\web\View */

$this->title = 'Корзина';
$additionalPrices = [
        0 => 0,
        1 => 20,
        2 => 30,
        7 => 35,
        8 => 40,
        9 => 10,
        3 => 50,
        4 => 70,
        5 => 100,
        6 => 200
    ];
?>
<div class="add-to-cart-form">
    <div class="close-cart-form">
        <a href="#">Закрыть</a>
    </div>
    <div class="cart-form">
        <input type="hidden" name="itemId" id="itemId" value="<?= $itemData['id']; ?>" />
        <table style="width:100%;">
            <thead>
                <tr>
                    <td>Фото</td>
                    <td>Артикул</td>
                    <td>Описание</td>
                    <td>Цвет</td>
                    <td>Количество, штук</td>
                    <td>Цена за штуку</td>
                    <td>Сумма</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="cart-form-image"><img src="https://zaglushka.ru/images/item/<?php echo substr($itemData['id'], -2); ?>/<?= $itemData['id']; ?>/5.jpg"</td>
                    <td class="cart-form-title"><span>Артикул:</span><span><?= $itemData['title']; ?></span></td>
                    <td class="cart-form-description"><?= $itemData['short_descr']; ?></td>
                    <td class="cart-form-color-select">
                        <span>Цвет:</span>
                        <div class="cart-form-color-select-area">
                            <select class="change_color_cart">
                                <?php foreach($itemColorData as $color): ?>
                                <option value="<?= $color['items_colors_id']; ?>" data-remains="<?= $color['remains']; ?>" data-title="<?= $color['code']; ?>" data-addPrice="<?= $additionalPrices[$color['addprice']]; ?>" data-price0="<?= $itemData['price0']; ?>" data-price1="<?= $itemData['price1']; ?>" data-price2="<?= $itemData['price2']; ?>" data-price3="<?= $itemData['price3']; ?>" data-price4="<?= $itemData['price4']; ?>" data-price5="<?= $itemData['price5']; ?>" data-colorName="<?= $color['colorname']; ?>"><?= $color['colorname']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <svg class="icon icon_arrows icon_arrows_select">
                                <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrows"></use>
                            </svg>
                        </div>
                        <div class="cart-form-color-select-remains-area">
                            В наличии:<br />
                            <strong class="cart-form-color-select-remains-value"></strong>
                        </div>
                    </td>
                    <td class="cart-form-quantity">
                        <span>Количество штук:</span>
                        <input type="number" class="quantity" min="1" max="1000000"/>
                    </td>
                    <td class="cart-form-oneprice">
                        <span>Цена за штуку:</span>
                        <div class="cart_one_price">

                        </div>
                    </td>
                    <td class="cart-form-allprice">
                        <span>Сумма:</span>
                        <div class="cart_total_price"></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <hr />
        <div class="addToCartContainer">
            <button class="addToCart">Добавить в корзину</button>
        </div>
    </div>
    
</div>

<script>
$(function() {
    function disableScrolling(){
        var x=window.scrollX;
        var y=window.scrollY;
        window.onscroll=function(){window.scrollTo(x, y);};
    }

    function enableScrolling(){
        window.onscroll=function(){};
    }
    function formValidate() {
        // здесь валидация
    }
    function calcPrice() {
        var one_price=0;
        var total=0;
        var objSelect=$(".change_color_cart option:selected");
        var addPrice=parseFloat(objSelect.attr('data-addPrice'));
        var price0=parseFloat(objSelect.attr('data-price5'));
        var price1=parseFloat(objSelect.attr('data-price4'));
        var price2=parseFloat(objSelect.attr('data-price3'));
        var price3=parseFloat(objSelect.attr('data-price2'));
        var price4=parseFloat(objSelect.attr('data-price1'));
        var price5=parseFloat(objSelect.attr('data-price0'));
        var quantity=parseInt($(".quantity").val());
        addPrice=1+(addPrice/100);
        if(quantity<=100) {
            one_price=price5*addPrice;
            total=quantity*one_price;
        } 
        if(quantity>100 && quantity<=500) {
            one_price=price4*addPrice;
        }  
        if(quantity>500 && quantity<=1000) {
            one_price=price3*addPrice;
        }  
        if(quantity>1000 && quantity<=5000) {
            one_price=price2*addPrice;
        }  
        if(quantity>5000 && quantity<=10000) {
            one_price=price1*addPrice;
        }  
        if(quantity>10000) {
            one_price=price0*addPrice;
        } 
        total=quantity*one_price;
        if(addPrice>0 && quantity>0)
        {
            $(".cart_one_price").html(one_price.toFixed(2));
            $(".cart_total_price").html(total.toFixed(2));
        } else {
            $(".cart_one_price").html();
            $(".cart_total_price").html();
        }
    }
    $(".change_color_cart").on("change", function(){
        $(".cart_one_price").html('');
        $(".cart_total_price").html('');
        $(".cart-form-color-select-remains-value").html('');
        var selectedObj=$(this).find("option:selected");
        var remains=parseInt(selectedObj.attr("data-remains"));
        if(remains>0) {
            $(".cart-form-color-select-remains-area").show();
            $(".cart-form-color-select-remains-value").html(remains.toLocaleString()+" шт.");
        } else {
            $(".cart-form-color-select-remains-area").hide();
        }
        calcPrice();
    });
    $(".quantity").on("input", function(){
        var quantity=parseInt($(".quantity").val());
        console.log(quantity);
        if(quantity>0) {
            calcPrice();
        } else {
            $(".cart_one_price").html('');
            $(".cart_total_price").html('');
        }
    });
    $(".close-cart-form").on("click", function() {
        $(".overlay").hide();
        $(".overlay .popup_container").html();
        enableScrolling();
    });
    $(".addToCart").on("click", function() {
        var quantity=parseInt($(".quantity").val());
        var colorId=parseInt($(".change_color_cart option:selected").val());
        if(quantity>0 && colorId>0) {
            $.post("/cart", { id: colorId, quantity: quantity, action: "addItem" }).done(function(data) {
                $(".popup_cart").slideDown(300);
                $(".overlay").hide();
                $(".overlay .popup_container").html();
                enableScrolling();
                var quantity=parseInt($(".quantity").val());
                $(".js-nav-cart-count").html(parseInt($(".js-nav-cart-count").html())+quantity);
                var sum=$(".cart_total_price").html();
                var img_link=$(".cart-form-image img").attr("src");
                var colorName=$(".change_color_cart option:selected").attr("data-colorName");
                $(".popup_cart_header_table_data_image img").attr("src", img_link);
                $(".popup_cart_header_table_data_color span").html(colorName);
                $(".popup_cart_header_table_data_quantity span").html(quantity);
                $(".popup_cart_header_table_data_price span").html(sum);
                $(".popup_cart").delay(10000).slideUp(300);
            });
        }
        if(colorId>0) {
            $(".change_color_cart").focus();
        }
        if(quantity>0) {
            $(".quantity").focus();
        }
    });
});
</script>