<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Контакты';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact-page">
    <h1 style="font-size:24px;margin: 20px 10px;"><?= Html::encode($this->title) ?></h1>
    <div class="catalog_content" style="margin-bottom: 20px;">
        <!-- карта и реквизиты -->
    </div>
    <div class="catalog_content">
        <!-- менеджеры -->
         <h2>Менеджеры по работе с клиентами</h2>
            <section class="contacts">
                <div class="contacts__title-wrap">
                    <h3 class="title">
                        <span>
                            Направление «Фурнитура»
                        </span>
                        <div class="contacts__tooltip" data-title="Направление включает в себя более 7000 наименований, среди которых заглушки для труб, опоры, фиксаторы, подпятники, колпачки, переходники и др."></div>
                    </h3>
                </div>
                <div class="contacts__subtitle-wrap">
                    <h3 class="subtitle">Оптовый отдел</h3>
		</div>
                <div class="contacts-list">
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/kraynova.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Галина Крайнова</div>
                            <div class="contacts-item__email">gk@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 127</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/timofeev.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Денис Тимофеев</div>
                            <div class="contacts-item__email">d.timofeev@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 113</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="https://zaglushka.ru/images/managers/malenchev.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Максим Аленчев</div>
                            <div class="contacts-item__email">m.alenchev@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 135</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
		                    <img src="https://zaglushka.ru/images/managers/npetrakova.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Надежда Петракова</div>
                            <div class="contacts-item__email">n.petrakova@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 125</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="https://zaglushka.ru/images/managers/atumarkin.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Алексей Тумаркин</div>
                            <div class="contacts-item__email">a.tumarkin@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 126</div>
                        </div>
                    </div>
                    <!-- <div class="contacts-item">
                        <div class="contacts-item__img">
							<img src="https://zaglushka.ru/images/managers/rprusskiy.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Роман Прусский</div>
                            <div class="contacts-item__email">r.prusskiy@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 124</div>
                        </div>
                    </div> -->
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/sshubarcov.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Сергей Шубарцов</div>
                            <div class="contacts-item__email">sshubarcov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 141</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/pudenkov.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Павел Юденков</div>
                            <div class="contacts-item__email">p.udenkov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 111</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/dcvetkov.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Дмитрий Цветков</div>
                            <div class="contacts-item__email">d.cvetkov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 101</div>
                        </div>
                    </div>
                </div>
                <div class="contacts__subtitle-wrap">
                    <h3 class="subtitle">Розничный отдел</h3>
                </div>
                <div class="contacts-list">
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/strekalovskaya.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Марина Стрекаловская</div>
                            <div class="contacts-item__email">ms@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 121</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/aglotov.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Александр Глотов</div>
                            <div class="contacts-item__email">a.glotov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 137</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/amakarov.jpg" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Артем Макаров</div>
                            <div class="contacts-item__email">a.makarov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 138</div>
                        </div>
                    </div>
                </div>
                </section>
                <section class="contacts">
                <div class="contacts__title-wrap">
                    <h3 class="title">
                        <span>
                            Направление «Малые архитектурные формы»
                        </span>
                        <div class="contacts__tooltip" data-title="В данном направлении представлены различные комплектующие и канатные конструкции для детских площадок и спортивных комплексов."></div>
                    </h3>
                </div>
                <div class="contacts-list">
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/dyachkov.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Дмитрий Дьячков</div>
                            <div class="contacts-item__email">d.dyachkov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 123</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/bagaeva.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Светлана Багаева</div>
                            <div class="contacts-item__email">s.bagaeva@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 129</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/krasnova.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Анастасия Краснова</div>
                            <div class="contacts-item__email">a.krasnova@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 139</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/evdokimov.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Вадим Евдокимов</div>
                            <div class="contacts-item__email">vevdokimov@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 146</div>
                        </div>
                    </div>
                    <div class="contacts-item">
                        <div class="contacts-item__img ">
                            <img src="https://zaglushka.ru/images/managers/aromanova.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Альбина Романова</div>
                            <div class="contacts-item__email">a.romanova@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 116</div>
                        </div>
                    </div>
                </div>
                </section>
                <section class="contacts">
                <div class="contacts__title-wrap">
                    <h3 class="title">
                        <span>
                            Направление «Мебель»
                        </span>
                        <div class="contacts__tooltip" data-title="В данном направлении представлены пластиковые стулья и мебельные комплектующие от ведущих итальянских производителей."></div>
                    </h3>
                </div>
                <div class="contacts-list">
                    <div class="contacts-item">
                        <div class="contacts-item__img">
                            <img src="https://zaglushka.ru/images/managers/samoylova.png" width="96" height="96">
                        </div>
                        <div class="contacts-item__text">
                            <div class="contacts-item__name">Екатерина Самойлова</div>
                            <div class="contacts-item__email">es@zaglushka.ru</div>
                            <div class="contacts-item__phone">доб. 130</div>
                        </div>
                    </div>
                </div>
                </section>
                <section class="contact-item">
                    <div class="contacts-item"><a href="/files/rekvizity.pdf" target="_blank">Реквизиты компании</a></div>
                </section>
    </div>
</div>
<div class="site-contact-page_mobile">
    <h1><?= Html::encode($this->title) ?></h1>
    <h2>ОФИС | СКЛАД</h2>
    <section class="site-contact-page_mobile-block">
        <div class="site-contact-page_mobile_address">
            <img src="https://zaglushka.ru/images/contacts/pointer.svg">
            <p>
                194362, Россия, г. Санкт-Петербург,<br />
                п. Парголово, ул. Железнодорожная, д. 11, корп. 2, лит. A, пом. 21-H. АНГАР № 12
            </p>
        </div>
        <div class="site-contact-page_mobile_phones">
            <img src="https://zaglushka.ru/images/contacts/phone.svg">
            <p>
                <a style="color: black; text-decoration: none;" href="tel:88005550499">8 (800) 555 04 99</a> (бесплатно по России)<br />
                <a style="color: black; text-decoration: none;" href="tel:8 (812) 242 80 99">8 (812) 242 80 99</a>  — Санкт-Петербург
            </p></div>
        <div class="site-contact-page_mobile_email">
            <img src="https://zaglushka.ru/images/contacts/mail.svg">
            <p>
                <a style="color: black; text-decoration: none;" href="mailto:sales@zaglushka.ru">sales@zaglushka.ru</a>
            </p></div>
        <div class="site-contact-page_mobile_timework">
            <img src="https://zaglushka.ru/images/contacts/calendar.svg">
            <p>
                Пн – Пт: с 9:00 до 18:00
            </p></div>
    </section>
    <h2>ПРОИЗВОДСТВО</h2>
    <section class="site-contact-page_mobile-block">
         <div class="site-contact-page_mobile_address">
             <img src="https://zaglushka.ru/images/contacts/pointer.svg">
             <p>

                 Россия, г. Санкт-Петербург,<br />ул. Минеральная, д. 32 <br />
                 (Машиностроительный завод «Арсенал»)
             </p>
         </div>
    </section>
    <h2>ПОЧТОВЫЙ АДРЕС</h2>
    <section class="site-contact-page_mobile-block">
        <div class="site-contact-page_mobile_address">
            <img src="https://zaglushka.ru/images/contacts/pointer.svg">
            <p>
                194362, Россия,<br />г. Санкт-Петербург, а/я 17
            </p>
        </div>
    </section>
    <h2>РЕКВИЗИТЫ</h2>
    <section class="site-contact-page_mobile-block">
        <div class="site-contact-page_mobile_address">
            <img src="https://zaglushka.ru/images/contacts/pointer.svg">
            <p>
                <a href="" />Реквизиты компании</a>
            </p>
        </div>
    </section>
    <section class="site-contact-page_mobile_yandex_map">

    </section>
    <h2>РУКОВОДИТЕЛИ</h2>
    <h3>Направление Фурнитура</h3>
    <section class="site-contact-page_mobile_card">
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/timofeev.png" />
            </div>
            <p>
                <strong>Денис Тимофеев</strong><br />d.timofeev@zaglushka.ru<br />(доб. 113)</p>
        </div>
    </section>
    <h2>МЕНЕДЖЕРЫ ПО РАБОТЕ С КЛИЕНТАМИ</h2>
    <h3>Направление Фурнитура</h3>
    <section class="site-contact-page_mobile_card">
        <h3>Оптовый отдел</h3>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/kraynova.png" />
            </div>
            <p><strong>Галина Крайнова</strong><br />gk@zaglushka.ru<br />(доб. 127)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/malenchev.jpg" />
            </div>
            <p><strong>Максим Аленчев</strong><br />m.alenchev@zaglushka.ru<br />(доб. 135)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/npetrakova.png" />
            </div>
            <p><strong>Надежда Петракова</strong><br />n.petrakova@zaglushka.ru<br />(доб. 125)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/sshubarcov.jpg" />
            </div>
            <p><strong>Сергей Шубарцов</strong><br />sshubarcov@zaglushka.ru<br />(доб. 141)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/pudenkov.jpg" />
            </div>
            <p><strong>Павел Юденков</strong><br />p.udenkov@zaglushka.ru<br />(доб. 111)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/dcvetkov.jpg" />
            </div>
            <p><strong>Дмитрий Цветков</strong><br />d.cvetkov@zaglushka.ru<br />(доб. 101)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/tpanova.jpg" />
            </div>
            <p><strong>Таисия Панова</strong><br />t.panova@zaglushka.ru<br />(доб. 124)</p>
        </div>
        <h3>Розничный отдел</h3>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/aglotov.jpg" />
            </div>
            <p><strong>Александр Глотов</strong><br />a.glotov@zaglushka.ru<br />(доб. 137)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/amakarov.jpg" />
            </div>
            <p><strong>Артем Макаров</strong><br />a.makarov@zaglushka.ru<br />(доб. 138)</p>
        </div>
    </section>
    <h3>Направление Фурнитура</h3>

    <section class="site-contact-page_mobile_card">
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/dyachkov.png" />
            </div>
            <p><strong>Дмитрий Дьячков</strong><br />d.dyachkov@zaglushka.ru<br />(доб. 123)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/bagaeva.png" />
            </div>
            <p><strong>Светлана Багаева</strong><br />s.bagaeva@zaglushka.ru<br />(доб. 129)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/krasnova.png" />
            </div>
            <p><strong>Анастасия Краснова</strong><br />a.krasnova@zaglushka.ru<br />(доб. 139)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/evdokimov.png" />
            </div>
            <p><strong>Вадим Евдокимов</strong><br />vevdokimov@zaglushka.ru<br />(доб. 146)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/aromanova.png" />
            </div>
            <p><strong>Альбина Романова</strong><br />a.romanova@zaglushka.ru<br />(доб. 116)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/ppopov.jpg" />
            </div>
            <p><strong>Павел Попов</strong><br />p.popov@zaglushka.ru<br />(доб. 306)</p>
        </div>
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/dzotova.jpg" />
            </div>
            <p><strong>Диана Зотова</strong><br />d.zotova@zaglushka.ru<br />(доб. 104)</p>
        </div>
    </section>
    <h3>Направление Мебель</h3>

    <section class="site-contact-page_mobile_card">
        <div class="personel_data">
            <div class="image">
                <img src="https://zaglushka.ru/images/managers/samoylova.png" />
            </div>
            <p><strong>Екатерина Самойлова</strong><br />es@zaglushka.ru<br />(доб. 130)</p>
        </div>
    </section>
</div>
