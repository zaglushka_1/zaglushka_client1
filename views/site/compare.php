<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;


AppAsset::register($this);
JqueryAsset::register($this);
?>
    <h1>Избранное </h1>
<?php if(isset($catalog_element['catalog_items'])): ?>
    <!-- Фильтры -->
    <div class="catalog-table">
        <div class="catalog-table-head">
            <div class="catalog-table-head-position">
                <span>№</span>
            </div>
            <div class="catalog-table-head-articul">
                <span>Артикул</span>
            </div>
            <div class="catalog-table-head-images">
                <span></span>
            </div>
            <div class="catalog-table-head-prices">
                <div class="catalog-table-head-prices-head">
                    <span>Цена за 1 шт.</span>
                </div>
                <div class="catalog-table-head-prices-10000">
                    <span>от 10 000</span>
                </div>
                <div class="catalog-table-head-prices-5000">
                    <span>от 5 000</span>
                </div>
                <div class="catalog-table-head-prices-1000">
                    <span>от 1 000</span>
                </div>
                <div class="catalog-table-head-prices-500">
                    <span>от 500</span>
                </div>
                <div class="catalog-table-head-prices-100">
                    <span>от 100</span>
                </div>
                <div class="catalog-table-head-prices-1">
                    <span>розница</span>
                </div>
            </div>
            <div class="catalog-table-head-quantity">
                <span>Наличие</span>
            </div>
            <div class="catalog-table-head-cart">
                <span></span>
            </div>
        </div>
        <?php $i=1; ?>
        <?php foreach($catalog_element['catalog_items'] as $key => $item): ?>
                <div class="catalog-table-body-row">
                    <div class="catalog-table-body-row-position">
                        <span><?= $i ;?></span>
                    </div>
                    <div class="catalog-table-body-row-articul">
                        <!--<span>Артикул:</span> -->
                        <a href='/<?= $item['slug']; ?>'>
                            <span><?= $item['title']; ?></span>
                        </a>
                    </div>
                    <div class="catalog-table-body-row-images">
                        <a href='/<?= $item['slug']; ?>'>
                            <?php $img="/images/item/".substr($item['id'], -2)."/".$item['id']."/18.jpg";?>
                            <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                                <img src="<?= $img; ?>" width="95" heigh="95"/>
                            <?php else: ?>
                                <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                            <?php endif; ?>
                        </a>
                        <?php $img="/images/item/".substr($item['id'], -2)."/".$item['id']."/5.jpg";?>
                        <a href='/<?= $item['slug']; ?>'>
                            <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                                <img src="<?= $img; ?>" width="95" heigh="95"/>
                            <?php else: ?>
                                <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="catalog-table-body-row-prices">
                        <?php $price_head=['розница', 'от 100', 'от 500', 'от 1 000', 'от 5 000', 'от 10 000']; ?>
                        <div class="catalog-table-body-row-price catalog-table-body-row-price-head">
                            <span>Кол-во, шт.</span>
                            <span>Цена за шт.</span>
                        </div>
                        <?php for($j=5;$j>=0;$j--): ?>
                            <div class="catalog-table-body-row-price">
                                <span><?= $price_head[$j]; ?></span>
                                <span><?= $item['price'.$j]; ?> р.</span>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="catalog-table-body-row-quantity">
                        <div class="catalog-table-body-row-quantity-colors">
                            <?php if(isset($item['colors']) && sizeof($item['colors'])>0): ?>
                                <?php $out_item=0; ?>
                                <?php $remains_item=0; ?>
                                <?php $all_remains=0; ?>
                                <?php foreach($item['colors'] as $color): ?>
                                    <?php if((isset($color['transit']) && (int)$color['remains']==0 && (int)$color['transit']>0) or (int)$color['remains']>0): ?>
                                        <div class="catalog-table-body-row-quantity-color-row<?= ($remains_item>2) ? "" : "";?>" data-item_id="<?= $key; ?>">
                                            <div class="catalog-table-body-row-quantity-color-row-remains">
                                                <div class="catalog-table-body-row-quantity-color-row-remains-colorhex">
                                                    <div class="" style="width: 12px; height: 12px; border: 1px solid #cacaca; background-color: #<?= $color['color_hex']; ?>; margin-right:0px; margin-top:5px;">

                                                    </div>
                                                </div>
                                                <div class="catalog-table-body-row-quantity-color-row-remains-colorname">
                                        <span>
                                            <?= $color['color_name']; ?>
                                        </span>
                                                </div>
                                                <div class="catalog-table-body-row-quantity-color-row-remains-quamtity">
                                        <span>
                                            <?php $all_remains+=(int)$color['remains']; ?>
                                            <?= number_format($color['remains'],0,'',' '); ?> шт.
                                        </span>
                                                </div>
                                            </div>
                                            <?php if(isset($color['transit']) && $color['transit']>0): ?>
                                                <div class="catalog-table-body-row-quantity-color-row-transit">
                                                    <div class="catalog-table-body-row-quantity-color-row-transit-transit">
                                                        <span>+едет</span>
                                                    </div>
                                                    <div class="catalog-table-body-row-quantity-color-row-transit-quantity">
                                        <span>
                                            <?= number_format($color['transit'],0,'',' '); ?> шт.
                                        </span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <?php $remains_item++; ?>
                                    <?php else: ?>
                                        <?php $out_item++; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                            <?php endif; ?>
                            <?php if(isset($remains_item) and $remains_item>2): ?>
                                <a href="#" class="other-color-show-hide" data-id="<?= $item['id']; ?>">другие цвета</a>
                            <?php endif; ?>
                            <?php if(isset($item['color']) && sizeof($item['color'])==$out_item): ?>
                                <span style="margin: 0 auto; text-align:center;">В производстве</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="catalog-table-body-row-info">
                        <div class="catalog-table-body-row-info-quantity">
                            <span>на складе <?= (isset($all_remains)) ? number_format($all_remains,0,'',' ') : '0'; ?> шт.</span>
                        </div>
                        <div class="catalog-table-body-row-info-more">
                            <a href='/<?= $item['slug']; ?>'>
                                <span>Подробнее ></span>
                            </a>
                        </div>
                    </div>
                    <div class="catalog-table-body-row-cart">
                        <a href='#' class="add_to_cart" data-id='<?= $item['id']; ?>' style="">
                            <span>В КОРЗИНУ ></span>
                            <img src="https://zaglushka.ru/images/cart.svg">
                        </a>
                    </div>
                    <?php if(!Yii::$app->user->isGuest): ?>
                        <div class="compare-catalog active" data-itemId="<?= $item['id']; ?>">
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star"></i>
                        </div>
                </div>
                <?php $i++; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
