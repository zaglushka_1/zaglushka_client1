<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Каталоги и брошюры';
?>
<style>
        .contacts-container {display: flex; background: #fff; width:100%; padding: 20px; flex-flow: wrap; justify-content: flex-start;}
        .item-catalog {flex-basis:25%; margin-bottom:50px; } 
        .item-catalog a {text-align:center; text-decoration: none; position: relative;}
        .item-catalog a img {max-width:95%; margin-bottom:20px; margin-left: 10px; margin-right:10px; box-shadow: 1px 1px 5px #b2b2b2;}
        .item-catalog a span {width:100%;text-align:center; display: inline-block; min-height: 60px;}
        .item-catalog a .product-item-table__btn {margin: 0 auto; display: block;text-decoration: none !important;}
        @media  (max-width: 400px) {
        .item-catalog {flex-basis: 100%;}    
        }
        @media  (min-width: 400px) and (max-width: 800px) {
        .item-catalog {flex-basis: 50%;}    
        }
        @media  (min-width: 800px) and (max-width: 1000px) {
        .item-catalog {flex-basis: 33%;}    
        }
    
</style>
<div class="site-contact-page">
    <h1><?= Html::encode($this->title) ?></h1>

<div class="catalog_content">
                <div class="contacts__title-wrap">
                    <h3 class="title">
                        <span>
                            Направление «Малые архитектурные формы»
                        </span>
                        <div class="contacts__tooltip" data-title="Направление включает в себя более 7000 наименований, среди которых заглушки для труб, опоры, фиксаторы, подпятники, колпачки, переходники и др."></div>
                    </h3>
                </div>
    <div class="contacts-list">
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/kanat-viking.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/kanat-viking.jpg'/><br />
                        <span>Канат Викинг</span><br /><br />                        
                        <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/detskie-kacheli-basic.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/detskie-kacheli-basic-cover.jpg'/><br />
                        <span>Детские качели Basic</span><br /><br />                        
                        <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/detskie-kacheli-forto.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/detskie-kacheli-forto-cover.jpg'/><br />
                        <span>Детские качели Forto</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/kanat-kombinirovanny-i-komplektuyuschie.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/kanat-kombinirovannyj-i-komplektuyuschie-cover.jpg'/><br />
                        <span>Канат комбинированный и комплектующие</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/skalodromnye-zatsepy.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/skalodromnye-zatsepy-cover.jpg'/><br />
                        <span>Скалодромные зацепы</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/homuty-dlya-workout-detskih-igrovyh-ploschadok-i-polos-prepyatstvij.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/homuty-dlya-workout-detskih-igrovyh-ploschadok-i-polos-prepyatstvij-cover.jpg'/><br />
                        <span>Хомуты для Workout, детских игровых площадок и полос препятствий</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/sidenya-dlya-kachelej.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/sidenya-dlya-kachelej-cover.jpg'/><br />
                        <span>Сиденье для качелей</span><br /><br />                        <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/sfericheskie-soediniteli-dlya-kanatnyh-konstruktsij.pdf' class="link">
                        <img href='https://zaglushka.ru/booklet_files/cover/sfericheskie-soediniteli-dlya-kanatnyh-konstruktsij-cover.jpg'/><br />
                        <span>Сферические соединители для канатных конструкций</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/hdpe-plastik.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/hdpe-plastik-cover.jpg'/><br />
                        <span>HDPE-пластик</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/bezopasnost-na-detskih-ploschadkah.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/bezopasnost-na-detskih-ploschadkah-cover.jpg'/><br />
                        <span>Безопасность на детских площадках</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/kolpachki-dlya-trub.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/kolpachki-dlya-trub-cover.jpg'/><br />
                        <span>Колпачки для труб</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/hdpe-plastik-nashi-resheniya-dlya-blagoustrojstva-goroda.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/hdpe-plastik-nashi-resheniya-dlya-blagoustrojstva-goroda-cover.jpg'/><br />
                        <span>HDPE-пластик наши решения для благоустройства города</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/vstraivaemye-batuty.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/vstraivaemye-batuty-cover.jpg'/><br />
                        <span>Встраиваемые батуты</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
    </div>
                <div class="contacts__title-wrap">
                    <h3 class="title">
                        <span>
                                Направление «Фурнитура»
                        </span>
                        <div class="contacts__tooltip" data-title="Направление включает в себя более 7000 наименований, среди которых заглушки для труб, опоры, фиксаторы, подпятники, колпачки, переходники и др."></div>
                    </h3>
                </div>
        <div class="contacts-list">
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/katalog-tehnicheskoj-plastikovoj-furnitury.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/katalog-tehnicheskoj-plastikovoj-furnitury2017-cover.jpg'/><br />
                        <span>Каталог технической пластиковой фурнитуры</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/katalog-plastikovoj-furnitury.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/katalog-plastikovoj-furnitury-cover.jpg'/><br />
                        <span>Каталог пластиковой фурнитуры</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/novaya-opora-na-bolt-i-gajku.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/novaya-opora-na-bolt-i-gajku-cover.jpg'/><br />
                        <span>Новая опора на болт и гайку</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
        </div>
                <div class="contacts__title-wrap">
                    <h3 class="title">
                        <span>
                                Направление «Мебель»
                        </span>
                        <div class="contacts__tooltip" data-title="Направление включает в себя более 7000 наименований, среди которых заглушки для труб, опоры, фиксаторы, подпятники, колпачки, переходники и др."></div>
                    </h3>
                </div>
    <div class="contacts-list">
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Italseat-2017.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Italseat-2017-cover.jpg'/><br />
                        <span>Каталог</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Aria.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Aria-cover.jpg'/><br />
                        <span>Aria</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/College.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/College-cover.jpg'/><br />
                        <span>College</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Duetto.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Duetto-cover.jpg'/><br />
                        <span>Duetto</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Feel.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Feel-cover.jpg'/><br />
                        <span>Feel</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Goa.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Goa-cover.jpg'/><br />
                        <span>Goa</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Lux.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Lux-cover.jpg'/><br />
                        <span>Lux</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Manila.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Manila-cover.jpg'/><br />
                        <span>Manila</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Pluto.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Pluto-cover.jpg'/><br />
                        <span>Pluto</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Q5.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Q5-cover.jpg'/><br />
                        <span>Q5</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Race.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Race-cover.jpg'/><br />
                        <span>Race</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Teddy.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Teddy-cover.jpg'/><br />
                        <span>Teddy</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
                <div class="item-catalog">
                    <a download href='https://zaglushka.ru/booklet_files/pdf/Swiss.pdf' class="link">
                        <img src='https://zaglushka.ru/booklet_files/cover/Swiss-cover.jpg'/><br />
                        <span>Swiss</span><br /><br />                        
                            <button class="product-item-table__btn">Скачать</button>
                    </a>
                </div>
            </div>
</div>
</div>
