<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\JqueryAsset;

AppAsset::register($this);
JqueryAsset::register($this);
?>
    <table class="header">
        <tbody>
            <tr class="header__row">
                <td class="header__logo">
                    <img src="https://zaglushka.ru/images/logo_print.png">
                </td>
                <td class="header__address">
                    <strong>ОТДЕЛ ПРОДАЖ</strong>
                    <br>
                    <strong>тел. 7 (812) 242-80-99</strong>
                    <br>
                    <strong>тел. 7 (800) 555-04-99</strong>
                    <br>
                    <br>
                    <p>
                        <strong>АДРЕС СКЛАДА:</strong>
                        <br>
                        г. Санкт-Петербург, п. Парголово ул. Подгорная, дом 6. Въезд с ул. Железнодорожная, дом 11 На территории АНГАР № 12 График работы: Пн-Пт, 9:00 - 18:00
                    </p>
                </td>
                <td class="header__contacts">

                </td>
            </tr>
        </tbody>
    </table>
<br /><br />
    <main>
    <?php if(sizeof($cart)>0): ?>
        <table class="list" width="100%">
            <thead>
            <tr>
                <th>№</th>
                <th>Фото</th>
                <th>Артикул</th>
                <th>Описание</th>
                <th>Цвет</th>
                <th class="nowrap">Кол-во штук</th>
                <th>Оптовая группа</th>
                <th class="nowrap">Цена за 1 штуку</th>
                <th>Сумма</th>
            </tr>
            </thead>
            <tbody>
                <?php $i=1; ?>
                <?php $all=0; ?>
                <?php foreach($cart as $cart_element): ?>
                <tr>
                    <td><?= $i; ?></td>
                    <td>
                        <?php
                            if(substr($cart_element['item']['items_id'], -2)=="00") {
                                $imagePath="/images/item/0/".$cart_element['item']['items_id']."/";
                            } else {
                                $imagePath="/images/item/".substr($cart_element['item']['items_id'], -2)."/".$cart_element['item']['items_id']."/";
                            }
                        ?>
                        <?php $img="{$imagePath}1.jpg"; ?>
                        <?php if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                            <img src="<?= $img; ?>" alt="4НЧК" width="95" height="95">
                        <?php else: ?>
                            <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                        <?php endif; ?>
                    </td>
                    <?php if($cart_element['quantity']=="free"): ?>
                        <td>
                            <?= $cart_element['item']['items_colors_code']; ?>
                        </td>
                        <td>
1
                        </td>
                        <td>
                            <?= $cart_element['item']['colorname']; ?>
                        </td>
                        <td colspan="4">
                            Бесплатные образцы
                        </td>
                    <?php else: ?>
                        <?php
                            $group_num=0;
                            if($cart_element['quantity']<100) {
                                $group_num=0;
                                $group_name="розница";
                            }
                            if($cart_element['quantity']>=100 && $cart_element['quantity']<500) {
                                $group_num=1;
                                $group_name="от 100 до 500";
                            }
                            if($cart_element['quantity']>=500 && $cart_element['quantity']<1000) {
                                $group_num=2;
                                $group_name="от 500 до 1000";
                            }
                            if($cart_element['quantity']>=1000 && $cart_element['quantity']<5000) {
                                $group_num=3;
                                $group_name="от 1000 до 5000";
                            }
                            if($cart_element['quantity']>=5000 && $cart_element['quantity']<10000) {
                                $group_num=4;
                                $group_name="от 5000 до 10000";
                            }
                            if($cart_element['quantity']>=10000) {
                                $group_num=5;
                                $group_name="от 10000";
                            }
                        ?>
                        <td>
                            <?= $cart_element['item']['items_colors_code']; ?>
                        </td>
                        <td>
                            1
                        </td>
                        <td>
                            <?= $cart_element['item']['colorname']; ?>
                        </td>
                        <td>
                            <?= $cart_element['quantity']; ?>
                        </td>
                        <td>
                            <?= $group_name; ?>
                        </td>
                        <td>
                            <?= $cart_element['item']['price'.$group_num]; ?> р.
                        </td>
                        <td>
                            <?php $sum=$cart_element['item']['price'.$group_num]*$cart_element['quantity']; ?>
                            <?= $sum; ?> р.
                            <?php $all=$all+$sum; ?>
                        </td>
                    <?php endif; ?>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <p class="total">
            Стоимость продукции: <?= $sum; ?> руб.
        </p>
    <?php endif; ?>
        <p>
            До бесплатной доставки "до дверей" в городе Санкт-Петербург в заказе не хватает товаров на сумму 3 080 рублей.
        </p>
        <p>Бесплатная доставка не распространяется на горки, канаты "Викинг", стальные цепи, сиденья и каркасы для стульев.</p>
    </main>

