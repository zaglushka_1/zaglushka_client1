<?php

use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Zaglushka.ru';
?>
<div class="site-index">
    <h1 class="site-index-h1">Изготовление пластиковых заглушек</h1>
    <div class="catalog_element">
    <?php foreach($catalog_element as $top_element): ?>
        <h2 class="catalog_link">
            <a href="<?= $top_element['top_element']['slug']; ?>">
                <?php if(file_exists($_SERVER['DOCUMENT_ROOT']."/images/breadcrumbs/categories/".$top_element['top_element']['id'].".jpg")): ?>
                    <?php $img="/images/breadcrumbs/categories/".$top_element['top_element']['id'].".jpg"; ?>
                <?php elseif(file_exists($_SERVER['DOCUMENT_ROOT']."/images/cat/".$top_element['top_element']['id']."_list.jpg")): ?>
                    <?php $img="/images/cat/".$top_element['top_element']['id']."_list.jpg"; ?>
                <?php else: ?>
                    <?php $img="/images/dummy_".$top_element['top_element']['id'].".png"; ?>
                <?php endif; ?>
                <div class="top_element_img">
                    <img src="<?= $img; ?>" width="95" height="95"/>
                </div>
                <div class="top_element_link">
                    <?= $top_element['top_element']['title']; ?>
                </div>
            </a>
        </h2>
        <?php if(sizeof($top_element['sub_element'])>0): ?>
        <div class="sub_elements">
            <?php foreach($top_element['sub_element'] as $sub_element): ?>
            <div class="sub_element">
                <a href="<?= $sub_element['slug']; ?>">
                    <div class="sub_cat_img">
                        <img src="/images/cat/<?= $sub_element['id']; ?>_list.jpg" />
                    </div>
                    <div class="sub_cat_title">
                        <span><?= $sub_element['title']; ?></span>
                    </div>
                    <div class="sub_cat_remains">
                        <span>В наличии <?= (isset($sub_element['quantity'])) ? number_format($sub_element['quantity'],0,'',' ') : 0; ?> шт.</span>
                    </div>
                </a>
            </div>
            <?php endforeach; ?>
        </div>
        <?php endif; ?>
    <?php endforeach; ?>
    </div>
    <div class="p-main-epilog">
	<section class="p-main-epilog__block">
            <h2 class="p-main-epilog__title">Компания «Заглушка.Ру» – крупнейший производитель и поставщик пластиковой фурнитуры</h2>
            <p class="p-main-epilog__paragraph">Мы производим и продаём заглушки, опоры, фиксаторы, колпачки и другую пластиковую фурнитуру уже более 25 лет. «Заглушка.Ру» – это более 50'000'000 изделий в наличии на складе в Санкт-Петербурге, более 20'000 товаров в каталоге, около 2'500 наименований производимой продукции, общая площадь складских помещений – 7'500 м², около 30'000 довольных клиентов ежегодно. Сегодня с уверенностью можно сказать, что наша компания занимает лидирующие позиции на российском рынке пластиковой фурнитуры.</p>
            <p class="p-main-epilog__paragraph">Пластиковые заглушки составляют основу нашего ассортимента. Они имеют ряд преимуществ: устойчивы к коррозии, воздействию солнечных лучей и перепадам температур, просто и легко монтируются, служат долгие годы. Пластиковые заглушки, представленные в нашем каталоге, универсальны в использовании и применяются в различных сферах производства.</p>
	</section>
	<section class="p-main-epilog__block">
            <h2 class="p-main-epilog__title">Пластиковые заглушки в широком ассортименте</h2>
            <p class="p-main-epilog__paragraph">Наша компания предлагает <a class="link" href="https://zaglushka.ru/zaglushki-dlya-trub">заглушки для труб различных размеров</a> и форм в разнообразной цветовой палитре. Мы постоянно работаем над расширением ассортимента: разрабатываем новые изделия, в том числе изготавливаем продукцию под заказ по индивидуальным запросам, проектируем и производим новые пресс-формы, заключаем договоры с российскими и зарубежными фирмами-поставщиками.</p>
            <p class="p-main-epilog__paragraph">Собственное производство полипропиленовых заглушек, опор, фиксаторов и другой фурнитуры постоянно обеспечивает склад продукцией, поэтому 90% представленных в каталоге изделий всегда в наличии. Сотрудничая с нами, вы получаете продукцию по конкурентоспособной цене в кратчайшие сроки.</p>
            <p class="p-main-epilog__paragraph">Изготовление пластиковых заглушек – основное, но не единственное направление деятельности нашей компании. У нас вы можете найти широкий ряд пластиковой фурнитуры, необходимой для производства мебели, а также различные комплектующие для обустройства детских и спортивных площадок.</p>
            <p class="p-main-epilog__paragraph">Мы делаем всё возможное, чтобы стать надёжным партнёром вашей компании. У нас вы обязательно найдёте продукцию для разных сфер производства, потому что «Заглушка.Ру» – крупнейший в России производитель и поставщик пластиковой фурнитуры.</p>
    	</section>
    </div>
</div>
