<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Способы оплаты';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-pay-page">
    <h1 style="font-size:24px;margin: 20px 10px;"><?= Html::encode($this->title) ?></h1>
    <div class="catalog_content">
    <p>В зависимости от того, являетесь ли вы юридическим или физическим лицом, вы можете ознакомиться с условиями и способами оплаты услуг.</p>
    <h2>Для юридических лиц</h2>
    <h3 class="pay pay-wallet">Безналичный расчет</h3>
    <p>Размер комиссии зависит от банка, через который осуществляется платеж. Оплата фиксируется по рабочим дням, только по факту поступления средств на расчетный счет компании. <a href="https://zaglushka.ru/files/rekvizity.pdf" download="">Реквизиты компании</a></p>
    <h2>Для физических лиц</h2>
    <h3>Банковские карты</h3>
    <div class="payments-logo">
        <ul class="payments">
            <li class="visa"></li>
            <li class="maestro"></li>
            <li class="mastercard"></li>
            <li class="mir"></li>
            <li class="jcb"></li>
        </ul>
    </div>
    <p>Автоматическое зачисление средств в течение 10–15 минут. Совершить оплату можно с карт Visa, Visa Electron, MasterCard, Maestro, Eurocard и МИР.</p>
    <h3>Электронные деньги</h3>
    <div class="payments-logo">
        <ul class="payments">
            <li class="yandexmoney"></li>
            <li class="webmoney"></li>
            <li class="qiwi"></li>
        </ul>
    </div>
    <p>Автоматическое зачисление средств в течение 5–10 минут.</p>
    <h3>Интернет-банки</h3>
    <div class="payments-logo">
        <ul class="payments">
            <li class="sberbank"></li>
            <li class="alfa-click"></li>
            <li class="tinkoff"></li>
        </ul>
    </div>
    <p>Автоматическое зачисление средств в течение 5–10 минут.</p>
    <!--<h3>Наложенный платеж для физических и юридических лиц</h3>
    <div class="payments-logo">
        <ul class="payments">
            <li class="nn-icon"></li>
        </ul>
   </div>
    <p>Наложенный платеж – это оплата товара при его получении. Способ оплаты – наличный или безналичный расчет. Отправка посылок наложенным платежом осуществляется с помощью транспортной компании СДЭК.</p>
    </div> -->
    </div>
</div>
