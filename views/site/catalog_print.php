<?php

use yii\helpers\Url;
use app\assets\AppAsset;
use yii\web\JqueryAsset;

AppAsset::register($this);
JqueryAsset::register($this);
?>

<table border="0">
    <tr>
        <td rowspan="9"><img src="https://zaglushka.ru/images/logo_print.png" /></td>
        <td>
            <p><strong>ОТДЕЛ ПРОДАЖ</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>тел. 7 (812) 242-80-99</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>тел. 7 (800) 555-04-99</strong></p></td>
    </tr>
    <tr>
        <td>
            <p></td>
    </tr>
    <tr>
        <td>
            <p><strong>АДРЕС СКЛАДА</strong></p></td>
    </tr>
    <tr>
        <td><p>г. Санкт-Петербург, п. Парголово ул. Подгорная</p></td>
    </tr>
    <tr>
        <td><p>дом 6. Въезд с ул. Железнодорожная, дом 11</p></td>
    </tr>
    <tr>
        <td><p>На территории АНГАР № 12</p></td>
    </tr>
    <tr>
        <td><p>График работы: Пн-Пт, 9:00 - 18:00</p></td>
    </tr>
</table>
    <h1><?= $catalog_element['title']; ?></h1>
<?php if(isset($catalog_element['catalog_items']) && sizeof($catalog_element['catalog_items'])>0 ): ?>
    <table class="catalog-print-table">
        <thead>
            <tr>
                <th rowspan="2">№</th>
                <th rowspan="2">Артикул</th>
                <th rowspan="2">Схема</th>
                <th rowspan="2">Фото</th>
                <th colspan="5">Цена, руб. за 1 шт.</th>
                <th rowspan="2">Упаковка</th>
            </tr>
            <tr>
                <th>от 5 000</th>
                <th>от 1 000 до 5 000</th>
                <th>от 500 до 1 000</th>
                <th>от 100 до 500</th>
                <th>розница до 100</th>
            </tr>
        </thead>
        <tbody>
        <?php $i=1; ?>
        <?php foreach($catalog_element['catalog_items'] as $key => $item): ?>
        <?php if($key!='pages' && $key!='filter_no_result'): ?>
        <tr>
            <td>
                <p><?= $i; ?></p>
            </td>
            <td>
                <p>
                    <?= $item['title']; ?>
                </p>
            </td>
            <td>
                <?php $img="/images/item/".substr($key, -2)."/".$key."/18.jpg";?>
                <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                    <img src="<?= $img; ?>" width="110" heigh="95"/>
                <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
            <td>
                <?php $img="/images/item/".substr($key, -2)."/".$key."/5.jpg";?>
                    <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                        <img src="<?= $img; ?>" width="110" heigh="95"/>
                    <?php else: ?>
                    <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                <?php endif; ?>
            </td>
            <?php for($j=5;$j>=1;$j--): ?>
                <td>
                    <span><?= $item['price'.$j]; ?> р.</span>
                </td>
            <?php endfor; ?>
            <td>
                <p>
                    <?php if($item['pack_size']>0): ?>
                        <?= $item['pack_size']; ?>
                    <?php else: ?>
                    <?php endif; ?>
                </p>
            </td>
        </tr>
        <?php $i++; ?>
        <?php endif; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
<p>Все цены указаны в рублях и включают в себя НДС.</p>
<p>Все цены указаны при условии 100% предоплаты и действительны в течение 5 дней.</p>
<p>Все цены указаны при условии самовывоза продукции со склада в г. Санкт-Петербург</p>
<p>(для иногородних клиентов доставка до склада ТК в г. Санкт-Петербург бесплатна).</p>
<p>Все цены указаны за стандартный цвет для каждого вида изделия</p>
<p>(изменение цвета на любой из предлагаемой палитры +30% к стоимости, изменение цвета на любой по системе RAL +50% к стоимости).</p>
<p>О возможных сроках поставки заказного товара, необходимо уточнять заранее.</p>
