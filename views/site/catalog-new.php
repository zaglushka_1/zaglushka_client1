<?php

use yii\helpers\Url;
use yii\widgets\LinkPager;
use app\assets\AppAsset;
use yii\web\JqueryAsset;
use yii\web\JsExpression;



/*for($i=sizeof($catalog_element['breadcrumbs'])-1;$i>=0;$i--){
    $this->params['breadcrumbs'][] = array(
        'label'=> $catalog_element['breadcrumbs'][$i]['title'],
        'url'=>$catalog_element['breadcrumbs'][$i]['url']
    );
}*/

AppAsset::register($this);
JqueryAsset::register($this);
//print_r($data->cat->items);
?>

<?php if($data->cat->catTags): ?>
    <div class="slider top_tag_container">
    <?php foreach($data->cat->catTopTags as $tag): ?>
        <div>
            <a class="top_tag_link" href="/<?= $tag->slug; ?>/<?= $tag->slug; ?>">
                <img src="/images/breadcrumbs/tags/<?= $tag->cat_id; ?>_<?= $tag->id; ?>.jpg" width="70" height="70"/>
                <span><?= $tag->title; ?></span>
            </a>
        </div>
    <?php endforeach; ?>
    </div>
<?php endif; ?>
<?php if($data->cat->child): ?>
    <div class="sub_elements">
        <?php foreach($data->cat->child as $child): ?>
            <div class="sub_element">
                <a href="<?= $child->slug->slug; ?>">
                    <div class="sub_cat_img">
                        <img src="/images/cat/<?= $child->id; ?>_list.jpg" />
                    </div>
                    <div class="sub_cat_title">
                        <span><?= $child->title; ?></span>
                    </div>
                    <div class="sub_cat_remains">
                        <!-- -->
                        <span>В наличии <?= $child->remains; ?> шт.</span>
                        <?php //($child->remains) ? number_format($child->remains,'',' ') : 0; ?>
                    </div>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>


<?php if(isset($data->cat->items) && sizeof($data->cat->items)>0 ): ?>
    <!-- Фильтры -->
    <div class="catalog-table">
        <div class="catalog-table-head">
            <div class="catalog-table-head-position">
                <span>№</span>
            </div>
            <div class="catalog-table-head-articul">
                <span>Артикул</span>
            </div>
            <div class="catalog-table-head-images">
                <span></span>
            </div>
            <div class="catalog-table-head-prices">
                <div class="catalog-table-head-prices-head">
                    <span>Цена за 1 шт.</span>
                </div>
                <div class="catalog-table-head-prices-10000">
                    <span>от 10 000</span>
                </div>
                <div class="catalog-table-head-prices-5000">
                    <span>от 5 000</span>
                </div>
                <div class="catalog-table-head-prices-1000">
                    <span>от 1 000</span>
                </div>
                <div class="catalog-table-head-prices-500">
                    <span>от 500</span>
                </div>
                <div class="catalog-table-head-prices-100">
                    <span>от 100</span>
                </div>
                <div class="catalog-table-head-prices-1">
                    <span>розница</span>
                </div>
            </div>
            <div class="catalog-table-head-quantity">
                <span>Наличие</span>
            </div>
            <div class="catalog-table-head-cart">
                <span></span>
            </div>
        </div>
        <?php $i=1; ?>
        <?php foreach($data->cat->items as $key => $item): ?>
            <?php if($key!='pages' && $key!='filter_no_result'): ?>
                <div class="catalog-table-body-row">
                    <div class="catalog-table-body-row-position">
                        <span><?= $i ;?></span>
                    </div>
                    <div class="catalog-table-body-row-articul">
                        <!--<span>Артикул:</span> -->
                        <a href='/<?= $item->slug->slug; ?>'>
                            <span><?= $item->title; ?></span>
                        </a>
                    </div>
                    <div class="catalog-table-body-row-images">
                        <a href='/<?= $item->slug->slug; ?>'>
                            <?php $img="/images/item/".substr($item->id, -2)."/".$item->id."/18.jpg";?>
                            <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                                <img src="<?= $img; ?>" width="110" heigh="95"/>
                            <?php else: ?>
                                <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                            <?php endif; ?>
                        </a>
                        <?php $img="/images/item/".substr($item->id, -2)."/".$item->id."/5.jpg";?>
                        <a href='/<?= $item->slug->slug; ?>'>
                            <?php if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)): ?>
                                <img src="<?= $img; ?>" width="110" heigh="95"/>
                            <?php else: ?>
                                <img class="product-table__image" src="https://zaglushka.ru/images/dummy_95.png" alt="" height="95" width="95">
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="catalog-table-body-row-prices">
                        <?php $price_head=['розница', 'от 100', 'от 500', 'от 1 000', 'от 5 000', 'от 10 000']; ?>
                        <div class="catalog-table-body-row-price catalog-table-body-row-price-head">
                            <span>Кол-во, шт.</span>
                            <span>Цена за шт.</span>
                        </div>
                        <?php for($j=5;$j>=0;$j--): ?>
                            <div class="catalog-table-body-row-price">
                                <span><?= $price_head[$j]; ?></span>
                                <?php $price_key="price".$j; ?>
                                <span><?= $item->$price_key; ?> р.</span>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="catalog-table-body-row-quantity">
                        <div class="catalog-table-body-row-quantity-colors">
                            <?php if(isset($item->itemColors) && sizeof($item->itemColors)>0): ?>
                                <?php $out_item=0; ?>
                                <?php $remains_item=0; ?>
                                <?php $all_remains=0; ?>
                                <?php foreach($item->itemColors as $color): ?>
                                    <?php if(((int)$color->remains>0 or isset($color->transit->quant))): ?>
                                        <?php //print_r($color); ?>
                                        <div class="catalog-table-body-row-quantity-color-row<?= ($remains_item>2) ? "" : "";?>" data-item_id="<?= $item->id; ?>">
                                            <div class="catalog-table-body-row-quantity-color-row-remains">
                                                <div class="catalog-table-body-row-quantity-color-row-remains-colorhex">
                                                    <div class="" style="width: 12px; height: 12px; border: 1px solid #cacaca; background-color: #<?= $color->colors->hex; ?>; margin-right:5px; margin-top:5px;">

                                                    </div>
                                                </div>
                                                <div class="catalog-table-body-row-quantity-color-row-remains-colorname">
                                        <span>
                                            <?= $color->colors->name; ?>
                                        </span>
                                                </div>
                                                <div class="catalog-table-body-row-quantity-color-row-remains-quamtity">
                                        <span>
                                            <?php $all_remains+=(int)$color->remains; ?>
                                            <?= number_format($color->remains,0,'',' '); ?> шт.
                                        </span>
                                                </div>
                                            </div>
                                            <?php print_r($color->transit); ?>
                                            <?php if(isset($color->transit->quant)): ?>
                                                <div class="catalog-table-body-row-quantity-color-row-transit">
                                                    <div class="catalog-table-body-row-quantity-color-row-transit-transit">
                                                        <span>+едет</span>
                                                    </div>
                                                    <div class="catalog-table-body-row-quantity-color-row-transit-quantity">
                                                        <span>
                                                            <?= number_format($color->transit->quant,0,'',' '); ?> шт.
                                                        </span>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <?php $remains_item++; ?>
                                    <?php else: ?>
                                        <?php $out_item++; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else: ?>
                                <span style="margin: 0 auto; text-align:center;">В производстве</span>
                            <?php endif; ?>
                            <?php if($remains_item>2): ?>
                                <a href="#" class="other-color-show-hide" data-id="<?= $item->id; ?>">другие цвета</a>
                            <?php endif; ?>
                            <?php if(sizeof($item->itemColors)==$out_item): ?>
                                <span style="margin: 0 auto; text-align:center;">В производстве</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="catalog-table-body-row-info">
                        <div class="catalog-table-body-row-info-quantity">
                            <span>на складе <?= number_format($all_remains,0,'',' '); ?> шт.</span>
                        </div>
                        <div class="catalog-table-body-row-info-more">
                            <a href='/<?= $item->slug->slug; ?>'>
                                <span>Подробнее ></span>
                            </a>
                        </div>
                    </div>
                    <div class="catalog-table-body-row-cart">
                        <a href='#' class="add_to_cart" data-id='<?= $item->id; ?>' style="">
                            <span>В КОРЗИНУ ></span>
                            <img src="https://zaglushka.ru/images/cart.svg">
                        </a>
                    </div>
                </div>
                <?php $i++; ?>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php if(isset($catalog_element['catalog_content'])): ?>
    <?php $this->title = $catalog_element['catalog_content'][0]['title_h1']; ?>
    <div class="catalog_content">
        <h1><?= $catalog_element['catalog_content'][0]['title_h1']; ?></h1>
        <?= $catalog_element['catalog_content'][0]['descr']; ?>
    </div>
<?php endif; ?>