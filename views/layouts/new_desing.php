<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
//AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="yandex-verification" content="586690f6264a9b6d" />
        <?= Html::csrfMetaTags() ?>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
        <link href="/css/site.css" rel="stylesheet"/>
        <title></title>

        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
        <header class="container-fluid fixed-top" style="padding: 0; margin:0">
            <div class="" style="background:#fff;min-height:150px;">
                <div class="container">
                    <section class="section-1">
                        <div class="row justify-content-end" >
                            <div class="col-3">
                                <div class="row justify-content-around">
                                    <div class="col">
                                        <a href="tel:78122428099" class="header-phone">8 (812) 242 80 99 </a>
                                    </div>
                                    <div class="col">
                                        <a href="/change_city">Санкт-Петербург</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3">
                                <a href="tel:78005550499" title="Телефон +7 800 555 04 99" alt="Телефон +7 800 555 04 99" class="header-phone">8 (800) 555 04 99</a>
                                <span>Бесплатно по России</span>
                            </div>
                            <div class="col-1">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Ру
                                    </button>
                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="#">En</a></li>
                                        <li><a class="dropdown-item" href="#">It</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="section-2">
                        <div class="row">
                                <div class="col-1">
                                    лого
                                </div>
                                <div class="offset-1 col-5">
                                    <div class="input-group flex-nowrap">
                                        <input type="text" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm">
                                        <span class="input-group-text" id="inputGroup-sizing-sm">Small</span>
                                    </div>
                                </div>
                                <div class="col-2 offset-1 ">
                                    Вход регистрация
                                </div>
                                <div class="col-2">
                                    Корзина
                                </div>
                        </div>
                    </section>
                    <section class="section-3">
                        <div class="row">
                            <div class="col-1">
                                Каталог
                            </div>
                            <div class="col-8 offset-1">
                                <ul>
                                    <li>
                                        <a href="/about">О компании</a>
                                    </li>
                                    <li>
                                        <a href="/delivery">Доставка</a>
                                    </li>
                                    <li>
                                        <a href="/contacts">Контакты</a>
                                    </li>
                                    <li>
                                        <a href="/booklets">Брошюры</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </header>

        <main class="container" style="height: 10000px; margin-top: 160px;">
            <?= $content ?>
        </main>

        <footer>
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            лого
                        </div>

                    </div>
                </div>
            </div>
        </footer>



    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
