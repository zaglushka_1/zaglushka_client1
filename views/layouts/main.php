<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use yii\bootstrap\Tabs;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="yandex-verification" content="586690f6264a9b6d" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" type="text/css" href="/plugin/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/plugin/slick/slick-theme.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/suggestions-jquery@20.3.0/dist/css/suggestions.min.css" rel="stylesheet" />
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="header-container">
        <?= $this->render('@app/views/includes/header', []); ?>
    </div>
    <div class="menu-container">
        <?= $this->render('@app/views/includes/top-menu', ['catalog_element'=>Yii::$app->params['catalog_element']]); ?>
    </div>
    <div class="breadcrumbs" style="border-bottom: 1px solid #cdcdcd;">
        <div class="container">
            <div class="flex-container" style="display:flex; flex-flow: nowrap; justify-content: space-between;">
                <div class="breadcrumb-container">
                    <?php if(isset($this->params['breadcrumbs'])) :?>
                    <div class="breadcrumb-start" style="position: relative;">
                        <svg class="icon icon_arrow-breadcrumbs">
                            <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#arrow-breadcrumbs"></use>
                        </svg>
                    </div>
                    <?php endif; ?>
                    <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],'homeLink'=>false]) ?>
                </div>
                <?php if(strpos(Yii::$app->request->absoluteUrl, "?")!=FALSE): ?>
                    <?php $url_glue="&"; ?>
                <?php else: ?>
                    <?php $url_glue="?&"; ?>
                <?php endif; ?>
                <div class="user_container">
                    <?php if(isset(Yii::$app->params['print'])): ?>
                    <ul class="print_version_list">
                        <li class="print"><a href="<?= Yii::$app->request->absoluteUrl; ?><?= $url_glue; ?>print=1">
                                <svg class="page-actions__icon icon icon_print">
                                    <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#print"></use>
                                </svg>
                                <span>Распечатать</span>
                            </a>
                        </li>
                        <li class="excel"><a href="<?= Yii::$app->request->absoluteUrl; ?><?= $url_glue; ?>excel=1">
                                <svg class="page-actions__icon icon icon_excel">
                                    <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#excel"></use>
                                </svg>
                                <span>Скачать (excel)</span>
                            </a>
                        </li>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    
    <div class="container" style="position:relative;">
        <?= $content ?>
    <!-- Вынести в компонент -->
        <div class="popup_cart_free">
            <div class="header-cart__title">
                <div class="header-cart__header-icon">
                    <svg class="icon icon_checkcart">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#checkcart"></use>
                    </svg>                </div>
                <span>Бесплатные образцы добавлены в корзину</span>
            </div>

            <div class="header-cart__footer-free">
                <a href="/cart" style="width: 100%; margin-top: 10px;">Оформить заказ</a>
            </div>
        </div>
        <div class="popup_cart">
            <div class="popup_cart_header">
                <div class="popup_cart_header_icon">
                    <div class="popup_cart_header_icon_svg">
                        <svg class="icon icon_checkcart">
                            <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#checkcart"></use>
                        </svg>
                    </div>
                    <span>Товар добавлен в корзину</span>
                </div>
                <div class="popup_cart_header_close">
                    <svg class="header-cart__close icon icon_close-thin">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#close-thin"></use>
                    </svg>
                </div>
            </div>
            <div class="popup_cart_header_table">
                <div class="popup_cart_header_table_image"></div>
                <div class="popup_cart_header_table_color">
                    <span>Цвет</span>
                </div>
                <div class="popup_cart_header_table_quantity">
                    <span>Кол-во</span>
                </div>
                <div class="popup_cart_header_table_price">
                    <span>Сумма</span>
                </div>
            </div>
            <div class="popup_cart_header_table_data">
                <div class="popup_cart_header_table_data_image">
                    <img src="" width="60" height="60"/>
                </div>
                <div class="popup_cart_header_table_data_color">
                    <span></span>
                </div>
                <div class="popup_cart_header_table_data_quantity">
                    <span></span>
                </div>
                <div class="popup_cart_header_table_data_price">
                    <span></span>
                </div>
            </div>
            <div class="popup_cart_header_table_delivery">
                <div class="popup_cart_header_table_delivery_icon">
                    <svg class="header-cart__delivery-icon icon icon_car">
                        <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#car"></use>
                    </svg>
                </div>
                <div class="popup_cart_header_table_delivery_text">
                    <span>Для бесплатной доставки добавьте в корзину товаров на <span class="popup_cart_header_table_delivery_text_free_delivery"></span> p.</span>
                </div>
            </div>
            <div class="popup_cart_header_table_button">
                <a href="#" class="btn btn_outline_primary header-cart__btn header-cart__close header-cart__close-btn" style="flex-basis: 48%;">Продолжить покупки</a>
                <a href="/cart" class="btn btn_outline_primary header-cart__btn js-popup_checkout" style="flex-basis: 48%;">Оформить заказ</a>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="footer-one">
        <div class="container">
            <div class="footer-section">
                <div class="footer-question">
                    <p>Есть вопросы?</p>
                </div>
                <div class="footer-phone">
                    <p><span class="mobile_hide">Звоните: </span><a  href="tel:+78005550499">8 (800) 555 04 99</a><br /><small>(звонок по России бесплатный)</small></p>
                </div>
                <div class="footer-local-phone">
                    <a href="tel:+78005550499"><?= (Yii::$app->params['local_phone']) ? Yii::$app->params['local_phone'] : ''; ?></a>
                    <p>
                        <span class="change-town"><?= (Yii::$app->params['city']) ? Yii::$app->params['city'] : ''; ?></span>
                    </p>
                </div>
                <div class="footer-price">
                    <a  href="https://zaglushka.ru/files/price.xlsx" target="_blank"><svg class="header-price__icon icon icon_download">
                            <use xlink:href="/images/sprite.svg?id=a471817642adde4a4e32#download"></use>
                        </svg>
                        <span>Прайс-лист</span>
                    </a>
                </div>
                <div class="footer-email">
                    <p><span class="mobile_hide">Или пишите: </span><a href="mailto:sales:zaglushka.ru">sales@zaglushka.ru</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-two">
        <div class="container">
            <div class="footer-section">
                <div class="copyright">
                    <p>© 1991–2021 ООО «Заглушка.pу»</p>
                </div>
                <div class="social">
                    <p>Получайте самые выгодные предложения первыми - Подпишитесь на нас:</p>
                    <ul>
                        <li>
                            <a href="https://www.instagram.com/zaglushkaspb/" target="_blank">
                                <img src="https://zaglushka.ru/images/icons/inst.png" />
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCOPNLGn8SzBevpTPs4_VWQA" target="_blank">
                                <img src="https://zaglushka.ru/images/icons/youtube.png" />
                            </a>
                        </li>
                        <li>
                            <a href="https://vk.com/zaglushka_ru" target="_blank">
                                <img src="https://zaglushka.ru/images/icons/vk.png" />
                            </a>
                        </li>
                        <li>
                            <a href="https://wa.me/message/AWWEIHNDT6HVD1" target="_blank">
                                <img src="https://zaglushka.ru/images/icons/viber.png" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

    <div class="overlay">
        <div class="popup_container">
            
        </div>
        <div class="popup_container_city">
            <div class="city-selector">
                <div class="close-town-selector">
                    <a href="#">Закрыть</a>
                </div>

                <div class="city-selector-area">
                <?php if(isset(Yii::$app->params['cities'])): ?>
                    <?php
                    $countries=[1 => 'Россия', 2 => 'Казахстан', 3 => 'Беларусь', 4 => 'Украина'];
                    $items=[];
                    foreach(Yii::$app->params['cities'] as $country_id => $cities):
                        $item               = [];
                        $item['label']      = $countries[$country_id];
                        $item['content']    = $this->render('@app/views/includes/list-sities', ['cities'=>$cities,'country_id'=>$country_id]);
                        if($country_id===1) {
                            $item['active'] = true;
                        } else {
                            $item['active'] = false;
                        }
                        $items[]=$item;
                    endforeach;
                    echo Tabs::widget(['items'=>$items]);
                    ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="mob-town-selector">

        </div>
        <div class="mob-menu">
            <ul class="mob-menu-nav">
                <li class="header">
                    <span>Меню</span>
                </li>
                <li>
                    <a href="/">Продукция</a>
                </li>
                <li>
                    <a href="/about">О компании</a>
                </li>
                <li>
                    <a href="/delivery">Доставка</a>
                </li>
                <li>
                    <a href="/contacts">Контакты</a>
                </li>
                <li>
                    <a href="/booklet">Буклеты</a>
                </li>
                <li class="header">
                    <span>Покупки</span>
                </li>
                <li>
                    <a href="/cart">Корзина</a>
                </li>
                <li>
                    <a href="/specials">Товары со скидками</a>
                </li>
            </ul>
        </div>
    </div>
    <?php if(Yii::$app->user->isGuest): ?>
    <?php else: ?>
        <div class="usermenu">
            <ul>
                <li>
                    <p>
                        <?= Yii::$app->user->identity->username; ?>
                    </p>
                </li>
                <li class="footer-compare-link">
                    <p><a href="/compare">В сравнении: <?= Yii::$app->params['compare_count']; ?></a></p>
                </li>
                <li>Сумма для бесплатной доставки в город <?= Yii::$app->params['city']; ?>: </li>
            </ul>
        </div>
    <?php endif; ?>
    <?php $this->endBody() ?>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="/plugin/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/suggestions-jquery@20.3.0/dist/js/jquery.suggestions.min.js"></script>
</body>
</html>
<?php $this->endPage() ?>
