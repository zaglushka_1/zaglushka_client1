<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\ItemsModel;
use mysqli;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        $db=new mysqli('zaglushka.ru','zag_descr','axlid13DZqpcpYvG','zaglushka');
        $db->set_charset("utf8");
        $resource = $db->query('SELECT id, descr, short_descr FROM items');
        $counter=0;
        while($row = $resource->fetch_assoc()) {
            $short_descr=str_replace("<p>", "", $row['descr']);
            $short_descr=str_replace("</p>", "", $short_descr);
            $short_descr1=str_replace("&Oslash;", "", $row['short_descr']);
            //if($short_descr1==$short_descr || strpos($row['descr'], "<p>")===FALSE)
            //{
                $item=ItemsModel::find()->where(['id'=>$row['id']])->one();
                if(isset($item->descr) && $row['descr']!=$item->descr) {
                    $q = "UPDATE items SET descr='{$item->descr}' WHERE id='{$item->id}'";
                    $stmt = $db->prepare($q);
                    $stmt->execute();
                    $stmt->close();
                    print_r($row);
                    $counter++;
                }
            //}
        }
        print_r($counter);
        $resource->free();
        $db->close();

    }
}
