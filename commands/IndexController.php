<?php


namespace app\commands;
use Yii;
use yii\console\Controller;
use app\models\CachePagesModel;
use app\models\ItemsSizesClass;
use app\models\ItemsModel;
use app\models\SlugsModel;
use app\models\CatsModel;
use app\models\CatsFilterModel;
use app\models\ItemsTagsClass;
use app\models\TagsModel;

class IndexController extends Controller
{

    var $cat_count=[];
    var $cat_items=[];
    // перенести в модели
    private function setItemsSizesCache() {

    }

    // запись в кеш, убрать в модели
    private function setCache($data) {
        $cache=Yii::$app->cache;
        foreach($data as $key => $value) {
            $json_value=json_encode($value);
            $cache->set($key,$json_value);
            //echo "Ключ: $key установлен\n";
        }
    }

    //надо накопить очередь

    // кеш тегов
    private function setTagsCache()
    {
        $tags=TagsModel::find()->all();
        $tags_slugs = [];
        foreach ($tags as $tag) {
            $tags_slugs["tags_" . $tag->translit][] = $tag->id;
        }
        self::setCache($tags_slugs);
    }

    private function setItemsCache()
    {
        $cache = Yii::$app->cache;
        $items = ItemsModel::find()->where(['enabled' => 1])->all();
        $item_data_id = [];
        $item_data_slug_by_id = [];
        $item_data_tag_id = [];
        foreach ($items as $item) {
            $colors = [];
            foreach ($item->itemColors as $color) {
                $row = [];
                $row['id'] = $color->id;
                $row['color_id'] = $color->color;
                $row['color_name'] = $color->colors->name;
                $row['color_hex'] = $color->colors->hex;
                $row['main_color'] = $color->main;
                $row['remains'] = $color->remains;
                // переписать этот кусок, так как остатки накладываются друг на друга
                $remains = $cache->get("remains_" . $item->cat_id);
                if ($remains === false) {
                    $cache->set("remains_" . $item->cat_id, (int)$color->remains);
                } else {
                    $cache->set("remains_" . $item->cat_id, (int)$remains + (int)$color->remains);
                }
                $remains = $cache->get("remains_" . $item->add_cat_id);
                if ($remains === false) {
                    $cache->set("remains_" . $item->add_cat_id, (int)$color->remains);
                } else {
                    $cache->set("remains_" . $item->add_cat_id, (int)$remains + (int)$color->remains);
                }
                if (isset($color->transit)) {
                    $row['transit_date'] = $color->transit->date;
                    $row['transit'] = $color->transit->quant;
                    $transit = $cache->get("transit_" . $item->cat_id);
                    if ($transit === false) {
                        $cache->set("transit_" . $item->cat_id, (int)$color->transit->quant);
                    } else {
                        $cache->set("transit_" . $item->add_cat_id, (int)$transit + (int)$color->transit->quant);
                    }
                }
                $colors[] = $row;
            }
            $row = [];
            $row['cat_id'] = $item->cat_id;
            $row['add_cat_id'] = $item->add_cat_id;
            $row['code'] = $item->code;
            $row['title'] = $item->title;
            $row['price0'] = $item->price0;
            $row['price1'] = $item->price1;
            $row['price2'] = $item->price2;
            $row['price3'] = $item->price3;
            $row['price4'] = $item->price4;
            $row['price5'] = $item->price5;
            $row['price6'] = $item->price6;
            $row['pack_size'] = $item->pack_size;
            $row['descr'] = $item->descr;
            $row['weight'] = $item->weight;
            $row['volume'] = $item->volume;
            $row['colors'] = $colors;
            $row['slug'] = $item->slug->slug;
            $this->cat_items["cat_items_" . $item->cat_id][] = $item->id;
            $this->cat_items["cat_items_" . $item->add_cat_id][] = $item->id;
            $row_json = json_encode($row);
            $cache->set("item_" . $item->id, $row_json);
            //$cache->set("item_".$item->slug->slug,"item_".$item->id);
            //echo "Ключ: item_{$item->id} установлен\n";
        }
        self::setCache($this->cat_items);
        self::setCache($item_data_tag_id);
    }

    private function setCatsCache() {
        $cats=CatsModel::find()->where(['enabled'=>1])->orderBy(['ord'=>SORT_DESC])->all();
        $data=[];
        $data_parents=[];
        $data_top_level=[];
        $data_all_cat=[];
        $data_top_tags=[];
        foreach($cats as $cat) {
            $row=[];
            $row['id']=$cat->id;
            $row['title']=$cat->title;
            $row['ord']=$cat->ord;
            $row['descr']=$cat->descr;
            $row['enabled']=$cat->enabled;
            $row['type']=$cat->type;
            $row['filter_hide']=$cat->filter_hide;
            $row['title_h1']=$cat->title_h1;
            $row['meta_title']=$cat->meta_title;
            $row['meta_description']=$cat->meta_description;
            $row['meta_keywords']=$cat->meta_keywords;
            $row['parent_id']=$cat->parent_id;
            $row['slug']=$cat->slug->slug;
            $data["cat_".$cat->id]=$row;
            if($cat->catTopTags) {
                foreach($cat->catTopTags as $tags) {
                    if($tags['top_level']==1) {
                        $tag_row = [];
                        $tag_row['cat_id'] = $tags['cat_id'];
                        $tag_row['id'] = $tags['id'];
                        $tag_row['title'] = $tags['title'];
                        $tag_row['translit'] = $tags['translit'];
                        $data_top_tags["cat_top_tags_".$cat->id][] = $tag_row;
                    }
                }
            }
            $data['top_tags_'.$cat->id]="";
            $data_parents['cat_parents_'.$cat->parent_id][]=$cat->id;
            if($cat->parent_id==0) {
                $data_top_level["toplevel"][]=$row;
            }

            $data_all_cat['all_catalog_element'][$cat->id]=$row;

        }
        self::setCache($data);
        self::setCache($data_parents);
        self::setCache($data_top_level);
        self::setCache($data_all_cat);
        self::setCache($data_top_tags);
    }

    private function setSlugCache() {
        $slugs=SlugsModel::find()->all();
        $data=[];
        foreach ($slugs as $slug) {
            $row['object_id']=$slug->object_id;
            $row['subject_id']=$slug->subject_id;
            $data['slug_'.$slug->slug]=$row;
        }
        self::setCache($data);
    }

    public function catFilter() {
        $catFilters=CatsFilterModel::find()->where(['enabled'=>1])->all();
        $data=[];
        foreach($catFilters as $catFilter) {
            $row=[];
            $row['id']=$catFilter->id;
            $row['cat_id']=$catFilter->cat_id;
            $row['title']=$catFilter->title;
            $row['ord']=$catFilter->ord;
            $data['catalog_filter_data_'.$catFilter->cat_id]=$row;
        }
        self::setCache($data);
    }

    public function itemsTags() {
        $itemsTags=ItemsTagsClass::find()->all();
        $data=[];
        foreach($itemsTags as $itemTag) {
            $data["cat_tag_items_".$itemTag->tag_id][]=$itemTag->item_id;
        }
        self::setCache($data);
    }
    /* Разбить индексатор на части и кешировать частями */
    public function actionCacheFilter() {
        $items_sizes=ItemsSizesClass::find()->asArray()->all();
        $data=[];
        foreach($items_sizes as $item_size) {
            $data['ItemsSizes'][$item_size['type']][]=$item_size;
        }
        self::setCache($data);
    }
    // кеширование продукции
    public function actionCacheItems() {
        $data['Items']=ItemsModel::find()->indexBy('id')->asArray()->all();
        self::setCache($data);
    }

    public function actionCacheBottomTags() {
        $data=TagsModel::find()->indexBy('id')->where(['under_table'=>1])->all();
        $data_cache=[];
        foreach($data as $tag) {
            $row = [];
            $row['slug'] = $tag->translit;
            $row['name'] = $tag->title;
            if (isset($tag->items->item->cat->parent->id)) {
                $data_cache['bottom_tags'][$tag->items->item->cat->parent->id][] = $row;
            }
        }
        self::setCache($data_cache);
    }

    public function actionIndex() {
        $cache=Yii::$app->cache;
        //$cache->flush();
        echo "Cache Flush\n";
        $items_sizes=ItemsSizesClass::find()->all();
        $size_arr=[];
        $item_arr=[];
        // фильтры убрать искать напрямую через базу
        foreach($items_sizes as $item_size) {
            $line_param="filter_".$item_size->type;
            for($i=1;$i<=6;$i++) {
                $size="size_$i";
                if(isset($item_size->$size)) {
                    $size_arr["filter_".$item_size->type."_".$size."_".$item_size->item_id]=$item_size->$size;
                    $item_arr["filter_size_".$item_size->type."_".$size."_".$item_size->$size][]=$item_size->item_id;

                }
            }
        }
        self::setCache($size_arr);
        echo "size_arr_cache installed";
        sleep(5);
        self::setCache($item_arr);
        echo "size_arr_cache installed";
        sleep(5);
        self::setTagsCache();
        echo "tags cache installed";
        sleep(5);
        self::setItemsCache();
        echo "size_arr_cache installed";
        sleep(5);
        self::setSlugCache();
        echo "slug cache installed";
        sleep(5);
        self::setCatsCache();
        echo "cats cache installed";
        sleep(5);
        self::itemsTags();
        echo "items cache installed";
        sleep(5);
    }
}