<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\CatsModel;
use app\models\ItemsModel;
use app\models\TagsModel;

class SlugsModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'slugs';
    }

    public function getSubjectId() {
        return (int)$this->subject_id;
    }

    public function getItem() {
        return $this->hasOne(ItemsModel::className(),['id'=>'object_id']);
    }

    public function getCat() {
        return $this->hasOne(CatsModel::className(),['id'=>'object_id']);
    }

    public function getTag() {
        return $this->hasOne(TagsModel::className(),['id'=>'object_id']);
    }
}