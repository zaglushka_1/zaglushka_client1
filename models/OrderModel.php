<?php


namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use app\models\OrderItemsModel;

class OrderModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'orders';
    }

    public function getOrderItems() {
        return $this->hasMany(OrderItemsModel::className(),['order_id'=>'id']);
    }
}