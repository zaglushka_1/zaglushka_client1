<?php


namespace app\models;

use yii\db\ActiveRecord;
use app\models\ItemsTagsClass;
use app\models\SlugsModel;

class TagsModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'tags';
    }

    public function getItems() {
        return $this->hasOne(ItemsTagsClass::className(),['tag_id'=>'id']);
    }

    public function getSlug() {
        return $this->hasOne(SlugsModel::className(),['object_id'=>'id'])->andWhere(['subject_id'=>3]);
    }
}