<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\ItemsModel;
use app\models\TagsModel;

class ItemsTagsClass extends ActiveRecord
{
    public static function tableName()
    {
        return 'items_tags';
    }

    public function getItem() {
        return $this->hasOne(ItemsModel::className(),['id'=>'item_id']);
    }

    public function getTag() {
        return $this->hasOne(TagsModel::className(),['id'=>'tag_id']);
    }

}