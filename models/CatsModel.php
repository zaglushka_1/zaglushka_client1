<?php

namespace app\models;

use app\models\CatsTagsModel;
use yii\db\ActiveRecord;
use app\models\CatsFilterModel;
use app\models\HotItemsModel;
use app\models\ItemsModel;
use app\models\SlugsModel;
use app\models\CatsRemainsModel;
use app\models\ItemsQuantRemainModel;
use app\models\CatsQunatityRemainsModel;

class CatsModel extends ActiveRecord {

    public static function tableName() {
        return 'cats';
    }

    /* Необходимо протестировать данный метод */
    public function __set($name,$value) {
        return false;
    }

    public function getId() {
        return $this->id;
    }

    public function getParent() {
        return $this->hasOne(self::className(), ['id'=>'parent_id']);
    }

    public function getChild() {
        return $this->hasMany(self::className(), ['parent_id'=>'id'])->orderBy(['ord'=>SORT_DESC]);
    }

    public function getCatsFilters() {
        return $this->hasMany(CatsFilterModel::className(),['cat_id'=>'id']);
    }

    public function getHotItems() {
        return $this->hasMany(HotItemsModel::className(),['category_id'=>'id']);
    }

    public function getItems() {
        // need join
        return $this->hasMany(ItemsModel::className(),['cat_id'=>'id'])->limit(20);
    }

    public function getAddItems() {
        return $this->hasMany(ItemsModel::className(),['add_cat_id'=>'id']);
    }

    public function getSlug() {
        return $this->hasOne(SlugsModel::className(),['object_id'=>'id'])->andWhere(['subject_id'=>2]);
    }

    public function getCatTags() {
        //print_r(\app\models\CatsTagsModel::find()->all());
        //exit;
        return $this->hasMany(CatsTagsModel::className(),['cat_id'=>'id']);
    }

    public function getCatTopTags() {
        return $this->hasMany(CatsTagsModel::className(),['cat_id'=>'id'])->andWhere(["top_level"=>"1"])->groupBy('id')->orderBy(['ord'=>SORT_ASC]);
    }

    public function getCatUnderTableTags() {
        return $this->hasMany(CatsTagsModel::className(),['cat_id'=>'id'])->andWhere(["under_table"=>"1"])->groupBy('id')->orderBy(['ord'=>SORT_ASC]);

    }

    public function getRemains() {
        $remains=CatsQunatityRemainsModel::find()->where(['cat_id'=>$this->id])->orWhere(['add_cat_id'=>$this->id])->all();
        $sum=0;
        foreach($remains as $remain) {
            $sum+=(int)$remain->quantity_in_vault+(int)$remain->quantity_in_move;
        }
        return $sum;
    }
}
