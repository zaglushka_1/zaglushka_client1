<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\ItemsColorsModel;

class ItemsTransitClass extends ActiveRecord
{
    public static function tableName()
    {
        return 'items_transit';
    }

    public function getItem() {
        return $this->hasMany(ItemsColorsModel::className(),['item_id'=>'item_id'])->andWhere(['code'=>'code']);
    }
}