<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;
use yii\web\Session;
use yii\web\CookieCollection;

class CartModel extends Model {

    public function addToCart($id,$quantity) {
        $db = Yii::$app->db;
        if(!isset($_COOKIE['session_id'])) {
            $session_id=Yii::$app->session->getId();
            setcookie( "session_id", $session_id, time()+(60*60*24*30), "/",'zaglushka.ru');
        } else {
            $session_id = $_COOKIE['session_id'];
        }
        $sql="SELECT * FROM `cart` WHERE `session_id`='{$session_id}'";
        $data=$db->createCommand($sql)->queryOne();
        if(isset($data['data'])) {
            $cartData=json_decode($data['data'], true);
            $cartData[$id]=$quantity;
            $cart=json_encode($cartData);
            $sql="UPDATE `cart` SET `data`='{$cart}' WHERE `session_id`='{$session_id}'";
        } else {
            $cartData[$id]=$quantity;
            $cart=json_encode($cartData);
            $sql="INSERT INTO `cart` (`data`,`session_id`,`id_color`) VALUES ('{$cart}', '{$session_id}',0)";
        }
        $db->createCommand($sql)->execute();
        return $sql;
           
    }

    public function deleteCart($id) {
        $db = Yii::$app->db;
        $session_id=Yii::$app->session->getId();
        $sql="SELECT * FROM `cart` WHERE `session_id`='{$session_id}'";
        $data=$db->createCommand($sql)->queryOne();
        if(isset($data['data'])) {
            $cartData=json_decode($data['data'], true);
            unset($cartData[$id]);
            $cart=json_encode($cartData);
            $sql="UPDATE `cart` SET `data`='{$cart}' WHERE `session_id`='{$session_id}'";
            $db->createCommand($sql)->execute();
            return true;
        }
        return false;
    }
    
    public function getCart() {
        $cookie = Yii::$app->request->cookies;
        if(!isset($_COOKIE['session_id'])) {
            $session_id=Yii::$app->session->getId();
            setcookie( "session_id", $session_id, time()+(60*60*24*30), "/",'zaglushka.ru');
        } else {
            $session_id = $_COOKIE['session_id'];
        }
        $sql="SELECT * FROM `cart` WHERE `session_id`='{$session_id}'";
        $data=Yii::$app->db->createCommand($sql)->queryOne();
        $cart_data=NULL;
        if(isset($data['data'])) {
            $cart=json_decode($data['data'],true);
            $cart_data=[];

            foreach($cart as $color_id => $quantity) {
                if($quantity!="free") {
                    $sql = "SELECT `items`.`id` as `items_id`, `items`.`cat_id` as `cat_id`, `items`.`title` as `title`, `items`.`price0` as `price0`, `items`.`price1` as `price1`, `items`.`price2` as `price2`, `items`.`price3` as `price3`, `items`.`price4` as `price4`, `items`.`price5` as `price5`, `items`.`price6` as `price6`, `items_colors`.`code` as `items_colors_code`, `colors`.`name` as `colorname`, `colors`.`hex` as `colorhex` FROM `items`, `items_colors`, `colors` WHERE `items_colors`.`id`='{$color_id}' AND `items`.`id`=`items_colors`.`item_id` AND `colors`.`id`=`items_colors`.`color`";
                } else {
                    $sql = "SELECT `items`.`id` as `items_id`, `items`.`cat_id` as `cat_id`, `items`.`title` as `title`, `items`.`price0` as `price0`, `items`.`price1` as `price1`, `items`.`price2` as `price2`, `items`.`price3` as `price3`, `items`.`price4` as `price4`, `items`.`price5` as `price5`, `items`.`price6` as `price6`, `items_colors`.`code` as `items_colors_code`, `colors`.`name` as `colorname`, `colors`.`hex` as `colorhex` FROM `items`, `items_colors`, `colors` WHERE `items`.`id`='{$color_id}' AND `items`.`id`=`items_colors`.`item_id` AND `colors`.`id`=`items_colors`.`color`";
                }
                $data=Yii::$app->db->createCommand($sql)->queryOne();
                $row['item']=$data;
                if(!isset($row['quantity'])) {
                    $row['quantity']=1;
                } else {
                    $row['quantity'] = $quantity;
                }
                $row['item_id'] = $color_id;
                // возможные цвета
                if($quantity!=="free") {
                    $sql = "SELECT `items_colors`.*, `colors`.`name`, `colors`.`hex` FROM `items_colors`, `colors` WHERE `items_colors`.`item_id`='{$data['items_id']}' AND `colors`.`id`=`items_colors`.`color` ORDER BY `colors`.`id`";
                    $data = Yii::$app->db->createCommand($sql)->queryAll();
                    $row['item_colors'] = $data;
                }
                $cart_data[]=$row;

            }
            
        }
        return $cart_data;
    }
    
    public function payOnline() {
        
    }
    
    public function sendLetterOrder() {
        
    }
    
    public function flush() {
        
    }
    
}