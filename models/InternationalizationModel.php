<?php


namespace app\models;
use yii\db\ActiveRecord;

class InternationalizationModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'i10n';
    }
}