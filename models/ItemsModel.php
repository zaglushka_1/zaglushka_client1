<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\CatsModel;
use app\models\ItemsSameItemsModel;
use app\models\ItemsSizesClass;
use app\models\OrderItemsModel;
use app\models\PropsModel;
use app\models\SlugsModel;
use app\models\ItemsTagsClass;
use app\models\ItemsComplectItemsModel;

class ItemsModel extends ActiveRecord
{

    public static function tableName()
    {
        return 'items'; // TODO: Change the autogenerated stub
    }

    public function getCat() {
        return $this->hasOne(CatsModel::className(),['id'=>'cat_id']);
    }

    public function getSubCat() {
        return $this->hasOne(CatsModel::className(), ['id'=>'add_cat_id']);
    }

    public function getItemColors() {

        return $this->hasMany(ItemsColorsModel::className(),['item_id'=>'id'])->orderBy(['ord'=>SORT_ASC]);
    }

    public function getSameItems() {
        return $this->hasMany(ItemsSameItemsModel::className(),['same_item_id'=>'id']);
    }

    public function getItemSizes() {
        return $this->hasOne(ItemsSizesClass::className(),['item_id'=>'id']);
    }

    public function getOrderItem() {
        return $this->hasOne(OrderItemsModel::className(),['item_id'=>'id']);
    }

    public function getPropMatherial() {
        return $this->hasOne(PropsModel::className(), ['id'=>'prop_matherial']);
    }

    public function getPropPackage() {
        return $this->hasOne(PropsModel::className(),['id'=>'prop_package']);
    }

    public function getSlug() {
        return $this->hasOne(SlugsModel::className(),['object_id'=>'id'])->andWhere(['subject_id'=>4]);
    }

    public function getTags() {
        return $this->hasMany(ItemsTagsClass::className(),['item_id'=>'id']);
    }

    public function getComplectItems() {
        return $this->hasMany(ItemsComplectItemsModel::className(),['item_id'=>'id']);
    }
}