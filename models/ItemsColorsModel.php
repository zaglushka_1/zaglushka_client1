<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\models\ItemsModel;
use app\models\ColorsModel;
use app\models\ItemsTransitClass;

class ItemsColorsModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'items_colors';
    }

    public function getItem() {
        return $this->hasOne(ItemsModel::className(),['id'=>'item_id']);
    }

    public function getColors() {
        return $this->hasOne(ColorsModel::className(),['id'=>'color']);
    }

    public function getTransit() {
        return $this->hasOne(ItemsTransitClass::className(),['item_id'=>'item_id'])->andWhere(['code'=>'code']);
    }
}