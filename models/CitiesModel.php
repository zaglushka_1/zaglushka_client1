<?php


namespace app\models;
use yii\db\ActiveRecord;

class CitiesModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'cities';
    }
}