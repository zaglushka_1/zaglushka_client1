<?php


namespace app\models;

use yii\db\ActiveRecord;

class CachePagesModel extends ActiveRecord
{
    public static function tableName() {
        return 'cache_pages';
    }
}