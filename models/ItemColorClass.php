<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\Pagination;

class ItemColorClass extends Model {
    
    public function getItemColorData($id) {
        $sql="SELECT `items_colors`.`id` as `items_colors_id`,`items_colors`.`code` as `code`, `items_colors`.`add_price` as `addprice`, `items_colors`.`remains` as `remains`, `colors`.`name` as `colorname`, `colors`.`hex` as `colorhex` FROM `items_colors`, `colors` WHERE `item_id`='{$id}' AND `items_colors`.`color`=`colors`.`id` ORDER BY `items_colors`.`remains` DESC";
        $data=Yii::$app->db->createCommand($sql)->queryAll();
        return $data;
    }
    
}