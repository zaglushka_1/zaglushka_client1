<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\models\ItemsModel;

class ItemsComplectItemsModel extends ActiveRecord {

    public static function tableName() {
        return 'items_complect_items';
    }

    public function getItem() {
        return $this->hasOne(ItemsModel::className(), ['id'=>'complect_item_id']);
    }

}