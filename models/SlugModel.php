<?php
namespace app\models;

use Yii;
use yii\base\Model;

class SlugModel extends Model {
    
    public function getSlugData($slug) {
        $sql="SELECT `subject_id`, `object_id` FROM `slugs` WHERE `slug`='{$slug}'";
        $data=Yii::$app->db->createCommand($sql)->queryOne();
        return $data;
    }
    
}