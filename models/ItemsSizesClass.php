<?php


namespace app\models;

use yii\db\ActiveRecord;
use app\models\ItemsModel;

class ItemsSizesClass extends ActiveRecord
{
    public static function tableName()
    {
        return 'items_sizes';
    }

    public function getItem() {
        return $this->hasOne(ItemsModel::className(),['id'=>'item_id']);
    }
}