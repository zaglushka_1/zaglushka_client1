<?php

namespace app\models;

use Yii;
use yii\web\Controller;
use yii\base\Model;
use yii\data\Pagination;
use yii\helpers\BaseArrayHelper;
use yii\caching\Cache;
use app\models\CitiesModel;
use http\Client\Request;
use app\models\SlugsModel;
use app\models\CatsModel;
use app\models\TagsModel;
use app\models\ItemsModel;


class ClientModel extends Model {

    public function getBredcrumbs($slug,$data_output=false) {
        $request=Yii::$app->request;
        $cache=Yii::$app->cache;
            if($data_output==false) {
                $data_output = [];
            }

            if($slug->subject_id==4) {
                $row=[];
                $sql="SELECT * FROM `items` WHERE `id`='{$slug['object_id']}'";
                $data=Yii::$app->db->createCommand($sql)->queryOne();
                $row['title']=$data['title'];
                $row['url']=NULL;
                $slug['subject_id']=2;
                $slug['object_id']=$data['cat_id'];
                $data_output[]=$row;
            }
            if($slug->subject_id==2) {
                    $row=[];
                    $sql="SELECT `cats`.`title` as `title`, `cats`.`parent_id` as `parent_id`, `slugs`.`slug` as `slug`, `slugs`.`object_id` as `object_id`, `slugs`.`subject_id` as `subject_id` FROM `cats`,`slugs` WHERE `cats`.`id`='{$slug['object_id']}' AND `slugs`.`object_id`='{$slug['object_id']}' AND `slugs`.`subject_id`='2'";
                    $data=Yii::$app->db->createCommand($sql)->queryOne(); 
                    $row['title']=$data['title'];
                    $row['url']=$data['slug'];
                    $data_output[]=$row;
                    if($data['parent_id']!=0) {
                        $slug['subject_id']=2;
                        $slug['object_id']=$data['parent_id'];
                        $slug=(object)$slug;
                        $data_output=self::getBredcrumbs($slug,$data_output);
                    } else {
                        $slug=[];
                    }
            }

        return $data_output;
    }

    public function getLocation() {
        $ip = $_SERVER['REMOTE_ADDR'];
        /*if($query=unserialize(file_get_contents('http://ip-api.com/php/'.$ip.'?lang=ru'))) {
            return $query;
        }*/
        return false;
    }

    public function getTopCatalog() {
        $cache=Yii::$app->cache;
        $data=json_decode($cache->get("toplevel"), true);
        return $data;
    }
    
    public function getTowns() {
        $cache=Yii::$app->cache;
        $key='towns';
        $townInCountry=$cache->get($key);
        if($townInCountry=== false) {
            $sql="SELECT * FROM `cities` ORDER BY `country` ASC, `free_delivery_price` ASC, `id` ASC";
            $data=Yii::$app->db->createCommand($sql)->queryAll();
            $townInCountry=[];
            foreach($data as $cities) {
                $townInCountry[$cities['country']][]=$cities;
            }
        }
        return $townInCountry;
    }

    public function getTownPhone($town) {
        $current_city_data=CitiesModel::find()->where(['']);
    }
    public function getRemainsItemsCatalog($cat_id) {
        $cache=Yii::$app->cache;
        $key='remains_items_catalog_'.$cat_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT sum(`items_transit`.`quant`) as `remains` FROM `items_transit`,`items` WHERE `items`.`id`=`items_transit`.`item_id` AND (`items`.`cat_id`='{$cat_id}' OR `items`.`add_cat_id`='{$cat_id}') GROUP BY `items`.`cat_id`,`items`.`add_cat_id`";
            $data=Yii::$app->db->createCommand($sql)->queryAll();
            //$cache->set($key, $data);
        }
        return $data;            
    }
    
    public function getCountItemsCatalog($cat_id) {
        $cache=Yii::$app->cache;
        $key='count_items_catalog_'.$cat_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT sum(`items_colors`.`remains`) as `remains` FROM `items_colors`,`items` WHERE `items`.`id`=`items_colors`.`item_id` AND (`items`.`cat_id`='{$cat_id}' OR `items`.`add_cat_id`='{$cat_id}') GROUP BY `items`.`cat_id`, `items`.`add_cat_id`";
            $data=Yii::$app->db->createCommand($sql)->queryAll();
            //$cache->set($key, $data);
        }
        return $data; 
    }
    
    
    public function getAllCatalogItem($cat_id) {
        $cache=Yii::$app->cache;
        $key='all_catalog_item'.$cat_id;
        $data_output=$cache->get($key);
        if($data_output === false) {
            $sql="SELECT `cats`.`id` as `id`, `cats`.`title` as `title`, `slugs`.`slug` FROM `cats`,`slugs` WHERE `cats`.`enabled`='1'AND `cats`.`parent_id`='$cat_id' AND `cats`.`enabled`=1 AND `slugs`.`object_id`=`cats`.`id` AND `slugs`.`subject_id`='2' ORDER BY `cats`.`ord` DESC";
            $data=Yii::$app->db->createCommand($sql)->queryAll();
            $data_output=[];
            foreach($data as $element) {
                $row=[];
                $row['id']=$element['id'];
                $row['title']=$element['title'];
                $row['slug']=$element['slug'];
                $row['in_move']=self::getRemainsItemsCatalog($element['id']);
                $row['quantity']=self::getCountItemsCatalog($element['id']);
                $data_output[]=$row;
            }
            //$cache->set($key,$data_output);
        }
        return $data_output;
    }
    
    public function getAllCatalogElement() {
        $cat_top=self::getTopCatalog();
        $data=[];
        foreach($cat_top as $cat_key => $cat_value) {
            $data[$cat_value['id']]['top_element']=$cat_value;
            $data[$cat_value['id']]['sub_element']=self::getSubCatalogData($cat_value['id']);
        }
        return $data;
    }
    
    public function getCatalogContent($object_id) {
        $cache=Yii::$app->cache;
        $data=json_decode($cache->get("cat_".$object_id),true);
        return $data;
    }
    
    public function getSubCatalogData($object_id) {
        $cache=Yii::$app->cache;
        $data=json_decode($cache->get("cat_parents_".$object_id), true);
        $data_output=[];
        if($data === false or !is_array($data) or (is_array($data) and sizeof($data)==0)) {
            return [];
        } else {
            foreach ($data as $idx) {
                $row = [];
                $cat_item = json_decode($cache->get("cat_" . $idx), true);
                if ($cat_item !== false) {
                    $row['id'] = $idx;
                    $row['title'] = $cat_item['title'];
                    $row['slug'] = $cat_item['slug'];
                    $row['in_move']=json_decode($cache->get("transit_" . $idx), true);
                    $row['quantity']=json_decode($cache->get("remains_" . $idx), true);
                }
                $data_output[]=$row;
            }
        }
        return $data_output; 
    }
    
    public function getFilterSearchParams() {
        $request=Yii::$app->request;
        $filter_line="";
        if($request->get('filter_type')!==null) {
            $filter_line.="filter_".$request->get('filter_type');
        }
        for($i=1;$i<=6;$i++) {
            $size="size_".$i;
            if ($request->get($size) !== null) {
                $filter_line .= "_{$size}_" . $request->get($size);
            }
        }
        return $filter_line;
    }

    // сюда в будущем переместить выборку id отфильтрованных по условиям
    public function getFiltredItems() {

    }

    private function array_delete(array $array, array $symbols = array(''))
    {
        return array_diff($array, $symbols);
    }

    public function getCatalogItemsData($object_id,$tag=NULL,$print=false) {
        $cache=Yii::$app->cache;
        $request=Yii::$app->request;
        $filter_type=$request->get('filter_type');
        $items_sizes=json_decode($cache->get("ItemsSizes"),true);
        if(isset($filter_type)) {
            $params['type']=$filter_type;
            for($i=1;$i<=6;$i++) {
                $size="size_".$i;
                if ($request->get($size) !== null && $request->get($size) !== 'undefined' && strlen($request->get($size))!==0) {
                    $params[$size] = htmlspecialchars($request->get($size), ENT_QUOTES);
                }
            }
            if(sizeof($params)>1) {
                $filter_ids=[];
                unset($params['type']);

                foreach ($items_sizes[$filter_type] as $data_size) {
                    $item_id=$data_size['item_id'];
                    unset($data_size['item_id']);
                    unset($data_size['id']);
                    unset($data_size['type']);
                    foreach($data_size as $key => $value) {
                        if(!array_key_exists($key,$params)) {
                            unset($data_size[$key]);
                        }
                    }
                    $data_size=self::array_delete($data_size);

                    $data_size=self::array_delete($data_size);
                    $params=self::array_delete($params);
                    if(json_encode($params) == json_encode($data_size)) {
                        $filter_ids[] = $item_id;
                    }
                }
            }
        }
        if($tag) {
            $tag_id=json_decode($cache->get("tags_".$tag), true);
            $items_tags=json_decode($cache->get("cat_tag_items_".$tag_id[0]), true);
        }
        $item_cache=json_decode($cache->get("cat_items_".$object_id),true);
        if(!is_array($item_cache)) {
            return [];
        }
        if(isset($items_tags) && is_array($items_tags) and sizeof($items_tags)>0) {
            $item_cache=array_intersect_key(array_flip($item_cache),array_flip($items_tags));
            $item_cache=array_flip($item_cache);
        }
        if(isset($filter_ids) and is_array($filter_ids) and sizeof($filter_ids)>0) {
            $item_cache=array_intersect_key(array_flip($item_cache),array_flip($filter_ids));
            $item_cache=array_flip($item_cache);
        }
        $pages=new Pagination(['totalCount' => sizeof($item_cache), 'pageSize' => 20, 'forcePageParam' => false, 'pageSizeParam' => false]);
        if($print===false) {
            $data = array_slice($item_cache, $pages->offset, $pages->limit);
        } else {
            $data=$item_cache;
        }
        $data_output = [];
        foreach($data as $item) {
            $data_output[$item]=json_decode($cache->get('item_'.$item), true);
        }
        $data_output['pages']=$pages;

        if(isset($filter_result)) {
            $data_output['filter_no_result']=$filter_result;
        }
        return $data_output;
    }

    public function getCompareItems()  {
        $session = Yii::$app->session->getId();
        $key = "compare_" . $session;
        $cache = Yii::$app->cache;
        $data = $cache->get($key);

        if(is_array($data)) {
            $items_ids=[];
            foreach($data as $k=>$v) {
                $items_ids[]=$k;
            }
            $items=ItemsModel::find()->where(['in', 'id', $items_ids])->all();
            $data_output=[];
            foreach($items as $element) {
                $row=[];
                $row_colors=[];
                $row['id']=$element['id'];
                $row['title']=$element['title'];
                $row['slug']=$element->slug->slug;
                $row['price5']=$element->price5;
                $row['price4']=$element->price4;
                $row['price3']=$element->price3;
                $row['price2']=$element->price2;
                $row['price1']=$element->price1;
                $row['price0']=$element->price0;
                foreach($element->itemColors as $color) {
                    $row_color=[];
                    $row_color['color_hex']=$color->colors->hex;
                    $row_color['color_name']=$color->colors->name;
                    $row_color['remains'] = $color->remains;
                    if(isset($color->transit->quant)) {
                        $row_color['transit'] = $color->transit->quant;
                    }
                    $row_colors[]=$row_color;
                }
                $row['colors']=$row_colors;
                $row['transit']=self::getRemainsItemsCatalog($element['id']);
                $row['quantity']=self::getCountItemsCatalog($element['id']);
                $data_output[]=$row;
            }
            return $data_output;
        } else {
            return [];
        }
    }

    public function getItem($object_id) {
        $sql="SELECT * FROM `items` WHERE `id`='{$object_id}'";
        $data=Yii::$app->db->createCommand($sql)->queryOne();
        $sql="SELECT `items_colors`.*, `colors`.`name` as `color_name`, `colors`.`hex` as `color_hex` FROM `items_colors`, `colors` WHERE `item_id`='{$object_id}' AND `items_colors`.`color`=`colors`.`id` ORDER BY `color` ASC, `remains` DESC";
        $data_color=Yii::$app->db->createCommand($sql)->queryAll();
        foreach($data_color as $color) {
            $data['colors'][]=$color;
        }
        return $data;
    }
    
    public function getItemSize($object_id) {
        $cache=Yii::$app->cache;
        $key="item_size_".$object_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT * FROM `items_sizes` WHERE `id`='{$object_id}'";
            $data=Yii::$app->db->createCommand($sql)->queryOne();
            //$cache->set($key,$data);
        }
        return $data;
    }
    
    public function getTopTags($object_id) {
        $cache=Yii::$app->cache;
        $data=json_decode($cache->get("cat_top_tags_".$object_id), true);
        if($data===false) {
            return [];
        }
        return $data;
    }
    
    public function getFiltersData($object_id) {
        $cache=Yii::$app->cache;
        $key="filters_data_".$object_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT `items_sizes`.* FROM `items_sizes`, `items` WHERE `items`.`cat_id`='{$object_id}' AND `items`.`id`=`items_sizes`.`item_id`";
            $data=Yii::$app->db->createCommand($sql)->queryAll();
            //$cache->set($key,$data);
        }
        return $data;
    }
    
    public function getCatalogFilterData($object_id) {
        $cache=Yii::$app->cache;
        $data=json_decode($cache->get("catalog_filter_data_".$object_id), true);
        if($data === false) {
            return [];
        }
        return $data;
    }
    
    public function getTopTagsByTagId($object_id) {

        $cache=Yii::$app->cache;
        $key="top_rags_by_tag_id_".$object_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT `tags`.*, `slugs`.`slug` as `slug` FROM `tags`, `items`, `items_tags`, `slugs` WHERE `tags`.`top_level`='1' AND `tags`.`id`=`items_tags`.`tag_id` AND `items`.`id`=`items_tags`.`item_id` AND `items`.`cat_id` IN (SELECT `items`.`cat_id` FROM `items`, `items_tags` WHERE `items`.`id`=`items_tags`.`item_id` AND `items_tags`.`tag_id`='{$object_id}' GROUP BY `cat_id`) AND `slugs`.`subject_id`='3' AND `slugs`.`object_id`=`tags`.`id` GROUP BY `tags`.`id` ORDER BY `tags`.`ord` ASC";
            $data=Yii::$app->db->createCommand($sql)->queryAll();
            //$cache->set($key,$data);
        }
        return $data;
    }

    public function getBottomTagsByTagId($object_id) {
        $tags=[];
        $cache=Yii::$app->cache;
        $key="bottom_tags";
        $data=$cache->get($key);
        if($data !== false) {
            $data=json_decode($data);
            foreach($data as $catalog_id => $tag_data) {
                if($catalog_id==$object_id) {
                    $tags[]=$tag_data;
                }
            }
        }
        return $tags;
    }

    public function getPropMatherialByItemId($object_id) {
        $cache=Yii::$app->cache;
        $key="prop_matherial_by_item_id_".$object_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT `props`.`title` FROM `props`, `items` WHERE `props`.`id`=`items`.`prop_matherial` AND `items`.`id`='{$object_id}' AND `props`.`type`='1'";
            $data=Yii::$app->db->createCommand($sql)->queryOne();
            //$cache->set($key,$data);
        }
        return $data;
    }


    public function getPropFitPipeByItemId($object_id) {
        $cache=Yii::$app->cache;
        $key="prop_fit_pipe_by_item_id_".$object_id;
        $data=$cache->get($key);
        if($data === false) {
            $sql="SELECT `props`.`title` FROM `props`, `items` WHERE `props`.`id`=`items`.`prop_fit_pipe` AND `items`.`id`='{$object_id}' AND `props`.`type`='3'";
            $data=Yii::$app->db->createCommand($sql)->queryOne();
            //$cache->set($key,$data);
        }
        return $data['title'];
    }

    public function getTitlePage($object_id,$tag=NULL) {
        $title="";
        $slug=SlugsModel::find()->where(['subject_id'=>2,'object_id'=>$object_id])->one();
        if($slug->cat->parent_id!=0) {
            $title.=$slug->cat->parent->title." ";
        }
        $title.=$slug->cat->title." ";
        $tags=TagsModel::find()->where(['translit'=>$tag])->one();
        if(isset($tags->title)) {
            $title .= $tags->title." ";
        }
        $request=Yii::$app->request;
        $filter_type=$request->get('filter_type');
        if($filter_type!==NULL) {
            if($filter_type==1) {
                $size_1=$request->get('size_1');
                $title.= $size_1." мм";
            }
        }
        return $title;

    }

    public function getItemObject($item_id) {
        $item=ItemsModel::find()->where(['id'=>$item_id])->one();
        return $item;
    }


    
    public function getDataBySlug($slug,$tag=NULL,$print=false) {
        // вытащить из кеша
        $subject_id=$slug->subject_id;
        $object_id=$slug->object_id;
        $row['breadcrumbs']=self::getBredcrumbs($slug); // тоже необходимо оптимизировать
        if($subject_id==2) {
            $row['catalog']=self::getSubCatalogData($object_id); // оптимизировано - данные из памяти
            $row['catalog_items']=self::getCatalogItemsData($object_id,$tag,$print); // наполовину оптимизировано - нет функционала работы тегов, нет функционала работы фильтров
            $row['catalog_filter']=self::getCatalogFilterData($object_id); // оптимизировано - данные из памяти
            $row['catalog_content']=self::getCatalogContent($object_id); // оптимизировано - данные из памяти
            $row['top_tags']=self::getTopTags($object_id); // оптимизировано - данные из памяти
            $row['bottom_tags']=self::getBottomTagsByTagId($object_id); // оптимизировано - данные из памяти
            $row['filter']=self::getFiltersData($object_id);
            $row['title']=self::getTitlePage($object_id,$tag); // не оптимизировать
            $data=$row;
        }
        if($subject_id==4) {
            $row['item']=self::getItem($object_id); 
            $row['item_sizes']=self::getItemSize($object_id);
            $row['matherial']=self::getPropMatherialByItemId($object_id);
            $row['prop_fit_pipe']=self::getPropFitPipeByItemId($object_id);
            $row['itemObject']=self::getItemObject($object_id);
            $data=$row;
        }
        return $data;
    }
    
}

