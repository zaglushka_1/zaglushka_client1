<?php


namespace app\models;

use yii\db\ActiveRecord;
use app\models\ItemsModel;


class ItemsSameItemsModel extends ActiveRecord
{
    public static function tableName()
    {
        return 'items_same_items';
    }

    public function getItem() {
        return $this->hasOne(ItemsModel::className(),['id'=>'item_id']);
    }
}