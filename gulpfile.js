var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var imagemin = require('gulp-imagemin');
var plumber = require('gulp-plumber');

gulp.task('default', function () {
    gulp.src('web/css/*.css')
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('web/css/min'));
});
gulp.task('compress', function() {
    gulp.src('web/images/**/*')
        .pipe(plumber())
        .pipe(imagemin({
                progressive: true
        }))
        .pipe(gulp.dest('web/images_min/'))
});