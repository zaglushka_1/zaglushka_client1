<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'MUNA8vbWNBTqTzOwhwIWPijqnkzR6MeP',
            'baseUrl'=>'',
        ],
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname'  => 'localhost',
                'port'      => 6379,
                'database'  => 0,
            ]
            #'cachePath' => '/home/zagweb/new_client/runtime/cache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp-pulse.com',
                'username' => 's.seredenin@zaglushka.ru',
                'password' => '4bnEQWXR96',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],
        'session' => [
            'class' => 'yii\redis\Session',
            'timeout'=>10*365*24*60*60,
            'redis'  => [
                'hostname'  => 'localhost',
                'port'      => 6379,
                'database'  => 0,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
               
                '/'                 => 'site/index',
                '/about'            => 'site/about',
                '/delivery'         => 'site/delivery',
                '/contacts'         => 'site/contacts',
                '/pay'              => 'site/pay',
                '/booklet'          => 'site/booklet',
                '/search'           => 'site/search',
                '/cart'             => 'site/cart',
                '/compare'          => 'site/compare',
                '/cart/add'         => 'site/cart?action=add',
                '/cart/pay-online'  => 'site/cart?action=pay-online',
                '/login/'           => 'site/login',
                '/logout/'          => 'site/logout',
                [
                    'pattern'       =>  '/change_city/<city:[\w-]+>',
                    'route'         =>  'site/city',
                ],
                [
                    'pattern' => '/<slug:[\w-]+>',
                    'route' => 'site/index',
                ],
                [
                    'pattern' => '/code/<code:[\w-]+>',
                    'route' => 'site/code',
                ],
                [
                    'pattern' => '/<slug:[\w-]+>/print',
                    'route' => 'site/index?print=1',
                ],
                [  
                    'pattern' => '/<slug:[\w-]+>/<tag:[\w-]+>',
                    'route' => 'site/index',
                ]
            ],
        ],
        // минификация
        /*'view' => [
            'class' => '\rmrevin\yii\minify\View',
            'enableMinify' => !YII_DEBUG,
            'concatCss' => true, // concatenate css
            'minifyCss' => true, // minificate css
            'concatJs' => true, // concatenate js
            'minifyJs' => true, // minificate js
            'minifyOutput' => true, // minificate result html page
            'webPath' => '@web', // path alias to web base
            'basePath' => '@webroot', // path alias to web base
            'minifyPath' => '@webroot/minify', // path alias to save minify result
            'jsPosition' => [ \yii\web\View::POS_END ], // positions of js files to be minified
            'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
            'expandImports' => true, // whether to change @import on content
            'compressOptions' => ['extra' => true], // options for compress
            'excludeFiles' => [
                //'jquery.js', // exclude this file from minification
                //'app-[^.].js', // you may use regexp
            ],
            'excludeBundles' => [
                \app\helloworld\AssetBundle::class, // exclude this bundle from minification
            ],
        ],*/
        
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
