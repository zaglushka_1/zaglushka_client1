<?php
namespace Yii\helpers;

use Yii;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_RichText;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Color;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_PageSetup;
use app\models\ItemsModel;

class ExcelHelpers
{

    public function ExcelCatalog($data) {
        require("../vendor/phpoffice/phpexcel/Classes/PHPExcel.php");
        $Managers=[];
        $Managers[]=1;
        $cExcel   = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cWorksheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $cWorksheet->setShowGridlines(true);

        $height = 50 + (count($Managers) * 30);

        $cWorksheet->getRowDimension(1)->setRowHeight($height >= 172 ? $height : 172);

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath('../web/images/excel/logo.png');
        $cDrawing->setWidthAndHeight(200, 200);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(10);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('A1');

        $cWorksheet->mergeCellsByColumnAndRow(3, 1, 6, 1);
        $cWorksheet->setCellValueByColumnAndRow(
            3,
            1,
            "г. Санкт-Петербург, п. Парголово,\r\n" .
            "ул. Подгорная, д. 6. (въезд с Железнодорожной, 11) АНГАР №12\r\n" .
            "график работы: Пн-Пт, 9.00-18.00\r\n\r\n" .
            "8 (812) 242 80 99 (доб. 160)\r\n" .
            "8 (812) 640 91 99 (факс)\r\n" .
            "info@zaglushka.ru (секретарь)\r\n\r\n" .
            (
            count($Managers) === 1 ?
                1 . "   (доб. " . 1 . ")\r\n" .
                1 . "\r\n"
                :
                ("руководитель отдела продаж\r\n" .
                    "Тимофеев Денис Вячеславович (доб. 113)\r\n" .
                    "d.timofeev@zaglushka.ru")
            )
        );
        $cWorksheet->getStyle('D1:G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $cWorksheet->mergeCellsByColumnAndRow(7, 1, 11, 1);
        $sLabel = "8 (812) 242 80 99 (многоканальный)\r\n" .
            "8 (800) 555 04 99 (бесплатный звонок по России)\r\n\r\n";
        /*
                if (count($Managers) !== 1) {
                    foreach ($Managers as $Manager) {
                        $sLabel .= $Manager->title . "   (доб. " . $Manager->phone . ")\r\n" . $Manager->email . "\r\n";
                    }
                }
        */
        $cWorksheet->setCellValueByColumnAndRow(7, 1, $sLabel);
        $cWorksheet->getStyle('H1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);
        $r=2;
        if($data) {
            $cWorksheet->mergeCellsByColumnAndRow(4, $r, 9, $r);
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 0, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(1, $r, 1, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(2, $r, 2, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(3, $r, 3, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(10, $r, 10, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(11, $r, 11, $r + 1);

            $cWorksheet->getStyle('A' . $r . ':L' . ($r + 1))->getFont()->setBold(true);
            $cWorksheet->getStyle('A' . $r . ':L' . ($r + 1))->getFill()->applyFromArray([
                'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => ['rgb' => 'CECECE'],
            ]);

            $aHeaders = [
                0  => ['title' => '№', 'width' => 4],
                1  => ['title' => 'Артикул', 'width' => 20],
                4  => ['title' => 'Цена, руб. за 1 шт.'],
                10 => ['title' => 'Упаковка', 'width' => 10],
                11 => ['title' => 'Наличие на складе ', 'width' => 11],
            ];

            $aHeaders[2] = ['title' => 'Схема', 'width' => 13];
            $aHeaders[3] = ['title' => 'Фото', 'width' => 13];

            $cWorksheet->getRowDimension($r)->setRowHeight(20);
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
                if (isset($aItem['width'])) {
                    $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
                }
            }
            $r++;

            $aHeaders = [
                4 => ['title' => 'более 10000', 'width' => 9],
                5 => ['title' => 'от 5000 до 10000', 'width' => 9],
                6 => ['title' => 'от 1000 до 5000', 'width' => 9],
                7 => ['title' => 'от 500 до 1000', 'width' => 8],
                8 => ['title' => 'от 100 до 500', 'width' => 8],
                9 => ['title' => 'розница до 100', 'width' => 9],
            ];

            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
                if (isset($aItem['width'])) {
                    $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
                }
            }
            $r++;
            $k=1;
            foreach($data['catalog_items'] as $key => $item) {
                if ($key != 'pages' && $key != 'filter_no_result') {
                    $remains=0;
                    if ($item['colors']) {
                        foreach($item['colors'] as $color) {
                            $remains=$remains+(int)$color['remains'];
                        }
                    }

                    $cWorksheet->getRowDimension($r)->setRowHeight(75);
                    $cWorksheet->setCellValueByColumnAndRow(0, $r, $k);
                    $cWorksheet->setCellValueByColumnAndRow(1, $r, $item['title']);
                    $k++;
                    $img="/images/item/".substr($key, -2)."/".$key."/18.jpg";
                    if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                        $cDrawing = new PHPExcel_Worksheet_Drawing();
                        $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                        $cDrawing->setWidthAndHeight(80, 80);
                        $cDrawing->setOffsetX(5);
                        $cDrawing->setOffsetY(5);
                        $cDrawing->setWorksheet($cWorksheet);
                        $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(2) . $r);

                    }
                    $img="/images/item/".substr($key, -2)."/".$key."/5.jpg";
                    if(file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                        $cDrawing = new PHPExcel_Worksheet_Drawing();
                        $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                        $cDrawing->setWidthAndHeight(80, 80);
                        $cDrawing->setOffsetX(5);
                        $cDrawing->setOffsetY(5);
                        $cDrawing->setWorksheet($cWorksheet);
                        $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(3) . $r);

                    }
                    if (!empty($item['price5'])) {
                        $objRichText = new PHPExcel_RichText();
                        $objRichText->createTextRun($item['price5'], true);
                        $cWorksheet->setCellValueByColumnAndRow(4, $r, $objRichText);
                    }
                    if (!empty($item['price4'])) {
                        $objRichText = new PHPExcel_RichText();
                        $objRichText->createTextRun($item['price4'], true);
                        $cWorksheet->setCellValueByColumnAndRow(5, $r, $objRichText);
                    }
                    if (!empty($item['price3'])) {
                        $objRichText = new PHPExcel_RichText();
                        $objRichText->createTextRun($item['price3'], true);
                        $cWorksheet->setCellValueByColumnAndRow(6, $r, $objRichText);
                    }
                    if (!empty($item['price2'])) {
                        $objRichText = new PHPExcel_RichText();
                        $objRichText->createTextRun($item['price2'], true);
                        $cWorksheet->setCellValueByColumnAndRow(7, $r, $objRichText);
                    }
                    if (!empty($item['price1'])) {
                        $objRichText = new PHPExcel_RichText();
                        $objRichText->createTextRun($item['price1'], true);
                        $cWorksheet->setCellValueByColumnAndRow(8, $r, $objRichText);
                    }
                    if (!empty($item['price0'])) {
                        $objRichText = new PHPExcel_RichText();
                        $objRichText->createTextRun($item['price0'], true);
                        $cWorksheet->setCellValueByColumnAndRow(9, $r, $objRichText);
                    }
                    $cWorksheet->setCellValueByColumnAndRow(10, $r, $item['pack_size']);
                    $cWorksheet->setCellValueByColumnAndRow(11, $r, ($remains>0) ? $remains : '―');
                    $r++;
                }
            }
            $cWorksheet->getStyle('A2:L' . ($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);

        }
        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru_list.xlsx');
        $cWriter->save('php://output');

        die();
    }

    public function CompareExcel($data) {
        if (!isset($Managers) || count($Managers) === 0) {
            $Managers = [];
        }

        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $cWorksheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
        $cWorksheet->setShowGridlines(true);

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath('../web/images/excel/logo.png');
        $cDrawing->setWidthAndHeight(260, 260);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(15);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('A1');

        if (count($Managers) === 1) {
            $cWorksheet->mergeCellsByColumnAndRow(3, 1, 6, 1);
            $cWorksheet->setCellValueByColumnAndRow(3, 1, "АДРЕС СКЛАДА:");
            $cWorksheet->mergeCellsByColumnAndRow(3, 2, 6, 2);
            $cWorksheet->setCellValueByColumnAndRow(3, 2, "г. Санкт-Петербург, п. Парголово");
            $cWorksheet->mergeCellsByColumnAndRow(3, 3, 6, 3);
            $cWorksheet->setCellValueByColumnAndRow(3, 3, "ул. Подгорная, дом 6.");
            $cWorksheet->mergeCellsByColumnAndRow(3, 4, 6, 4);
            $cWorksheet->setCellValueByColumnAndRow(3, 4, "Въезд с ул. Железнодорожная, дом 11");
            $cWorksheet->mergeCellsByColumnAndRow(3, 5, 6, 5);
            $cWorksheet->setCellValueByColumnAndRow(3, 5, "На территории АНГАР № 12");
            $cWorksheet->mergeCellsByColumnAndRow(3, 6, 6, 6);
            $cWorksheet->setCellValueByColumnAndRow(3, 6, "График работы: Пн-Пт, 9:00 - 18:00");

            $cWorksheet->mergeCellsByColumnAndRow(7, 1, 10, 1);
            $cWorksheet->setCellValueByColumnAndRow(7, 1, "ОТДЕЛ ПРОДАЖ");
            $cWorksheet->mergeCellsByColumnAndRow(7, 2, 10, 2);
            $cWorksheet->setCellValueByColumnAndRow(7, 2, "тел. 7 (812) 242-80-99");
            $cWorksheet->mergeCellsByColumnAndRow(7, 3, 10, 3);
            $cWorksheet->setCellValueByColumnAndRow(7, 3, "тел. 7 (800) 555-04-99");

            $cWorksheet->mergeCellsByColumnAndRow(7, 6, 10, 6);
            $cWorksheet->setCellValueByColumnAndRow(7, 6, "МЕНЕДЖЕР ПО РАБОТЕ С КЛИЕНТАМИ:");

            $cWorksheet->mergeCellsByColumnAndRow(7, 7, 8, 7);
            $cWorksheet->setCellValueByColumnAndRow(7, 7, "доб. " . $Managers[0]->phone);
            $cWorksheet->mergeCellsByColumnAndRow(9, 7, 10, 7);
            $cWorksheet->setCellValueByColumnAndRow(9, 7, $Managers[0]->title);

            $cWorksheet->mergeCellsByColumnAndRow(7, 8, 8, 8);
            $cWorksheet->setCellValueByColumnAndRow(7, 8, "e-mail");
            $cWorksheet->mergeCellsByColumnAndRow(9, 8, 10, 8);
            $cWorksheet->setCellValueByColumnAndRow(9, 8, $Managers[0]->email);

            $cWorksheet->mergeCellsByColumnAndRow(0, 14, 10, 14);
            $cWorksheet->setCellValueByColumnAndRow(0, 14, "Коммерческое предложение");
            $cWorksheet->getRowDimension(14)->setRowHeight(25);

            $cWorksheet->getStyle('H1:H6')->getFont()->setBold(true);
            $cWorksheet->getStyle('D1')->getFont()->setBold(true);
            $cWorksheet->getStyle('A14')->getFont()->setBold(true)->setSize(16);

            $r = 16;
        } else {
            $cWorksheet->mergeCellsByColumnAndRow(3, 1, 6, 1);
            $cWorksheet->setCellValueByColumnAndRow(3, 1, "ОТДЕЛ ПРОДАЖ");
            $cWorksheet->mergeCellsByColumnAndRow(3, 2, 6, 2);
            $cWorksheet->setCellValueByColumnAndRow(3, 2, "тел. 7 (812) 242-80-99");
            $cWorksheet->mergeCellsByColumnAndRow(3, 3, 6, 3);
            $cWorksheet->setCellValueByColumnAndRow(3, 3, "тел. 7 (800) 555-04-99");
            $cWorksheet->mergeCellsByColumnAndRow(3, 4, 6, 4);
            $cWorksheet->mergeCellsByColumnAndRow(3, 5, 6, 5);

            $cWorksheet->mergeCellsByColumnAndRow(3, 6, 6, 6);
            $cWorksheet->setCellValueByColumnAndRow(3, 6, "АДРЕС СКЛАДА:");
            $cWorksheet->mergeCellsByColumnAndRow(3, 7, 6, 7);
            $cWorksheet->setCellValueByColumnAndRow(3, 7, "г. Санкт-Петербург, п. Парголово");
            $cWorksheet->mergeCellsByColumnAndRow(3, 8, 6, 8);
            $cWorksheet->setCellValueByColumnAndRow(3, 8, "ул. Подгорная, дом 6.");
            $cWorksheet->mergeCellsByColumnAndRow(3, 9, 6, 9);
            $cWorksheet->setCellValueByColumnAndRow(3, 9, "Въезд с ул. Железнодорожная, дом 11");
            $cWorksheet->mergeCellsByColumnAndRow(3, 10, 6, 10);
            $cWorksheet->setCellValueByColumnAndRow(3, 10, "На территории АНГАР № 12");
            $cWorksheet->mergeCellsByColumnAndRow(3, 11, 6, 11);
            $cWorksheet->setCellValueByColumnAndRow(3, 11, "График работы: Пн-Пт, 9:00 - 18:00");
            $cWorksheet->mergeCellsByColumnAndRow(3, 12, 6, 12);
            $cWorksheet->mergeCellsByColumnAndRow(3, 13, 6, 13);

            $cWorksheet->getStyle('D1:D6')->getFont()->setBold(true);
            $cWorksheet->getStyle('H1')->getFont()->setBold(true);

            $cWorksheet->mergeCellsByColumnAndRow(7, 1, 10, 1);

            $cWorksheet->setCellValueByColumnAndRow(7, 1, "МЕНЕДЖЕРЫ ПО РАБОТЕ С КЛИЕНТАМИ:");
            $Managers=[];
            for($i=0;$i<4;$i++) {
                $Managers[$i]['phone']="доб. 127";
                $Managers[$i]['title']="Галина Крайнова";
                $Managers[$i]['email']="gk@zaglushka.ru";
            }
            foreach ($Managers as $iNum => $Manager) {
                $r = ($iNum + 1) * 2;
                $cWorksheet->mergeCellsByColumnAndRow(7, $r, 8, $r);
                $cWorksheet->setCellValueByColumnAndRow(7, $r, 'доб. '.$Manager['phone']);
                $cWorksheet->mergeCellsByColumnAndRow(9, $r, 10, $r);
                $cWorksheet->setCellValueByColumnAndRow(9, $r, $Manager['title']);
                $cWorksheet->mergeCellsByColumnAndRow(7, $r + 1, 8, $r + 1);
                $cWorksheet->setCellValueByColumnAndRow(7, $r + 1, 'e-mail');
                $cWorksheet->mergeCellsByColumnAndRow(9, $r + 1, 10, $r + 1);
                $cWorksheet->setCellValueByColumnAndRow(9, $r + 1, $Manager['email']);
                $cWorksheet->getStyle('H'.$r.':K'.$r)->getBorders()->applyFromArray([
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ]);
                $cWorksheet->getStyle('H'.$r.':K'.($r + 1))->getBorders()->applyFromArray([
                    'left' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'right' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'top' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                    'bottom' => [
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    ],
                ]);
            }

            for ($r = 1; $r <= count($Managers) * 2 + 1; $r++) {
                $cWorksheet->getRowDimension($r)->setRowHeight(17);
            }
        }

        foreach ($data['catalog_items'] as $key => $Item) {
            $item=ItemsModel::find()->where(['id'=>$Item['id']])->one();
            $bWithImages = false;
            $cWorksheet->getRowDimension($r)->setRowHeight(130);
            if(substr($Item['id'], -2)=="00") {
                $imagePath="/images/item/0/".$Item['id']."/";
            } else {
                $imagePath="/images/item/".substr($Item['id'], -2)."/".$Item['id']."/";
            }
            $img="{$imagePath}1.jpg";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('A'.$r);
                $bWithImages = true;
            }
            $img="{$imagePath}2.jpg";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(0);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('C'.$r);
                $bWithImages = true;
            }
            $img="{$imagePath}3.jpg";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(60);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('D'.$r);
                $bWithImages = true;
            }
            $img="{$imagePath}4.jpg";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(35);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('G'.$r);
                $bWithImages = true;
            }
            $img="{$imagePath}9.jpg";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img);
                $cDrawing->setWidthAndHeight(160, 160);
                $cDrawing->setOffsetX(10);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates('J'.$r);
                $bWithImages = true;
            }
            $r++;

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 10, $r);
            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, html_entity_decode($item->short_descr, ENT_COMPAT, 'UTF-8'));
            $r++;

            $iFirstRow = $r;

            $cWorksheet->mergeCellsByColumnAndRow(3, $r, 8, $r);
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 0, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(1, $r, 1, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(2, $r, 2, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(9, $r, 9, $r + 1);
            $cWorksheet->mergeCellsByColumnAndRow(10, $r, 10, $r + 1);

            $aHeaders = [
                0 => ['title' => '№', 'width' => 4],
                1 => ['title' => 'Артикул', 'width' => 20],
                2 => ['title' => 'Цвет', 'width' => 15],
                3 => ['title' => 'Цена, руб. за 1 шт.'],
                9 => ['title' => 'Упаковка', 'width' => 12],
                10 => ['title' => 'Наличие на складе '.date('d.m.Y', mktime()), 'width' => 13],
            ];
            $cWorksheet->getRowDimension($r)->setRowHeight(20);
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
                if (isset($aItem['width'])) {
                    $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
                }
            }

            $aHeaders = [
                3 => ['title' => 'более 10000', 'width' => 9],
                4 => ['title' => 'от 5000 до 10000', 'width' => 9],
                5 => ['title' => 'от 1000 до 5000', 'width' => 9],
                6 => ['title' => 'от 500 до 1000', 'width' => 9],
                7 => ['title' => 'от 100 до 500', 'width' => 9],
                8 => ['title' => 'розница до 100', 'width' => 9],
            ];

            $r++;
            $cWorksheet->getRowDimension($r)->setRowHeight(30);
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
                if ($aItem['width']) {
                    $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
                }
            }

            $r++;
            $k = 1;
            foreach ($item->itemColors as $Color) {
                $cWorksheet->getRowDimension($r)->setRowHeight(-1);
                $cWorksheet->setCellValueByColumnAndRow(0, $r, $k++);
                $cWorksheet->setCellValueByColumnAndRow(1, $r, $Color->code);
                $cWorksheet->setCellValueByColumnAndRow(2, $r, $Color->colors->name);
                if ($item->price5) {
                    $objRichText = new PHPExcel_RichText();
                    $objRichText->createTextRun($item->price5, true);
                    $cWorksheet->setCellValueByColumnAndRow(3, $r, $objRichText);
                }
                if ($item->price4) {
                    $objRichText = new PHPExcel_RichText();
                    $objRichText->createTextRun($item->price4, true);
                    $cWorksheet->setCellValueByColumnAndRow(4, $r, $objRichText);
                }
                if ($item->price3) {
                    $objRichText = new PHPExcel_RichText();
                    $objRichText->createTextRun($item->price3, true);
                    $cWorksheet->setCellValueByColumnAndRow(5, $r, $objRichText);
                }
                if ($item->price2) {
                    $objRichText = new PHPExcel_RichText();
                    $objRichText->createTextRun($item->price2, true);
                    $cWorksheet->setCellValueByColumnAndRow(6, $r, $objRichText);
                }
                if ($item->price1) {
                    $objRichText = new PHPExcel_RichText();
                    $objRichText->createTextRun($item->price1, true);
                    $cWorksheet->setCellValueByColumnAndRow(7, $r, $objRichText);
                }
                if ($item->price0) {
                    $objRichText = new PHPExcel_RichText();
                    $objRichText->createTextRun($item->price0, true);
                    $cWorksheet->setCellValueByColumnAndRow(8, $r, $objRichText);
                }
                $cWorksheet->setCellValueByColumnAndRow(9, $r, $item->pack_size ? $item->pack_size.' шт.' : '');
                $cWorksheet->setCellValueByColumnAndRow(10, $r, $Color->remains ? number_format($Color->remains, 0, ',', ' ') : '―');
                $r++;
            }

            $cWorksheet->getStyle('A'.$iFirstRow.':K'.($iFirstRow + 1))->getFont()->setBold(true);
            $cWorksheet->getStyle('A'.$iFirstRow.':K'.($iFirstRow + 1))->getFill()->applyFromArray([
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => ['rgb' => 'CECECE'],
            ]);
            $cWorksheet->getStyle('A'.$iFirstRow.':K'.($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);

            $cWorksheet->getStyle('A'.($iFirstRow - ($bWithImages?2:1)).':K'.($r - 1))->getBorders()->applyFromArray([
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'top' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
                'bottom' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                ],
            ]);

            $r++;
            $r++;

        }

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru_Sravnenie.xlsx');
        $cWriter->save('php://output');
        die();

    }

    public function CartExcel($data) {
        $cExcel = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();

        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $sOrderNumDate = '';

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath('../web/images/excel/logo.png');
        $cDrawing->setWidthAndHeight(200, 200);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(10);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('B1');
        $Managers=[1,2,3,4];
        $height = 50 + (count($Managers) * 30);

        $cWorksheet->getRowDimension(1)->setRowHeight($height >= 172 ? $height : 172);

        $cWorksheet->mergeCellsByColumnAndRow(3, 1, 4, 1);
        $cWorksheet->setCellValueByColumnAndRow(
            3,
            1,
            "г. Санкт-Петербург, п. Парголово,\r\n" .
            "ул. Подгорная, д. 6. (въезд с Железнодорожной, 11) АНГАР №12\r\n" .
            "график работы: Пн-Пт, 9.00-18.00\r\n\r\n" .
            "8 (812) 242 80 99 (доб. 160)\r\n" .
            "8 (812) 640 91 99 (факс)\r\n" .
            "info@zaglushka.ru (секретарь)\r\n\r\n" .
            (
            count($Managers) === 1 ?
                $Managers[0]->title . "   (доб. " . $Managers[0]->phone . ")\r\n" .
                $Managers[0]->email . "\r\n"
                :
                ("руководитель отдела продаж\r\n" .
                    "Тимофеев Денис Вячеславович (доб. 113)\r\n" .
                    "d.timofeev@zaglushka.ru")
            )
        );
        $cWorksheet->getStyle('D1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $cWorksheet->mergeCellsByColumnAndRow(5, 1, 8, 1);
        $sLabel = "8 (812) 242 80 99 (многоканальный)\r\n" .
            "8 (800) 555 04 99 (бесплатный звонок по России)\r\n\r\n";
        $cWorksheet->setCellValueByColumnAndRow(5, 1, $sLabel);
        $cWorksheet->getStyle('F1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $r = 2;
        $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
        $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Заказ клиента' . $sOrderNumDate);
        $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(16);
        $cWorksheet->getRowDimension($r)->setRowHeight(25);
        $r++;
        $iStart = $r;

        $aHeaders = [
            0 => ['title' => '№', 'width' => 4],
            1 => ['title' => 'Фото', 'width' => 13],
            2 => ['title' => 'Артикул', 'width' => 22],
            3 => ['title' => 'Описание', 'width' => 25],
            4 => ['title' => 'Цвет', 'width' => 15],
            5 => ['title' => 'Кол-во штук', 'width' => 10],
            6 => ['title' => 'Оптовая группа', 'width' => 12],
            7 => ['title' => 'Цена за 1 штуку', 'width' => 9],
            8 => ['title' => 'Сумма', 'width' => 11],
        ];
        foreach ($aHeaders as $iCell => $aItem) {
            if ($aItem['width']) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }

        $totalPrice = 0;
        if (!empty($data)) {
            foreach ($aHeaders as $iCell => $aItem) {
                $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
            }
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFill()->applyFromArray([
                'type'       => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => ['rgb' => 'CECECE'],
            ]);
            $cWorksheet->getRowDimension($r)->setRowHeight(40);
            $r++;

            $k = 1;


            foreach ($data as $OrderItem) {
                $cWorksheet->setCellValueByColumnAndRow(0, $r, $k++);
                if(substr($OrderItem['item']['items_id'], -2)=="00") {
                    $imagePath="/images/item/0/".$OrderItem['item']['items_id']."/";
                } else {
                    $imagePath="/images/item/".substr($OrderItem['item']['items_id'], -2)."/".$OrderItem['item']['items_id']."/";
                }
                $img="{$imagePath}1.jpg";
                if (file_exists($_SERVER['DOCUMENT_ROOT'].$img)) {
                    $cDrawing = new PHPExcel_Worksheet_Drawing();
                    $cDrawing->setPath(($_SERVER['DOCUMENT_ROOT'].$img));
                    $cDrawing->setWidthAndHeight(80, 80);
                    $cDrawing->setOffsetX(5);
                    $cDrawing->setOffsetY(5);
                    $cDrawing->setWorksheet($cWorksheet);
                    $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(1) . $r);
                }
                $item=ItemsModel::find()->where(['id'=>$OrderItem['item']['items_id']])->one();
                $cWorksheet->setCellValueByColumnAndRow(2, $r, $OrderItem['item']['items_colors_code']);
                $cWorksheet->setCellValueByColumnAndRow(3, $r, html_entity_decode($item->short_descr, ENT_COMPAT, 'UTF-8'));
                $cWorksheet->setCellValueByColumnAndRow(4, $r, $OrderItem['item']['colorname']);

                if ($OrderItem['quantity']=="free") {
                    $cWorksheet->setCellValueByColumnAndRow(5, $r, 'Бесплатные образцы');
                    $cWorksheet->mergeCells('F' . ($r) . ':I' . ($r));
                } else {

                    $group_num=0;
                    if($OrderItem['quantity']<100) {
                        $group_num=0;
                        $group_name="розница";
                    }
                    if($OrderItem['quantity']>=100 && $OrderItem['quantity']<500) {
                        $group_num=1;
                        $group_name="от 100 до 500";
                    }
                    if($OrderItem['quantity']>=500 && $OrderItem['quantity']<1000) {
                        $group_num=2;
                        $group_name="от 500 до 1000";
                    }
                    if($OrderItem['quantity']>=1000 && $OrderItem['quantity']<5000) {
                        $group_num=3;
                        $group_name="от 1000 до 5000";
                    }
                    if($OrderItem['quantity']>=5000 && $OrderItem['quantity']<10000) {
                        $group_num=4;
                        $group_name="от 5000 до 10000";
                    }
                    if($OrderItem['quantity']>=10000) {
                        $group_num=5;
                        $group_name="от 10000";
                    }

                    $cWorksheet->setCellValueByColumnAndRow(5, $r, $OrderItem['quantity']);
                    $cWorksheet->setCellValueByColumnAndRow(6, $r, preg_replace(['#<br />#', '#&nbsp;#'], ["\n", ' '], $group_name));
                    $cWorksheet->setCellValueByColumnAndRow(7, $r, $OrderItem['item']['price'.$group_num], true);
                    $cWorksheet->setCellValueByColumnAndRow(8, $r, round($OrderItem['item']['price'.$group_num], 1) * $OrderItem['quantity']);
                }


                $cWorksheet->getRowDimension($r)->setRowHeight(80);
                $r++;

                $totalPrice += round(floatval($OrderItem['item']['price'.$group_num]) * (int)$OrderItem['quantity'], 1);
            }

            $cWorksheet->getStyle('A' . $iStart . ':I' . ($r - 1))->getBorders()->applyFromArray([
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ]);

            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(
                0,
                $r,
                'Стоимость продукции: ' . $totalPrice . ' рублей'
            );
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setBold(true)->setSize(12);
            $cWorksheet->getRowDimension($r)->setRowHeight(20);

            /*if (!$freeDeliveryIsImpossible && $CurrentCity && $CurrentCity->free_delivery_price) {
                if ($totalPrice >= $CurrentCity->free_delivery_price) {
                    $r++;
                    $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
                    $cWorksheet->setCellValueByColumnAndRow(
                        0,
                        $r,
                        'Ваш заказ на сумму более ' . number_format($CurrentCity->free_delivery_price, 0, ',', ' ') . ' рублей. ' .
                        'Доставка "до дверей" в городе ' . $CurrentCity->title . ' бесплатно!'
                    );
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setSize(12);
                    $cWorksheet->getRowDimension($r)->setRowHeight(20);
                } else {
                    $r++;
                    $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
                    $cWorksheet->setCellValueByColumnAndRow(
                        0,
                        $r,
                        'До бесплатной доставки "до дверей" в городе ' . $CurrentCity->title . ' в заказе не хватает ' .
                        'товаров на сумму ' . number_format($CurrentCity->free_delivery_price - $totalPrice, 0, ',', ' ') . ' рублей.'
                    );
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                    $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setSize(12);
                    $cWorksheet->getRowDimension($r)->setRowHeight(20);
                }
            }*/
            $r++;
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(
                0,
                $r,
                'Бесплатная доставка не распространяется на горки, бухты каната и цепи, сиденья и каркасы для стульев.'
            );
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getFont()->setSize(12);
            $cWorksheet->getRowDimension($r)->setRowHeight(36);
        } else {
            $cWorksheet->mergeCellsByColumnAndRow(0, $r, 8, $r);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, 'Вы не добавили в заказ ни одного товара');
            $cWorksheet->getStyle('A' . $r . ':I' . $r)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru_korzina.xlsx');
        $cWriter->save('php://output');

        die();


    }

    public function ItemExcel($catalog_element) {

        $Managers=[];
        $cExcel   = new PHPExcel();
        $cExcel->setActiveSheetIndex(0);
        $cWorksheet = $cExcel->getActiveSheet();
        $cExcel->getDefaultStyle()->getAlignment()->setWrapText(true);
        $cExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $cExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $height = 50 + (count($Managers) * 30);

        $cWorksheet->getRowDimension(1)->setRowHeight($height >= 172 ? $height : 172);

        $cDrawing = new PHPExcel_Worksheet_Drawing();
        $cDrawing->setPath('../web/images/excel/logo.png');
        $cDrawing->setWidthAndHeight(200, 200);
        $cDrawing->setOffsetX(10);
        $cDrawing->setOffsetY(10);
        $cDrawing->setWorksheet($cWorksheet);
        $cDrawing->setCoordinates('A1');

        $cWorksheet->mergeCellsByColumnAndRow(2, 1, 5, 1);
        $cWorksheet->setCellValueByColumnAndRow(
            2,
            1,
            "г. Санкт-Петербург, п. Парголово,\r\n" .
            "ул. Подгорная, д. 6. (въезд с Железнодорожной, 11) АНГАР №12\r\n" .
            "график работы: Пн-Пт, 9.00-18.00\r\n\r\n" .
            "8 (812) 242 80 99 (доб. 160)\r\n" .
            "8 (812) 640 91 99 (факс)\r\n" .
            "info@zaglushka.ru (секретарь)\r\n\r\n" .
            (
            count($Managers) === 1 ?
                ($Managers[0]->title . "   (доб. " . $Managers[0]->phone . ")\r\n" .
                    $Managers[0]->email . "\r\n")
                :
                ("руководитель отдела продаж\r\n" .
                    "Тимофеев Денис Вячеславович (доб. 113)\r\n" .
                    "d.timofeev@zaglushka.ru")
            )
        );
        $cWorksheet->getStyle('C1:F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $cWorksheet->mergeCellsByColumnAndRow(6, 1, 9, 1);
        $sLabel = "8 (812) 242 80 99 (многоканальный)\r\n" .
            "8 (800) 555 04 99 (бесплатный звонок по России)\r\n\r\n";
        /*
                if (count($Managers) !== 1) {
                    foreach ($Managers as $Manager) {
                        $sLabel .= $Manager->title . "   (доб. " . $Manager->phone . ")\r\n" . $Manager->email . "\r\n";
                    }
                }
        */
        $cWorksheet->setCellValueByColumnAndRow(6, 1, $sLabel);
        $cWorksheet->getStyle('G1:J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_TOP);

        $r = 2;

        if(substr($catalog_element['item']['id'], -2)=="00") {
            $imagePath="/images/item/0/".$catalog_element['item']['id']."/";
        } else {
            $imagePath="/images/item/".substr($catalog_element['item']['id'], -2)."/".$catalog_element['item']['id']."/";
        }
            $cWorksheet->getRowDimension($r)->setRowHeight(80);
            $setted = [];
            $img1="{$imagePath}1.jpg";
            $img2="{$imagePath}2.jpg";
            $img3="{$imagePath}3.jpg";
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img1)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img1);
                $cDrawing->setWidthAndHeight(200, 200);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(0) . $r);
            } elseif (file_exists($_SERVER['DOCUMENT_ROOT'].$img2)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath(_SERVER['DOCUMENT_ROOT'].$img2);
                $cDrawing->setWidthAndHeight(200, 200);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(0) . $r);
            } elseif (file_exists($_SERVER['DOCUMENT_ROOT'].$img3)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img3);
                $cDrawing->setWidthAndHeight(200, 200);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(5);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(0) . $r);
            }
            $img5="{$imagePath}5.jpg";
            $img6="{$imagePath}6.jpg";
            $img7="{$imagePath}7.jpg";

            $r++;
            $cWorksheet->getRowDimension($r)->setRowHeight(80);
            $skip = 0;
            if (file_exists($_SERVER['DOCUMENT_ROOT'].$img5)) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img5);
                $cDrawing->setWidthAndHeight(75, 75);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(15);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(2) . $r);
            } elseif ($_SERVER['DOCUMENT_ROOT'].$img6) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img6);
                $cDrawing->setWidthAndHeight(75, 75);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(15);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(2) . $r);
            } elseif ($_SERVER['DOCUMENT_ROOT'].$img7) {
                $cDrawing = new PHPExcel_Worksheet_Drawing();
                $cDrawing->setPath($_SERVER['DOCUMENT_ROOT'].$img7);
                $cDrawing->setWidthAndHeight(75, 75);
                $cDrawing->setOffsetX(5);
                $cDrawing->setOffsetY(15);
                $cDrawing->setWorksheet($cWorksheet);
                $cDrawing->setCoordinates(PHPExcel_Cell::stringFromColumnIndex(2) . $r);
            }
            $r++;

        $cWorksheet->mergeCellsByColumnAndRow(0, $r, 9, $r);
        $cWorksheet->getRowDimension($r)->setRowHeight(30);
        $cWorksheet->setCellValueByColumnAndRow(0, $r, html_entity_decode($catalog_element['item']['short_descr'], ENT_COMPAT, 'UTF-8'));

        $r++;
        $cWorksheet->mergeCellsByColumnAndRow(2, $r, 7, $r);
        $cWorksheet->mergeCellsByColumnAndRow(0, $r, 0, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(1, $r, 1, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(8, $r, 8, $r + 1);
        $cWorksheet->mergeCellsByColumnAndRow(9, $r, 9, $r + 1);

        $iFirstDataRow = $r;
        $aHeaders      = [
            0 => ['title' => 'Артикул', 'width' => 20],
            1 => ['title' => 'Цвет', 'width' => 15],
            2 => ['title' => 'Цена, руб. за 1 шт.'],
            8 => ['title' => 'Упаковка', 'width' => 13],
            9 => ['title' => 'Наличие на складе ' . date('d.m.Y', mktime()), 'width' => 13],
        ];
        $cWorksheet->getRowDimension($r)->setRowHeight(20);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $aItem['title']);
            if (isset($aItem['width'])) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }
        $r++;

        $aHeaders = [
            2 => ['title' => 'более 10000', 'width' => 9],
            3 => ['title' => 'от 5000 до 10000', 'width' => 9],
            4 => ['title' => 'от 1000 до 5000', 'width' => 9],
            5 => ['title' => 'от 500 до 1000', 'width' => 9],
            6 => ['title' => 'от 100 до 500', 'width' => 9],
            7 => ['title' => 'розница до 100', 'width' => 9],
        ];

        $cWorksheet->getRowDimension($r)->setRowHeight(30);
        foreach ($aHeaders as $iCell => $aItem) {
            $cWorksheet->setCellValueByColumnAndRow($iCell, $r, $catalog_element['item']['title']);
            if (isset($aItem['width'])) {
                $cWorksheet->getColumnDimensionByColumn($iCell)->setWidth($aItem['width']);
            }
        }
        $r++;

        $c = 1;
        foreach ($catalog_element['item']['colors'] as $Color) {
            //$cWorksheet->getRowDimension($r)->setRowHeight($Item->special && $Color->main && $Item->getSpecialDiscount(0) ? 50 : 40);
            $cWorksheet->setCellValueByColumnAndRow(0, $r, $Color['code']);
            //zero width space addition
            $cWorksheet->setCellValueByColumnAndRow(
                1,
                $r,
                html_entity_decode(str_replace('/', '/&#x200b;', $Color['color_name'])
            ));
            if ($catalog_element['item']['price5']) {
                $objRichText = new PHPExcel_RichText();
                $objRichText->createTextRun($catalog_element['item']['price5']);

                $cWorksheet->setCellValueByColumnAndRow(2, $r, $objRichText);
            }
            if ($catalog_element['item']['price4']) {
                $objRichText = new PHPExcel_RichText();
                $objRichText->createTextRun($catalog_element['item']['price4']);

                $cWorksheet->setCellValueByColumnAndRow(3, $r, $objRichText);
            }
            if ($catalog_element['item']['price3']) {
                $objRichText = new PHPExcel_RichText();
                $objRichText->createTextRun($catalog_element['item']['price3']);

                $cWorksheet->setCellValueByColumnAndRow(4, $r, $objRichText);
            }
            if ($catalog_element['item']['price2']) {
                $objRichText = new PHPExcel_RichText();
                $objRichText->createTextRun($catalog_element['item']['price2']);

                $cWorksheet->setCellValueByColumnAndRow(5, $r, $objRichText);
            }
            if ($catalog_element['item']['price1']) {
                $objRichText = new PHPExcel_RichText();
                $objRichText->createTextRun($catalog_element['item']['price1']);

                $cWorksheet->setCellValueByColumnAndRow(6, $r, $objRichText);
            }
            if ($catalog_element['item']['price0']) {
                $objRichText = new PHPExcel_RichText();
                $objRichText->createTextRun($catalog_element['item']['price0']);

                $cWorksheet->setCellValueByColumnAndRow(7, $r, $objRichText);
            }
            $cWorksheet->setCellValueByColumnAndRow(8, $r, $catalog_element['item']['pack_size']);
            $cWorksheet->setCellValueByColumnAndRow(9, $r, $Color['remains'] ?: '―');
            $r++;
        }

        $cWorksheet->getStyle('A' . $iFirstDataRow . ':J' . ($iFirstDataRow + 1))->getFont()->setBold(true);
        $cWorksheet->getStyle('A' . $iFirstDataRow . ':J' . ($iFirstDataRow + 1))->getFill()->applyFromArray([
            'type'       => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => ['rgb' => 'CECECE'],
        ]);
        $cWorksheet->getStyle('A' . $iFirstDataRow . ':J' . ($r - 1))->getBorders()->applyFromArray([
            'allborders' => [
                'style' => PHPExcel_Style_Border::BORDER_THIN,
            ],
        ]);

        $cWriter = PHPExcel_IOFactory::createWriter($cExcel, 'Excel2007');
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.‌sheet');//vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=Zaglushka.Ru_' . $catalog_element['item']['title'] . '.xlsx');
        $cWriter->save('php://output');

        die();


    }
}