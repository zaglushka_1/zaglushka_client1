<?php

namespace app\controllers;

use app\models\CitiesModel;
use app\models\ItemsModel;
use app\models\ItemsSizesClass;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\CartModel;
use app\models\ClientModel;
use app\models\ItemClass;
use app\models\ItemColorClass;
use app\models\SlugModel;
use YandexCheckout\Client;
use app\models\CachePagesModel;
use app\models\LoginForm;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use PHPExcel_RichText;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Style_Color;
use PHPExcel_Style_Fill;
use PHPExcel_Worksheet;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_PageSetup;
use app\models\SlugsModel;
use Yii\helpers\ExcelHelpers;


class SiteController extends Controller
{

    public function __construct($id,$module,$config=[]) {

        // поиск телефона города
        parent::__construct($id,$module,$config);
    }
    public function init() {
        parent::init();
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->layout='new_desing';
        return $this->render('static/index', []);
    }

    public function actionCity($city) {
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('city');
        $cookies->add(new \yii\web\Cookie(['name' => 'city','value' => $city]));
        $string=explode("/",$_SERVER['HTTP_REFERER']);
        $city=CitiesModel::find()->where(['title'=>$city])->one();
        if($city['subdomain']=='') {
            $url="http://oporu.ru";
        } else {
            $url="http://oporu.ru";
        }
        for($i=3;$i<sizeof($string);++$i) {
            $url.="/".$string[$i];
        }
        return $this->redirect($url);
    }

    public function actionCompare($print=NULL, $excel=NULL) {
        $request=Yii::$app->request;
        if($request->post() && $request->post("id")) {
            $id = $request->post("id");
            $session = Yii::$app->session->getId();
            $key = "compare_" . $session;
            $cache = Yii::$app->cache;
            $data = $cache->get($key);
            if ($data === false) {
                $data[$id] = $id;
                $cache->set($key, $data);
            } else {
                if (isset($data[$id])) {
                    unset($data[$id]);
                } else {
                    $data[$id] = $id;
                }
            }
            $cache->set($key, $data);
            return true;
        }
        if($request->get()) {
            $print=$request->get('print');
            $excel=$request->get('excel');
            Yii::$app->params['print']=true;
            $data['catalog_items']=ClientModel::getCompareItems();
            if($excel==1) {
                ExcelHelpers::CompareExcel($data);
                die;
            }
            if($print==1) {
                $this->layout='print';
                return $this->render('compare_print', ['catalog_element'=>$data]);
            }
            return $this->render('compare', ['catalog_element'=>$data]);
        }
    }

    public function actionCode($code) {
        if($code!==NULL) {
            $items = ItemsModel::find()->where(['code' => $code])->all();
            //print_r($items);
            return $this->render('items-code', ['items'=>$items,'title'=>$code]);
        }
        throw new NotFoundHttpException;
    }

    public function actionCart($print=NULL, $excel=NULL) {
        $request=Yii::$app->request;
        $action=$request->post("action");
        if($action=="addItem") {
            $id=$request->post("id");
            $quantity=$request->post("quantity");
            if(CartModel::addToCart($id,$quantity)) {
                return "Товар Добавлен";
            } else {
                return "Товар не добавлен";
            }
        }
        if($action=="addFreeItem") {
            $id=$request->post("id");
            $quantity="free";
            if(CartModel::addToCart($id,$quantity)) {
                return "Товар Добавлен";
            } else {
                return "Товар не добавлен";
            }
        }
        if($action=="deleteItem") {
            $id=$request->post("id");
            if(CartModel::deleteCart($id)) {
                return $id;
            } else {
                return "No delete";
            }
        }
        if($action=="flushItem") {
            if(CartModel::flushCart()) {
                return "Всё ок";
            } else {
                return "Всё не ок";
            }
        }
        // в yandex-pay-controller
        if($action=="pay-online") {
            /*$client = new Client();
            $client->setAuth('625523', 'test_M0IfmOvTcWadWkLWwKJjf6hTG8CReNaWSEKr5QT2jlI');
            $idempotenceKey = uniqid('', true);
            $response = $client->createPayment(
                array(
                    'amount' => array(
                        'value' => '2.00',
                        'currency' => 'RUB',
                    ),
                    'payment_method_data' => array(
                        'type' => 'bank_card',
                    ),
                    'confirmation' => array(
                        'type' => 'redirect',
                        'return_url' => 'https://dev.zaglushka.ru/return_url',
                    ),
                    'description' => 'Заказ №72',
                ),
                $idempotenceKey
            );*/
            //$confirmationUrl = $response->getConfirmation()->getConfirmationUrl();
            //return $confirmationUrl;
        }
        if($action=="addForm") {
            $id=$request->post("id");
            $itemData=ItemClass::getItemData($id);
            $itemColorData=ItemColorClass::getItemColorData($id);
            return $this->renderPartial('add-to-cart', ['itemData'=>$itemData, 'itemColorData'=>$itemColorData]);
        } else {
            Yii::$app->params['print']=true;
            $print=$request->get('print');
            $excel=$request->get('excel');
            if($excel==1) {
                ExcelHelpers::CartExcel(CartModel::getCart());
                die;
            }
            if($print==1) {
                $this->layout='print';
                return $this->render('cart_print', ['cart'=>(CartModel::getCart()) ? CartModel::getCart() : NULL]);
            }
            return $this->render('cart', ['cart'=>(CartModel::getCart()) ? CartModel::getCart() : NULL]);
        }
    }
    
    public function actionPay() {
        $top_menu=ClientModel::getAllCatalogElement(); 
        Yii::$app->params['catalog_element']=$top_menu;
        return $this->render('pay', []);
    }
    
    public function actionBooklet() {
        $top_menu=ClientModel::getAllCatalogElement(); 
        Yii::$app->params['catalog_element']=$top_menu;
        return $this->render('booklet', []);
    }
    
    public function actionDelivery() {
        $top_menu=ClientModel::getAllCatalogElement(); 
        Yii::$app->params['catalog_element']=$top_menu;
        return $this->render('delivery', []);
    }
    
    public function actionContacts() {
        $top_menu=ClientModel::getAllCatalogElement(); 
        Yii::$app->params['catalog_element']=$top_menu;
        return $this->render('contacts', []);
    }

    public function actionSpecial() {

    }
    
    public function actionSearch() {
        $cache=Yii::$app->cache;
        $top_menu=ClientModel::getAllCatalogElement(); 
        Yii::$app->params['catalog_element']=$top_menu;
        $autocomplete=[];
        $filters=[];
        $items=json_decode($cache->get("Items"),true);
        foreach($items as $item) {
            $autocomplete[]=$item['code'];
            $autocomplete[]=$item['title'];

        }
        $itemsFiltes=[];
        $items_sizes=json_decode($cache->get("ItemsSizes"),true);
        foreach($items_sizes as $filter_type => $itemSize) {
            $filters[$filter_type]['filter']=$itemSize;
            $itemsFiltes[$filter_type]=$itemSize;
        }
        $request=Yii::$app->request;
        $search_string=$request->get('search_string');
        if($search_string!==FALSE and strlen($search_string)>0) {
            // убираем запрос

            $items=ItemsModel::find()->where("`code` = '{$search_string}' OR `title`='{$search_string}' OR (`short_descr` LIKE '%{$search_string}%')")->all();
            $search_result=[];
            foreach($items as $item) {
                $search_result[$item->cat->parent->title][]=$item;
            }
        }
        $filter_type=$request->get('filter_type');
        if($filter_type!==FALSE and strlen($filter_type)>0) {
            $filter_id=[];
            $filter_size_1=$request->get('size_1');
            $filter_size_2=$request->get('size_2');
            $filter_size_3=$request->get('size_3');
            $filter_size_4=$request->get('size_4');
            $filter_size_5=$request->get('size_5');
            $filter_size_6=$request->get('size_6');
            foreach($itemsFiltes[$filter_type] as $itemFilter) {
                $filtred_id_flag=true;
                if(isset($filter_size_1) && $filter_size_1!='undefined' && $filtred_id_flag && $itemFilter['size_1']!=$filter_size_1) {
                    $filtred_id_flag=false;
                }
                if(isset($filter_size_2) && $filter_size_2!='undefined' && $filtred_id_flag && $itemFilter['size_2']!=$filter_size_2) {
                    $filtred_id_flag=false;
                }
                if(isset($filter_size_3) && $filter_size_3!='undefined' && $filtred_id_flag && $itemFilter['size_3']!=$filter_size_3) {
                    $filtred_id_flag=false;
                }
                if(isset($filter_size_4) && $filter_size_4!='undefined' && $filtred_id_flag && $itemFilter['size_4']!=$filter_size_4) {
                    $filtred_id_flag=false;
                }
                if(isset($filter_size_5) && $filter_size_5!='undefined' && $filtred_id_flag && $itemFilter['size_5']!=$filter_size_5) {
                    $filtred_id_flag=false;
                }
                if(isset($filter_size_6) && $filter_size_6!='undefined' && $filtred_id_flag && $itemFilter['size_6']!=$filter_size_6) {
                    $filtred_id_flag=false;
                }
                if($filtred_id_flag==true) {
                    $filter_id[]=$itemFilter['item_id'];
                }
            }
            $items=ItemsModel::find()->where(['id'=>$filter_id])->all();
            $search_result=[];
            foreach($items as $item) {
                $search_result[$item->cat->parent->title][]=$item;
            }

        }

        return $this->render('search', [
            'autocomplete'=>$autocomplete,
            'search_string'=>(isset($search_string)) ? $search_string : '',
            'search_result'=>(isset($search_result)) ? $search_result : '',
            'filters'=>(isset($filters)) ? $filters : '',
            'filter_type'=>(isset($filter_type)) ? $filter_type : '',
        ]);
    }
    
    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        $top_menu=ClientModel::getAllCatalogElement(); 
        Yii::$app->params['catalog_element']=$top_menu;
        return $this->render('about');
    }
}
